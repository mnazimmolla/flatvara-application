$(document).ready(function(){
	$('#details_info').click(function(){
		$('#details_form').css('display', 'inherit');
	});
	$('#interested_peo').click(function(){
		$('#interested_cat').css('display', 'inherit');
	});
	$('#rent').click(function(){
		$('#rent_cat').css('display', 'inherit');
		$('#sell_cat').css('display', 'none');
	});
	$('#sell').click(function(){
		$('#sell_cat').css('display', 'inherit');
		$('#rent_cat').css('display', 'none');
	});	
	$('#class_add').change(function(){
		var cat = $('#class_add').val();
		if(cat == 1)
		{
			$('#interested_peo').css('display', 'none');
			$('#details_info').css('display', 'none');
			$('#rent').css('display', 'inline-block');
			$('#sell').css('display', 'inline-block');
			$('#rent_cat').css('display', 'none');			
			$('#unit_cat').css('display', 'inherit');
			$('#land_cat').css('display', 'inherit');
			$('#sqr_for_cat_1').css('display', 'none');
			$('#interested_cat').css('display', 'none');
			$('#details_form').css('display', 'none');
			$('#others_cat').css('display', 'none');
			$('#advance').css('display', 'inherit');
			$('#bed').css('display', 'none');
			$('#bath').css('display', 'none');			
			$('#room_cat_id').css('display', 'none');	
			$('#bath_room_diff').css('display', 'none');
			$('#rent_date_basis').css('display', 'inherit');			
		}
		else if(cat == 2)
		{
			$('#rent').css('display', 'inline-block');
			$('#sell').css('display', 'inline-block');
			$('#details_info').css('display', 'inherit');
			$('#rent_cat').css('display', 'none');	
			$('#sell_cat').css('display', 'none');
			$('#sqr_for_cat_1').css('display', 'inherit');
			$('#land_cat').css('display', 'none');
			$('#unit_cat').css('display', 'none');
			$('#bath').css('display', 'inherit');
			$('#bed').css('display', 'inherit');			
			$('#room_cat_id').css('display', 'none');			
			$('#rent').click(function(){
				$('#interested_peo').css('display', 'block');
			});
			$('#sell').click(function(){
				$('#interested_peo').css('display', 'none');
			});
		}
		else if(cat == 3)
		{
			$('#rent').css('display', 'inline-block');
			$('#sell').css('display', 'inline-block');
			$('#rent_cat').css('display', 'none');	
			$('#sqr_for_cat_1').css('display', 'inherit');
			$('#details_info').css('display', 'block');	
			$('#sell_cat').css('display', 'none');
			$('#land_cat').css('display', 'none');
			$('#unit_cat').css('display', 'none');
			$('#bath').css('display', 'inherit');
			$('#bed').css('display', 'inherit');			
			$('#room_cat_id').css('display', 'none');			
			$('#rent').click(function(){
				$('#interested_peo').css('display', 'block');
			});
			$('#sell').click(function(){
				$('#interested_peo').css('display', 'none');
			});
		}
		else if(cat == 4)
		{
			$('#interested_peo').css('display', 'block');
			$('#details_info').css('display', 'block');	
			$('#rent').css('display', 'none');	
			$('#sell').css('display', 'none');	
			$('#rent_cat').css('display', 'inherit');
			$('#sqr_for_cat_1').css('display', 'none');
			$('#unit_cat').css('display', 'none');
			$('#land_cat').css('display', 'none');	
			$('#bath').css('display', 'inherit');
			$('#bed').css('display', 'inherit');	
			$('#others_cat').css('display', 'none');
			$('#advance').css('display', 'inherit');
			$('#room_cat_id').css('display', 'none');			
			$('#bath_room_diff').css('display', 'inherit');
			$('#rent_date_basis').css('display', 'inherit');			
		}		
		else if(cat == 5)
		{
			$('#interested_peo').css('display', 'none');
			$('#details_info').css('display', 'block');	
			$('#rent').css('display', 'none');	
			$('#sell').css('display', 'none');	
			$('#sqr_for_cat_1').css('display', 'none');
			$('#rent_cat').css('display', 'inherit');
			$('#unit_cat').css('display', 'none');
			$('#land_cat').css('display', 'none');	
			$('#bath').css('display', 'inherit');
			$('#bed').css('display', 'inherit');	
			$('#others_cat').css('display', 'none');
			$('#advance').css('display', 'inherit');
			$('#room_cat_id').css('display', 'none');
			$('#bath_room_diff').css('display', 'inherit');	
			$('#rent_date_basis').css('display', 'inherit');			
		}
		else if( cat == 6)
		{
			$('#interested_peo').css('display', 'none');
			$('#details_info').css('display', 'block');	
			$('#rent').css('display', 'none');	
			$('#sell').css('display', 'none');	
			$('#rent_cat').css('display', 'inherit');
			$('#unit_cat').css('display', 'none');
			$('#sqr_for_cat_1').css('display', 'none');
			$('#land_cat').css('display', 'none');	
			$('#bath').css('display', 'inherit');
			$('#bed').css('display', 'inherit');	
			$('#others_cat').css('display', 'none');
			$('#advance').css('display', 'inherit');
			$('#room_cat_id').css('display', 'none');	
			$('#bath_room_diff').css('display', 'inherit');	
			$('#rent_date_basis').css('display', 'inherit');			
		}
		else if(cat == 7)
		{
			$('#interested_peo').css('display', 'none');
			$('#details_info').css('display', 'block');	
			$('#rent').css('display', 'none');	
			$('#sell').css('display', 'none');	
			$('#rent_cat').css('display', 'inherit');
			$('#unit_cat').css('display', 'none');
			$('#land_cat').css('display', 'none');	
			$('#sqr_for_cat_1').css('display', 'none');
			$('#bath').css('display', 'none');
			$('#bed').css('display', 'none');	
			$('#others_cat').css('display', 'none');
			$('#advance').css('display', 'inherit');
			$('#room_cat_id').css('display', 'none');	
			$('#bath_room_diff').css('display', 'inherit');	
			$('#rent_date_basis').css('display', 'inherit');			
		}	
		else if(cat == 8)
		{
			$('#interested_peo').css('display', 'none');
			$('#details_info').css('display', 'block');	
			$('#rent').css('display', 'none');	
			$('#sell').css('display', 'none');	
			$('#rent_cat').css('display', 'inherit');
			$('#sqr_for_cat_1').css('display', 'none');
			$('#unit_cat').css('display', 'none');
			$('#land_cat').css('display', 'none');	
			$('#bath').css('display', 'none');
			$('#bed').css('display', 'none');	
			$('#others_cat').css('display', 'none');
			$('#advance').css('display', 'inherit');
			$('#room_cat_id').css('display', 'none');
			$('#bath_room_diff').css('display', 'inherit');	
			$('#rent_date_basis').css('display', 'inherit');			
		}		
		else if(cat == 10)
		{
			$('#interested_peo').css('display', 'none');
			$('#details_info').css('display', 'none');
			$('#rent').css('display', 'inline-block');
			$('#sell').css('display', 'inline-block');
			$('#rent_cat').css('display', 'none');
			$('#unit_cat').css('display', 'none');
			$('#sqr_for_cat_1').css('display', 'inline-block');
			$('#land_cat').css('display', 'none');		
			$('#interested_cat').css('display', 'none');
			$('#details_form').css('display', 'none');
			$('#others_cat').css('display', 'none');
			$('#advance').css('display', 'inherit');	
			$('#bed').css('display', 'none');
			$('#bath').css('display', 'none');	
			$('#room_cat_id').css('display', 'none');	
			$('#bath_room_diff').css('display', 'none');	
			$('#rent_date_basis').css('display', 'inherit');			
		}
		else if(cat == 9)
		{
			$('#interested_peo').css('display', 'none');
			$('#details_info').css('display', 'block');	
			$('#rent').css('display', 'none');	
			$('#sell').css('display', 'none');	
			$('#rent_cat').css('display', 'inherit');
			$('#sqr_for_cat_1').css('display', 'none');
			$('#unit_cat').css('display', 'none');
			$('#land_cat').css('display', 'none');	
			$('#bath').css('display', 'none');
			$('#bed').css('display', 'none');
			$('#others_cat').css('display', 'none');
			$('#advance').css('display', 'none');
			$('#room_cat_id').css('display', 'inherit');
			$('#bath_room_diff').css('display', 'none');			
			$('#rent_date_basis').css('display', 'none');
		}
		else if(cat == 11) {
			$('#interested_peo').css('display', 'none');
			$('#details_info').css('display', 'none');
			$('#rent').css('display', 'inline-block');
			$('#sell').css('display', 'inline-block');
			$('#rent_cat').css('display', 'none');
			$('#sqr_for_cat_1').css('display', 'none');
			$('#unit_cat').css('display', 'none');
			$('#land_cat').css('display', 'none');		
			$('#interested_cat').css('display', 'none');
			$('#details_form').css('display', 'none');
			$('#room_bed_basis').css('display', 'none');
			$('#others_cat').css('display', 'inherit');
			$('#advance').css('display', 'inherit');	
			$('#bed').css('display', 'none');
			$('#bath').css('display', 'none');
			$('#room_cat_id').css('display', 'none');	
			$('#bath_room_diff').css('display', 'none');	
			$('#rent_date_basis').css('display', 'inherit');			
		}
		else 
		{
			$('#interested_peo').css('display', 'block');
			$('#details_info').css('display', 'block');	
			$('#rent').css('display', 'none');	
			$('#sell').css('display', 'none');	
			$('#rent_cat').css('display', 'inherit');
			$('#sqr_for_cat_1').css('display', 'none');
			$('#unit_cat').css('display', 'none');
			$('#land_cat').css('display', 'none');	
			$('#bath').css('display', 'inherit');
			$('#bed').css('display', 'inherit');	
			$('#room_bed_basis').css('display', 'none');
			$('#others_cat').css('display', 'none');
			$('#advance').css('display', 'inherit');
			$('#room_cat_id').css('display', 'none');
			$('#bath_room_diff').css('display', 'none');			
		}
	});
	//Anytime Based Rent & Date Based Rent
	$('#rent_date_basis').change(function(){
		var date = $('#rent_date_basis').val();
		if(date != '')
		{
			$('#anytime').attr('disabled', true);
		}
		else{
			$('#anytime').attr('disabled', false);
		}
	});
	$('#anytime').change(function(){
		var time = $('#anytime').val();
		
		if($("#anytime").prop('checked') == true){
			$('#rent_date_basis').attr('disabled', true);
		}
		else
		{
			$('#rent_date_basis').attr('disabled', false);
		}
	});
	//Anytime Based Rent & Date Based Rent End
	
	//Bathroom Based Room & Attach Bathroom Based Room
	$('#bath_room').change(function(){
		var date = $('#bath_room').val();
		if(date != '')
		{
			$('#bath_room_attach').attr('disabled', true);
		}
		else{
			$('#bath_room_attach').attr('disabled', false);
		}
	});
	$('#bath_room_attach').change(function(){
		var time = $('#bath_room_attach').val();
		
		if($("#bath_room_attach").prop('checked') == true){
			$('#bath_room').attr('disabled', true);
		}
		else
		{
			$('#bath_room').attr('disabled', false);
		}
	});
	//Bathroom Based Room & Attach Bathroom Based Room End
});