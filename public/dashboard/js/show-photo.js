		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function (e) {
					$('#main').attr('src', e.target.result);
					$('#main').css('display', 'inline-block');
				}
				reader.readAsDataURL(input.files[0]);
			}
		}
		$("#main_img").change(function(){
			readURL(this);
		});	
		
		function secondImage(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function (e) {
					$('#main_two').attr('src', e.target.result);
					$('#main_two').css('display', 'inline-block');
				}
				reader.readAsDataURL(input.files[0]);
			}
		}
		$("#img_two").change(function(){
			secondImage(this);
		});
		
		function thirdImage(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function (e) {
					$('#main_three').attr('src', e.target.result);
					$('#main_three').css('display', 'inline-block');
				}
				reader.readAsDataURL(input.files[0]);
			}
		}
		$("#img_three").change(function(){
			thirdImage(this);
		});
		
		function fouthImage(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function (e) {
					$('#main_four').attr('src', e.target.result);
					$('#main_four').css('display', 'inline-block');
				}
				reader.readAsDataURL(input.files[0]);
			}
		}
		$("#img_four").change(function(){
			fouthImage(this);
		});	
		
		function fifthImage(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function (e) {
					$('#main_five').attr('src', e.target.result);
					$('#main_five').css('display', 'inline-block');
				}
				reader.readAsDataURL(input.files[0]);
			}
		}
		$("#img_five").change(function(){
			fifthImage(this);
		});	
		
		function sixthImage(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function (e) {
					$('#main_six').attr('src', e.target.result);
					$('#main_six').css('display', 'inline-block');
				}
				reader.readAsDataURL(input.files[0]);
			}
		}
		$("#img_six").change(function(){
			sixthImage(this);
		});	