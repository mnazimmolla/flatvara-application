$(document).ready(function(){
	$('#class_add').change(function(){
		$('#month_basis_cat').css('display', 'none');
		$('#sell_rent_basis').val(0);
		var searchVal = $('#class_add').val();
		if(searchVal == 1)
		{
			$('#land_basis_cat').css('display', 'inline-flex');
			$('#sell_basis_cat').css('display', 'inline-flex');
			$('#otherCat_add').css('display', 'none');
			$('#hotel_bording').css('display', 'none');
			$('#interested_peo').css('display', 'none');
		}
		else if(searchVal == 2)
		{
			$('#sell_basis_cat').css('display', 'inline-flex');
			$('#land_basis_cat').css('display', 'none');
			$('#hotel_bording').css('display', 'none');
			$('#otherCat_add').css('display', 'none');
			$('#interested_peo').css('display', 'none');
		}
		else if(searchVal == 3)
		{
			$('#sell_basis_cat').css('display', 'inline-flex');
			$('#interested_peo').css('display', 'none');
			$('#hotel_bording').css('display', 'none');
			$('#otherCat_add').css('display', 'none');
		}
		else if(searchVal == 4)
		{
			$('#sell_basis_cat').css('display', 'none');
			$('#month_basis_cat').css('display', 'inline-flex');
			$('#interested_peo').css('display', 'inline-flex');
			$('#land_basis_cat').css('display', 'none');
			$('#hotel_bording').css('display', 'none');
			$('#otherCat_add').css('display', 'none');
			
		}
		else if(searchVal == 5)
		{
			$('#sell_basis_cat').css('display', 'none');
			$('#month_basis_cat').css('display', 'inline-flex');
			$('#interested_peo').css('display', 'none');
			$('#land_basis_cat').css('display', 'none');
			$('#hotel_bording').css('display', 'none');	
			$('#otherCat_add').css('display', 'none');		
		}
		else if(searchVal == 6)
		{
			$('#sell_basis_cat').css('display', 'none');
			$('#month_basis_cat').css('display', 'inline-flex');
			$('#interested_peo').css('display', 'none');
			$('#land_basis_cat').css('display', 'none');
			$('#hotel_bording').css('display', 'none');	
			$('#otherCat_add').css('display', 'none');		
		}
		else if(searchVal == 7)
		{
			$('#sell_basis_cat').css('display', 'none');
			$('#month_basis_cat').css('display', 'inline-flex');
			$('#interested_peo').css('display', 'none');
			$('#land_basis_cat').css('display', 'none');
			$('#hotel_bording').css('display', 'none');	
			$('#otherCat_add').css('display', 'none');		
		}
		else if(searchVal == 8)
		{
			$('#sell_basis_cat').css('display', 'none');
			$('#month_basis_cat').css('display', 'inline-flex');
			$('#interested_peo').css('display', 'none');
			$('#land_basis_cat').css('display', 'none');
			$('#hotel_bording').css('display', 'none');	
			$('#otherCat_add').css('display', 'none');		
		}
		else if(searchVal == 9)
		{
			$('#hotel_bording').css('display', 'inline-flex');
			$('#land_basis_cat').css('display', 'none');
			$('#sell_basis_cat').css('display', 'none');
			$('#interested_peo').css('display', 'none');
			$('#otherCat_add').css('display', 'none');			
		}
		else if(searchVal == 10)
		{
			$('#hotel_bording').css('display', 'none');
			$('#land_basis_cat').css('display', 'none');
			$('#sell_basis_cat').css('display', 'inline-flex');
			$('#interested_peo').css('display', 'none');
			$('#otherCat_add').css('display', 'none');			
		}
		else if(searchVal == 11 )
		{
			$('#otherCat_add').css('display', 'inline-flex');
			$('#sell_basis_cat').css('display', 'inline-flex');
			$('#hotel_bording').css('display', 'none');
			$('#interested_peo').css('display', 'none');
			$('#land_basis_cat').css('display', 'none');
		}
		else
		{
			$('#land_basis_cat').css('display', 'none');
			/* $('#sell_basis_cat').css('display', 'none'); */
			$('#otherCat_add').css('display', 'none');
			$('#hotel_bording').css('display', 'none');
			$('#interested_peo').css('display', 'none');
		}
	});
 	$('#sell_rent_basis').change(function(){
		var rent = $('#sell_rent_basis').val();
		var searchVal = $('#class_add').val();
		if(rent == 2 && searchVal== 1 ||  searchVal == 11 )
		{
			$('#month_basis_cat').css('display', 'inline-flex');
			$('#interested_peo').css('display', 'none');			
		}
		else if(rent == 2 && searchVal == 10)
		{
			$('#month_basis_cat').css('display', 'inline-flex');
			$('#interested_peo').css('display', 'none');
		}
		else if(rent == 2)
		{
			$('#month_basis_cat').css('display', 'inline-flex');
			$('#interested_peo').css('display', 'inline-flex');
		}
		else if(rent == 1 )
		{
			$('#month_basis_cat').css('display', 'none');
			$('#interested_peo').css('display', 'none');
		}
		else 
		{
			$('#month_basis_cat').css('display', 'none');
			$('#interested_peo').css('display', 'none');
		}
	});
});