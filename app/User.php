<?php

namespace App;
use DB;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'pro_pic', 'dob', 'status', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected static function moderator()
    {
       $moderators = DB::table('users')
        ->where('status', 2)
        ->paginate(25);

        return $moderators;
    }
    public static function isAdmin()
    {
        if(Auth::user()->status == 1)
        {
            return true;
        }
        return false;
    }
    public static function isModerator()
    {
        if(Auth::user()->status == 2)
        {
            return true;
        }
        return false;
    }
    public static function searcyByEmail($user)
    {
        $user = DB::table('users')->where('email', $user)->get()->first();
        return $user;
    }
    public static function searcyByName($user)
    {
        $user = DB::table('users')->where('name', $user)->get()->first();
        return $user;
    }
}
