<?php

namespace App\Http\Controllers;
use App\Ad;
use DB;
use Auth;
use Illuminate\Http\Request;

class PromotionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
        $id = Auth::user()->id;
        $ads = Ad::getPromotedAd($id);
        return view('template.dashboard.sections.promoted')->with('ads', $ads);
    }
    public function promoted()
    {
        $ads = Ad::getAllPromoted();
        return view('template.dashboard.sections.promotedAdmin')->with('ads', $ads);
    }
    public function promoteAd()
    {   $ads = Ad::getPublishedAds(1);
        return view('template.dashboard.sections.promote')->with('ads', $ads);
    }
    public function promoteAdbyId($id)
    {   $id = $id;
        return view('template.dashboard.sections.promoteForm')->with('id', $id);
    }
    public function promoteCreate($id)
    {
        $ad = Ad::find($id);
        $ad->promotion = 1;
        $ad->promoted_at = date('Y-m-d');
        $ad->save();

        $promo = DB::table('promotions')->where('ad_id', $id)->delete();

        return redirect('/promote/request')->with('message', 'বিজ্ঞাপন প্রমোট সম্পন্ন');

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id){
        return view('template.frontEnd.sections.promotion');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'transection' => 'required',
        ]);
        $data = DB::table('promotions')->insert([
            'transection' => $request->transection,
            'user_id' => $request->user_id,
            'ad_id' => $request->ad_id,
        ]);
        return redirect('/my/promoted')->with('message', 'বিজ্ঞাপন প্রমোট করার জন্য এডমিন বার্তা পেয়েছেন');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
