<?php

namespace App\Http\Controllers;

use App\Ad;
use Illuminate\Http\Request;
use App\Area;
use App\User;
use Image;
use Auth;
use DB;
use App\Favourite;
class AdController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $user_id = Auth::user()->id;
        $ads = Ad::getAds($user_id);
        return view('template.dashboard.sections.my')->with('ads',$ads);
    }
    public function getUsers()
    {
        $path = public_path("email-list.txt");
        $file = fopen($path, 'w+');
        $data = DB::table('users')->pluck('email', 'name');
        $email = '';
        $count = 1;
        foreach($data as $key => $value)
        {
            $email .= $count. ". " .$key . ' ' .$value."\n";
            $count++;
        }
        fwrite($file, $email);
        fclose($file);
        return response()->download($path);

    }
    public function create()
    {
        $divisions = Area::getDivisions();
        $categories = Ad::getCategories();
        $sub_categories = Ad::getSubcategories();
        return view('template.dashboard.sections.create',[
            'divisions'=>$divisions,
            'categories'=>$categories,
            'sub_categories'=>$sub_categories
        ]);
    }

    public function show($id)
    {
        $ad = Ad::getAdById($id);
        return view('template.dashboard.sections.single')->with('ad',$ad);
    }
    public function showforAdmin($id)
    {
        $ad = Ad::getAdById($id);
        return view('template.dashboard.sections.singleForAdmin')->with('ad',$ad);        
    }
    public function destroyforAdmin($id)
    {
        $ad = Ad::find($id);
        $ad->delete();
        return redirect('/dashboard/ads')->with('message', 'বিজ্ঞাপন ডিলিট সম্পন্ন হয়েছে');
    }
    public function pendingsAds()
    {   
        $ads = Ad::pendings();
        return view('template.dashboard.sections.pendings')->with('ads', $ads);
    }
    public function pendingShow($id)
    {
        $ad = Ad::find($id);
        return view('template.dashboard.sections.pending')->with('ad', $ad);
    }
    public function pendingPublish($id)
    {
        $ad = Ad::find($id);
        $ad->status = 3;
        $ad->save();
        return redirect('/pendings')->with('message', 'বিজ্ঞাপন প্রকাশিত সম্পন্ন');
    }
    public function pendingDestroy($id)
    {
        $ad = Ad::find($id);
        $ad->delete();
        return redirect('/pendings')->with('message', 'বিজ্ঞাপন ডিলিট সম্পন্ন হয়েছে');
    }
    public function edit($id)
    {
        $ad = Ad::finder($id);
        $divisions = Area::getDivisions();
        $categories = Ad::getCategories();
        $sub_categories = Ad::getSubcategories();
        return view('template.dashboard.sections.edit',[
            'ad' => $ad,
            'divisions' => $divisions,
            'categories' => $categories,
            'sub_categories' => $sub_categories
        ]);
    }
    public function ads()
    {
        $ads = Ad::ads();
        return view('template.dashboard.sections.ads')->with('ads', $ads);
    }
    public function publish($id)
    {
        $ad = Ad::find($id);
        $ad->status = 3;
        $ad->save();
        return redirect('/dashboard/ads')->with('message', 'বিজ্ঞাপন প্রকাশিত সম্পন্ন');
    }
    public function favourite($id)
    {
        $user_id = Auth::user()->id;
        $favourite = DB::table('favourites')->get();
       
        foreach ($favourite as $key => $value) {
            if(($value->ad_id == $id) && ($value->user_id == $user_id))
            {
               return redirect('/ad/favourite')->with('message', 'এড ফেভারিট সম্পন্ন হয়েছে');
            }
        }
        
        DB::table('favourites')->insert([
           'user_id' => $user_id,
           'ad_id' => $id,
        ]);
        return redirect('/ad/favourite')->with('message', 'এড ফেভারিট সম্পন্ন হয়েছে');

    }
    public function favouriteAds()
    {
        $favourites = Favourite::favourites(Auth::user()->id);
        return view('template.dashboard.sections.favourite')->with('favourites', $favourites);
    }
    public function removeFavourite($id)
    {
        $favourite = Favourite::find($id);
        $favourite->delete();

        return redirect('ad/favourite')->with('message', 'এড ফেভারিট মুছে ফেলা হয়েছে');
    }
    public function showReportFrom($id)
    {  
        $ad = Ad::getAdById($id);
        return view('template.frontEnd.sections.report')->with('ad', $ad);     
    }
    public function report(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required',
            'ad_id' => 'required',
            'report' => 'required',
        ]);
        DB::table('reports')->insert([
           'user_id' => $request->user_id,
           'ad_id' => $request->ad_id,
           'report' => $request->report,
        ]);

        return redirect('ad/report/' . $request->ad_id)->with('message', 'রিপোর্ট সম্পন্ন হয়েছে');
    }
    public function showReports()
    {
        if (!User::isAdmin()) {
          return redirect('/dashboard');
        }
        $ads = Ad::reports();
        return view('template.dashboard.sections.reports')->with('ads', $ads);
    }
    public function reportAdDelete($id)
    {
        DB::table('reports')->where('ad_id', $id)->delete();
        DB::table('ads')->where('id', $id)->delete();

        return redirect('reports')->with('message', 'বিজ্ঞাপন ডিলিট সম্পন্ন');
    }
    public function reportViewById($id)
    {
        $ad = Ad::reportAdById($id);
        return view('template.dashboard.sections.report')->with('ad', $ad);
    }
    public function update(Request $request, $id)
    {
      
        $this->validate($request,[
            'class_add' => 'required',
            'division' => 'required',
            'district' => 'required',
            'thana' => 'required',
            'area' => 'required',
            'add_title' => 'required',
            'full_address' => 'required',
            'add_details' => 'required',
            'main_img' => 'mimes:jpeg,png,jpg,gif',
            'img_two' => 'mimes:jpeg,png,jpg,gif',
            'img_three' => 'mimes:jpeg,png,jpg,gif',
            'img_four' => 'mimes:jpeg,png,jpg,gif',
            'img_five' => 'mimes:jpeg,png,jpg,gif',
            'img_six' => 'mimes:jpeg,png,jpg,gif',
        ]);

        if( $request->hasFile('main_img')){ 
            $image = $request->file('main_img');
            $img = Image::make($image); 
            $main_img = uniqid() . time().'.'. $image->getClientOriginalExtension();
            $watermark = Image::make(public_path('assets/img/watermark.png'));
            $img->insert($watermark, 'center');
            $img->save(public_path('assets/img/flat/'.$main_img));

        }

        if( $request->hasFile('img_two')){ 
            $imageTwo = $request->file('img_two');
            $imgTwo = Image::make($imageTwo);
            $img_two = uniqid() . time().'.'. $imageTwo->getClientOriginalExtension();
            $watermark = Image::make(public_path('assets/img/watermark.png'));
            $imgTwo->insert($watermark, 'center');
            $imgTwo->save(public_path('assets/img/flat/'.$img_two));
        }

        if( $request->hasFile('img_three')){ 
            $imageThree = $request->file('img_three');
            $imgThree = Image::make($imageThree);
            $img_three = uniqid() . time().'.'. $imageThree->getClientOriginalExtension();
            $watermark = Image::make(public_path('assets/img/watermark.png'));
            $imgThree->insert($watermark, 'center');
            $imgThree->save(public_path('assets/img/flat/'.$img_three));
        }

        if( $request->hasFile('img_four')){ 
            $imageFour = $request->file('img_four');
            $imgFour = Image::make($imageFour);
            $img_four = uniqid() . time().'.'. $imageFour->getClientOriginalExtension();
            $watermark = Image::make(public_path('assets/img/watermark.png'));
            $imgFour->insert($watermark, 'center');
            $imgFour->save(public_path('assets/img/flat/'.$img_four));
        }

        if( $request->hasFile('img_five')){ 
            $imageFive = $request->file('img_five');
            $imgFive = Image::make($imageFive);
            $img_five = uniqid() . time().'.'. $imageFive->getClientOriginalExtension();
            $watermark = Image::make(public_path('assets/img/watermark.png'));
            $imgFive->insert($watermark, 'center');
            $imgFive->save(public_path('assets/img/flat/'.$img_five));
        }

        if( $request->hasFile('img_six')){ 
            $imageSix = $request->file('img_six');
            $imgSix = Image::make($imageSix);
            $img_six = uniqid() . time().'.'. $imageSix->getClientOriginalExtension();
            $watermark = Image::make(public_path('assets/img/watermark.png'));
            $imgSix->insert($watermark, 'center');
            $imgSix->save(public_path('assets/img/flat/'.$img_six));
        }
        $ad = Ad::find($id);

        $ad->class_add = $request->class_add;
        $ad->otherCat = $request->otherCat;
        $ad->division = $request->division;
        $ad->district = $request->district;
        $ad->thana = $request->thana;
        $ad->area = $request->area;
        $ad->add_title = $request->add_title;
        $ad->full_address = $request->full_address;
        $ad->add_details = $request->add_details;
        $ad->land_basis = $request->land_basis;
        $ad->unit = $request->unit;
        $ad->land_area = $request->land_area;
        $ad->sell_rent_basis = $request->sell_rent_basis;
        $ad->rent_basis = $request->rent_basis;
        $ad->rent_date_basis = $request->rent_date_basis;
        $ad->anytime = $request->anytime;
        $ad->advance_cat = $request->advance_cat;
        $ad->sell_price_basis = $request->sell_price_basis;
        $ad->sq_feet = $request->sq_feet;
        $ad->bed_room = $request->bed_room;
        $ad->room_cat = $request->room_cat;
        $ad->bath_room = $request->bath_room;
        $ad->bath_room_attach = $request->bath_room_attach;
        $ad->mobile2 = $request->mobile2;
        $ad->another_mobile_2 = $request->another_mobile_2;
        $ad->another_mobile_3 = $request->another_mobile_3;
        if($request->hasFile('main_img'))
        {
          $ad->main_img = $main_img;
        }
        if($request->hasFile('img_two'))
        {
          $ad->img_two = $img_two;
        } 
        if($request->hasFile('img_three'))
        {
          $ad->img_three = $img_three; 
        } 
        if($request->hasFile('img_four'))
        { 
          $ad->img_four = $img_four;
        } 
        if($request->hasFile('img_five'))
        { 
          $ad->img_five = $img_five;
        }
        if($request->hasFile('img_six'))
        {   
          $ad->img_six = $img_six;
        }  
        $ad->ac = $request->ac;
        $ad->balcony = $request->balcony;
        $ad->cab_tv = $request->cab_tv;
        $ad->grill = $request->grill;
        $ad->electricity = $request->electricity;
        $ad->water = $request->water;
        $ad->cctv = $request->cctv;
        $ad->wifi = $request->wifi;
        $ad->lift = $request->lift;
        $ad->parking = $request->parking;
        $ad->gas = $request->gas;
        $ad->generator = $request->generator;
        $ad->sec_guard = $request->sec_guard;
        $ad->tiles = $request->tiles;
        $ad->mosaic = $request->mosaic;
        $ad->bachelor = $request->bachelor;
        $ad->female = $request->female;
        $ad->huswife = $request->huswife;
        $ad->student = $request->student;
        $ad->jobholder = $request->jobholder;
        $ad->business_holder = $request->business_holder;

        $ad->save();

        return redirect()->back()->with('message','Updated');


    }
    public function publishedAds()
    {
        $user_id = Auth::user()->id;
        $ads = Ad::getPublishedAds($user_id);
        return view('template.dashboard.sections.published')->with('ads', $ads);
    }
    public function unpublishedAd()
    {
        $user_id = Auth::user()->id;
        $ads = Ad::getUnpublishedAds($user_id);
        return view('template.dashboard.sections.unpublished')->with('ads', $ads);
    }
    public function savedAd()
    {
        $user_id = Auth::user()->id;
        $ads = Ad::getSavedAds($user_id);
        return view('template.dashboard.sections.saved')->with('ads', $ads);
    }
    public function destroy($id)
    {   $ad = Ad::find($id);
        if($ad != null)
        {
            $ad->delete();
        }
        return redirect('my/ad')->with('message', 'এড ডিলিট সম্পন্ন হয়েছে'); 
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'class_add' => 'required',
            'division' => 'required',
            'district' => 'required',
            'thana' => 'required',
            'area' => 'required',
            'add_title' => 'required',
            'full_address' => 'required',
            'add_details' => 'required',
            'main_img' => 'required|mimes:jpeg,png,jpg,gif',
            'img_two' => 'mimes:jpeg,png,jpg,gif',
            'img_three' => 'mimes:jpeg,png,jpg,gif',
            'img_four' => 'mimes:jpeg,png,jpg,gif',
            'img_five' => 'mimes:jpeg,png,jpg,gif',
            'img_six' => 'mimes:jpeg,png,jpg,gif',
        ]);

        $class_type = $request->class_add;
        $otherCat = $request->otherCat;
        $division = $request->division;
        $district = $request->district;
        $thana = $request->thana;
        $area = $request->area;
        $add_title = $request->add_title;
        $full_address = $request->full_address;
        $add_details = $request->add_details;
        $land_basis = $request->land_basis;
        $unit = $request->unit;
        $land_area = $request->land_area;
        $sell_rent_basis = $request->sell_rent_basis;
        $rent_basis = $request->rent_basis;
        $rent_date_basis = $request->rent_date_basis;
        $anytime = $request->anytime;
        $advance_cat = $request->advance_cat;
        $sell_price_basis = $request->sell_price_basis;
        $sq_feet = $request->sq_feet;
        $bed_room = $request->bed_room;
        $room_cat = $request->room_cat;
        $bath_room = $request->bath_room;
        $bath_room_attach = $request->bath_room_attach;
        $mobile2 = $request->mobile2;
        $another_mobile_2 = $request->another_mobile_2;
        $another_mobile_3 = $request->another_mobile_3;
        $ac = $request->ac;
        $balcony = $request->balcony;
        $cab_tv = $request->cab_tv;
        $grill = $request->grill;
        $electricity = $request->electricity;
        $water = $request->water;
        $cctv = $request->cctv;
        $wifi = $request->wifi;
        $lift = $request->lift;
        $parking = $request->parking;
        $gas = $request->gas;
        $generator = $request->generator;
        $sec_guard = $request->sec_guard;
        $tiles = $request->tiles;
        $mosaic = $request->mosaic;
        $bachelor = $request->bachelor;
        $female = $request->female;
        $huswife = $request->huswife;
        $student = $request->student;
        $jobholder = $request->jobholder;
        $business_holder = $request->business_holder;
        $user_id = $request->user_id;

        if( $request->hasFile('main_img')){ 
            $image = $request->file('main_img');
            $img = Image::make($image); 
            $main_img = uniqid() . time().'.'. $image->getClientOriginalExtension();
            $watermark = Image::make(public_path('assets/img/watermark.png'));
            $img->insert($watermark, 'center');
            $img->save(public_path('assets/img/flat/'.$main_img));

        }

        if( $request->hasFile('img_two')){ 
            $imageTwo = $request->file('img_two');
            $imgTwo = Image::make($imageTwo);
            $img_two = uniqid() . time().'.'. $imageTwo->getClientOriginalExtension();
            $watermark = Image::make(public_path('assets/img/watermark.png'));
            $imgTwo->insert($watermark, 'center');
            $imgTwo->save(public_path('assets/img/flat/'.$img_two));
        }

        if( $request->hasFile('img_three')){ 
            $imageThree = $request->file('img_three');
            $imgThree = Image::make($imageThree);
            $img_three = uniqid() . time().'.'. $imageThree->getClientOriginalExtension();
            $watermark = Image::make(public_path('assets/img/watermark.png'));
            $imgThree->insert($watermark, 'center');
            $imgThree->save(public_path('assets/img/flat/'.$img_three));
        }

        if( $request->hasFile('img_four')){ 
            $imageFour = $request->file('img_four');
            $imgFour = Image::make($imageFour);
            $img_four = uniqid() . time().'.'. $imageFour->getClientOriginalExtension();
            $watermark = Image::make(public_path('assets/img/watermark.png'));
            $imgFour->insert($watermark, 'center');
            $imgFour->save(public_path('assets/img/flat/'.$img_four));
        }

        if( $request->hasFile('img_five')){ 
            $imageFive = $request->file('img_five');
            $imgFive = Image::make($imageFive);
            $img_five = uniqid() . time().'.'. $imageFive->getClientOriginalExtension();
            $watermark = Image::make(public_path('assets/img/watermark.png'));
            $imgFive->insert($watermark, 'center');
            $imgFive->save(public_path('assets/img/flat/'.$img_five));
        }

        if( $request->hasFile('img_six')){ 
            $imageSix = $request->file('img_six');
            $imgSix = Image::make($imageSix);
            $img_six = uniqid() . time().'.'. $imageSix->getClientOriginalExtension();
            $watermark = Image::make(public_path('assets/img/watermark.png'));
            $imgSix->insert($watermark, 'center');
            $imgSix->save(public_path('assets/img/flat/'.$img_six));
        }

        if($request->file('img_six') != null)
        {
            $data = [
             'user_id' => $user_id,  
             'class_add' => $class_type,  
             'otherCat' => $otherCat,  
             'division' => $division,  
             'district' => $district,  
             'thana' => $thana,  
              'area' => $area,  
              'add_title' => $add_title,  
              'full_address' => $full_address,  
              'add_details' => $add_details,  
             'land_basis' => $land_basis,  
             'unit' => $unit,  
              'land_area' => $land_area,  
              'sell_rent_basis' => $sell_rent_basis,
              'sell_price_basis' => $sell_price_basis,  
             'rent_basis' => $rent_basis,  
              'rent_date_basis' => $rent_date_basis,  
             'anytime' => $anytime,  
             'advance_cat' => $advance_cat,  
              'sq_feet' =>$sq_feet, 
              'bed_room' => $bed_room,  
             'room_cat' => $room_cat,  
              'bath_room' => $bath_room,  
             'bath_room_attach' => $bath_room_attach,  
             'mobile2' => $mobile2,  
             'another_mobile_2' => $another_mobile_2,  
             'another_mobile_3' => $another_mobile_3,  
              'main_img' => $main_img,  
              'img_two' => $img_two,  
              'img_three' => $img_three,  
              'img_four' => $img_four, 
              'img_five' => $img_five,
              'img_six' => $img_six,  
             'ac' => $ac,  
             'balcony' => $balcony,  
             'cab_tv' =>$cab_tv,  
             'grill' => $grill,  
             'electricity' => $electricity,  
             'water' => $water,  
             'cctv' => $cctv,  
             'wifi' => $wifi,  
             'lift' => $lift,  
             'parking' => $parking,  
             'gas' => $gas,  
             'generator' => $generator,  
             'sec_guard' => $sec_guard,  
             'tiles' => $tiles,  
             'mosaic' => $mosaic,  
             'bachelor' => $bachelor,  
             'female' => $female,  
             'huswife' => $huswife,  
             'student' => $student,  
             'jobholder' => $jobholder,  
             'business_holder' => $business_holder
         ];

         Ad::create($data);

        }

        elseif($request->file('img_five') != null && $request->file('img_six') == null)
        {
            $data = [
             'user_id' => $user_id,  
             'class_add' => $class_type,  
             'otherCat' => $otherCat,  
             'division' => $division,  
             'district' => $district,  
             'thana' => $thana,  
              'area' => $area,  
              'add_title' => $add_title,  
              'full_address' => $full_address,  
              'add_details' => $add_details,  
             'land_basis' => $land_basis,  
             'unit' => $unit,  
              'land_area' => $land_area,  
              'sell_rent_basis' => $sell_rent_basis,
              'sell_price_basis' => $sell_price_basis,  
             'rent_basis' => $rent_basis,  
              'rent_date_basis' => $rent_date_basis,  
             'anytime' => $anytime,  
             'advance_cat' => $advance_cat,  
              'sq_feet' =>$sq_feet, 
              'bed_room' => $bed_room,  
             'room_cat' => $room_cat,  
              'bath_room' => $bath_room,  
             'bath_room_attach' => $bath_room_attach,  
             'mobile2' => $mobile2,  
             'another_mobile_2' => $another_mobile_2,  
             'another_mobile_3' => $another_mobile_3,  
              'main_img' => $main_img,  
              'img_two' => $img_two,  
              'img_three' => $img_three,  
              'img_four' => $img_four, 
              'img_five' => $img_five,  
             'ac' => $ac,  
             'balcony' => $balcony,  
             'cab_tv' =>$cab_tv,  
             'grill' => $grill,  
             'electricity' => $electricity,  
             'water' => $water,  
             'cctv' => $cctv,  
             'wifi' => $wifi,  
             'lift' => $lift,  
             'parking' => $parking,  
             'gas' => $gas,  
             'generator' => $generator,  
             'sec_guard' => $sec_guard,  
             'tiles' => $tiles,  
             'mosaic' => $mosaic,  
             'bachelor' => $bachelor,  
             'female' => $female,  
             'huswife' => $huswife,  
             'student' => $student,  
             'jobholder' => $jobholder,  
             'business_holder' => $business_holder
         ];

         Ad::create($data);
        }
        elseif($request->file('img_four') != null && $request->file('img_five') == null)
        {
            $data = [
             'user_id' => $user_id,  
             'class_add' => $class_type,  
             'otherCat' => $otherCat,  
             'division' => $division,  
             'district' => $district,  
             'thana' => $thana,  
              'area' => $area,  
              'add_title' => $add_title,  
              'full_address' => $full_address,  
              'add_details' => $add_details,  
             'land_basis' => $land_basis,  
             'unit' => $unit,  
              'land_area' => $land_area,  
              'sell_rent_basis' => $sell_rent_basis,
              'sell_price_basis' => $sell_price_basis,  
             'rent_basis' => $rent_basis,  
              'rent_date_basis' => $rent_date_basis,  
             'anytime' => $anytime,  
             'advance_cat' => $advance_cat,  
              'sq_feet' =>$sq_feet, 
              'bed_room' => $bed_room,  
             'room_cat' => $room_cat,  
              'bath_room' => $bath_room,  
             'bath_room_attach' => $bath_room_attach,  
             'mobile2' => $mobile2,  
             'another_mobile_2' => $another_mobile_2,  
             'another_mobile_3' => $another_mobile_3,  
              'main_img' => $main_img,  
              'img_two' => $img_two,  
              'img_three' => $img_three,  
              'img_four' => $img_four,  
             'ac' => $ac,  
             'balcony' => $balcony,  
             'cab_tv' =>$cab_tv,  
             'grill' => $grill,  
             'electricity' => $electricity,  
             'water' => $water,  
             'cctv' => $cctv,  
             'wifi' => $wifi,  
             'lift' => $lift,  
             'parking' => $parking,  
             'gas' => $gas,  
             'generator' => $generator,  
             'sec_guard' => $sec_guard,  
             'tiles' => $tiles,  
             'mosaic' => $mosaic,  
             'bachelor' => $bachelor,  
             'female' => $female,  
             'huswife' => $huswife,  
             'student' => $student,  
             'jobholder' => $jobholder,  
             'business_holder' => $business_holder
         ];

         Ad::create($data);
        }
        elseif($request->file('img_three') != null && $request->file('img_four') == null)
        {
            $data = [
             'user_id' => $user_id,  
             'class_add' => $class_type,  
             'otherCat' => $otherCat,  
             'division' => $division,  
             'district' => $district,  
             'thana' => $thana,  
              'area' => $area,  
              'add_title' => $add_title,  
              'full_address' => $full_address,  
              'add_details' => $add_details,  
             'land_basis' => $land_basis,  
             'unit' => $unit,  
              'land_area' => $land_area,  
              'sell_rent_basis' => $sell_rent_basis,
              'sell_price_basis' => $sell_price_basis,  
             'rent_basis' => $rent_basis,  
              'rent_date_basis' => $rent_date_basis,  
             'anytime' => $anytime,  
             'advance_cat' => $advance_cat,  
              'sq_feet' =>$sq_feet, 
              'bed_room' => $bed_room,  
             'room_cat' => $room_cat,  
              'bath_room' => $bath_room,  
             'bath_room_attach' => $bath_room_attach,  
             'mobile2' => $mobile2,  
             'another_mobile_2' => $another_mobile_2,  
             'another_mobile_3' => $another_mobile_3,  
              'main_img' => $main_img,  
              'img_two' => $img_two,  
              'img_three' => $img_three, 
             'ac' => $ac,  
             'balcony' => $balcony,  
             'cab_tv' =>$cab_tv,  
             'grill' => $grill,  
             'electricity' => $electricity,  
             'water' => $water,  
             'cctv' => $cctv,  
             'wifi' => $wifi,  
             'lift' => $lift,  
             'parking' => $parking,  
             'gas' => $gas,  
             'generator' => $generator,  
             'sec_guard' => $sec_guard,  
             'tiles' => $tiles,  
             'mosaic' => $mosaic,  
             'bachelor' => $bachelor,  
             'female' => $female,  
             'huswife' => $huswife,  
             'student' => $student,  
             'jobholder' => $jobholder,  
             'business_holder' => $business_holder
         ];

         Ad::create($data);
        }
        elseif($request->file('img_two') != null && $request->file('img_three') == null)
        {
            $data = [
             'user_id' => $user_id,  
             'class_add' => $class_type,  
             'otherCat' => $otherCat,  
             'division' => $division,  
             'district' => $district,  
             'thana' => $thana,  
              'area' => $area,  
              'add_title' => $add_title,  
              'full_address' => $full_address,  
              'add_details' => $add_details,  
             'land_basis' => $land_basis,  
             'unit' => $unit,  
              'land_area' => $land_area,  
              'sell_rent_basis' => $sell_rent_basis,
              'sell_price_basis' => $sell_price_basis,  
             'rent_basis' => $rent_basis,  
              'rent_date_basis' => $rent_date_basis,  
             'anytime' => $anytime,  
             'advance_cat' => $advance_cat,  
              'sq_feet' =>$sq_feet, 
              'bed_room' => $bed_room,  
             'room_cat' => $room_cat,  
              'bath_room' => $bath_room,  
             'bath_room_attach' => $bath_room_attach,  
             'mobile2' => $mobile2,  
             'another_mobile_2' => $another_mobile_2,  
             'another_mobile_3' => $another_mobile_3,  
              'main_img' => $main_img,  
              'img_two' => $img_two,  
             'ac' => $ac,  
             'balcony' => $balcony,  
             'cab_tv' =>$cab_tv,  
             'grill' => $grill,  
             'electricity' => $electricity,  
             'water' => $water,  
             'cctv' => $cctv,  
             'wifi' => $wifi,  
             'lift' => $lift,  
             'parking' => $parking,  
             'gas' => $gas,  
             'generator' => $generator,  
             'sec_guard' => $sec_guard,  
             'tiles' => $tiles,  
             'mosaic' => $mosaic,  
             'bachelor' => $bachelor,  
             'female' => $female,  
             'huswife' => $huswife,  
             'student' => $student,  
             'jobholder' => $jobholder,  
             'business_holder' => $business_holder
         ];

         Ad::create($data);
        }
        else
        {
            $data = [
             'user_id' => $user_id,  
             'class_add' => $class_type,  
             'otherCat' => $otherCat,  
             'division' => $division,  
             'district' => $district,  
             'thana' => $thana,  
              'area' => $area,  
              'add_title' => $add_title,  
              'full_address' => $full_address,  
              'add_details' => $add_details,  
             'land_basis' => $land_basis,  
             'unit' => $unit,  
              'land_area' => $land_area,  
              'sell_rent_basis' => $sell_rent_basis,
              'sell_price_basis' => $sell_price_basis,  
             'rent_basis' => $rent_basis,  
              'rent_date_basis' => $rent_date_basis,  
             'anytime' => $anytime,  
             'advance_cat' => $advance_cat,  
              'sq_feet' =>$sq_feet, 
              'bed_room' => $bed_room,  
             'room_cat' => $room_cat,  
              'bath_room' => $bath_room,  
             'bath_room_attach' => $bath_room_attach,  
             'mobile2' => $mobile2,  
             'another_mobile_2' => $another_mobile_2,  
             'another_mobile_3' => $another_mobile_3,  
              'main_img' => $main_img,  
             'ac' => $ac,  
             'balcony' => $balcony,  
             'cab_tv' =>$cab_tv,  
             'grill' => $grill,  
             'electricity' => $electricity,  
             'water' => $water,  
             'cctv' => $cctv,  
             'wifi' => $wifi,  
             'lift' => $lift,  
             'parking' => $parking,  
             'gas' => $gas,  
             'generator' => $generator,  
             'sec_guard' => $sec_guard,  
             'tiles' => $tiles,  
             'mosaic' => $mosaic,  
             'bachelor' => $bachelor,  
             'female' => $female,  
             'huswife' => $huswife,  
             'student' => $student,  
             'jobholder' => $jobholder,  
             'business_holder' => $business_holder
         ];

         Ad::create($data);
        }       
        return redirect('my/ad')->with('message', 'এড তৈরি সম্পন্ন হয়েছে');    
    }
}
