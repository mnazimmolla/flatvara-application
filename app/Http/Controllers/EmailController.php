<?php

namespace App\Http\Controllers;

use App\email;
use Illuminate\Http\Request;
use validator;
use Illuminate\Support\Facades\DB;
use Session;
class EmailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function __construct()
    // {
    //     return $this->middleware('auth');
    // }
    public function index(Request $request)
    {
        return view('template.frontEnd.sections.contact');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'subject' => 'required',
            'email' => 'required|email',
            'message' => 'required',
        ]);

        email::create($request->all());
        Session::flash('message', 'Message send successful!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\email  $email
     * @return \Illuminate\Http\Response
     */
    public function show(email $email)
    {
       $messages = email::paginate(10);
        return view('template.dashboard.sections.messages')->with('messages', $messages);
    }

    public function messageByid($id)
    {
        $message = email::find($id);
        return view('template.dashboard.sections.message')->with('message', $message);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\email  $email
     * @return \Illuminate\Http\Response
     */
    public function edit(email $email)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\email  $email
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, email $email)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\email  $email
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       email::destroy($id);
       Session::flash('delete', 'Message has been deleted');
       return redirect('/messages');
    }

}
