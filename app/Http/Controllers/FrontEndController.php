<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Area;
use Image;
use Auth;
use App\Ad;
use DB;
class FrontEndController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ads = Ad::getAdsFrontEnd();
        return view('template.frontEnd.sections.allAd')->with('ads', $ads);
    }

    public function home()
    {
        $ads = DB::select('SELECT id from ads WHERE promotion = 1 AND DATEDIFF(NOW(),promoted_at) >= 7');

        //SELECT id from ads WHERE DATEDIFF(NOW(),promoted_at) >= 7

        if ($ads) {
            foreach ($ads as $key) {
                $ad = Ad::find($key->id);
                $ad->promotion = null;
                $ad->promoted_at = null;
                $ad->save();
            }
        }

        $experied = DB::select('SELECT id from ads WHERE status = 3 AND DATEDIFF(NOW(),created_at) >= 90');

        //SELECT id from ads WHERE status = 3 AND DATEDIFF(NOW(),promoted_at) >= 90

        if ($experied) {
            foreach ($experied as $key) {
                $ad = Ad::find($key->id);
                $ad->delete();
            }
        }

        return view('template.frontEnd.sections.home');
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ad = Ad::getAdById($id);
        return view('template.frontEnd.sections.single')->with('ad',$ad); 
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function searchByClass(Request $request)
    {
        $ads = DB::table('ads')
                   ->join('categories','categories.id', '=', 'ads.class_add')
                   ->join('users','users.id','=','ads.user_id')
                   ->join('divisions','divisions.id','=','ads.division')
                   ->join('districts','districts.id','=','ads.district')
                   ->join('upazilas','upazilas.id','=','ads.thana')
                   ->where('class_add',$request->class_id)
                   ->where('ads.status',3)
                   ->select('ads.*','categories.category AS category', 'divisions.id AS division_id','divisions.bn_name AS division','districts.bn_name AS district','upazilas.bn_name AS thana','districts.id AS district_id','upazilas.id AS thana_id');

        $promotion = DB::table('ads')
                   ->union($ads)
                   ->join('categories','categories.id', '=', 'ads.class_add')
                   ->join('users','users.id','=','ads.user_id')
                   ->join('divisions','divisions.id','=','ads.division')
                   ->join('districts','districts.id','=','ads.district')
                   ->join('upazilas','upazilas.id','=','ads.thana')
                   ->where('ads.status',3)
                   ->where('class_add',$request->class_id)
                   ->where('ads.promotion',1)
                   ->select('ads.*','categories.category AS category', 'divisions.id AS division_id','divisions.bn_name AS division','districts.bn_name AS district','upazilas.bn_name AS thana','districts.id AS district_id','upazilas.id AS thana_id')
                   ->get();
        return response()->json($promotion);
    }

}
