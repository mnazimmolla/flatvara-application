<?php

namespace App\Http\Controllers;

use App\Area;
use Illuminate\Http\Request;
use Validator;
use DB;

class AreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'division_id' => 'required',
            'district_id' => 'required',
            'thana_id' => 'required',
            'area' => 'required',
        ]);

        Area::create($request->all());
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function show(Area $area)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function edit(Area $area)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $location = Area::find($id);
        $location->update($data);

        return redirect('location/search');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $area = Area::find($id);
        $area->delete();
        return redirect('location/search');
    }

    public function searchAreaForm()
    {
        return view('template.dashboard.sections.location-search')->with('data',$data = null);
    }

    public function searchArea(Request $request)
    {
        $area = Area::searchByAreas($request->search);
        return view('template.dashboard.sections.location-search')->with('data',$area);
    }

    public function editLocationForm($id)
    {
        $edit = Area::searchByArea($id);
        $divisions = DB::table('divisions')->get();
        $data = [
            'edit' => $edit[0],
            'divisions' => $divisions
        ];
        return view('template.dashboard.sections.location-edit')->with('data',$data);
    }

}
