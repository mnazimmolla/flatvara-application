<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $allModerator = User::moderator();
        return view('template.dashboard.sections.moderators')->with('moderators', $allModerator);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('template.dashboard.sections.createModerator');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required|min:6',
        ]);
        DB::table('users')->insert([
            'name' => $request->name,
            'email' => $request->email,
            'pro_pic' => 0,
            'status' => 2,
            'password' => bcrypt($request->password),
            'dob' => 0,
        ]); 

        return redirect('/moderator-list')->with('message', 'মডারেটর যোগ করা সম্পন্ন');       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $moderator = User::find($id);
        $moderator->delete();

        return redirect('/moderator-list')->with('message', 'মডারেটর ডিলিট সম্পন্ন');
    }
    public function searchUser()
    {
        return view('template.dashboard.sections.search-users');
    }
    public function getUsers(Request $request)
    {
        $base = $request->serach_base;
        $info = $request->user_cat_serach;

        if($base == 1)
        {
            $user = User::searcyByName($info); 
            return view('template.dashboard.sections.search-user')->with('user', $user);  
        }        
        if($base == 2)
        {
            $user = User::searcyByEmail($info); 
            return view('template.dashboard.sections.search-user')->with('user', $user);   
        }        
        if($base == 3)
        {
            $user = User::searcyByName($info); 
            return view('template.dashboard.sections.search-user')->with('user', $user); 
        }         
        if($base == 4)
        {
            $user = User::searcyByName($info); 
            return view('template.dashboard.sections.search-user')->with('user', $user);   
        } 
    }
    public function blockUser($id)
    {
        $user = User::find($id);
        $user->status = 4;
        $user->save();

        return redirect('/search/user')->with('message', 'ব্যবহারকারী ব্লক সম্পন্ন');
    }
    public function unblockUser($id)
    {
        $user = User::find($id);
        $user->status = 3;
        $user->save();

        return redirect('/search/user')->with('message', 'ব্যবহারকারী আনব্লক সম্পন্ন');
    }
}
