<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Ad extends Model
{
     protected $fillable = [
 			 'user_id',  
             'status',  
             'promotion',
             'promoted_at',  
             'class_add',  
             'otherCat',  
             'division',  
             'district',  
             'thana',  
              'area',  
              'add_title',  
              'full_address',  
              'add_details',  
             'land_basis',  
             'unit',  
              'land_area',  
              'sell_rent_basis',
              'sell_price_basis',  
             'rent_basis',  
              'rent_date_basis',  
             'anytime',  
             'advance_cat',  
              'sq_feet', 
              'bed_room',  
             'room_cat',  
              'bath_room',  
             'bath_room_attach',  
             'mobile2',  
             'another_mobile_2',  
             'another_mobile_3',  
              'main_img',  
              'img_two',  
              'img_three',  
              'img_four', 
              'img_five',  
              'img_six',  
             'ac',  
             'balcony',  
             'cab_tv',  
             'grill',  
             'electricity',  
             'water',  
             'cctv',  
             'wifi',  
             'lift',  
             'parking',  
             'gas',  
             'generator',  
             'sec_guard',  
             'tiles',  
             'mosaic',  
             'bachelor',  
             'female',  
             'huswife',  
             'student',  
             'jobholder',  
             'business_holder'
         ];

    public static function getAds($user_id)
    {
      $ads = DB::table('ads')
                   ->join('categories','categories.id', '=', 'ads.class_add')
                   ->join('users','users.id','=','ads.user_id')
                   ->where('users.id',$user_id)
                   ->select('ads.id AS id','categories.category AS category','ads.add_title AS title','ads.area AS area','ads.sell_price_basis AS price','ads.sell_rent_basis AS rent')
                   ->orderBy('ads.id', 'desc')
                   ->paginate(10);
      return $ads;             
    } 

    public static function getCategories()
    {
      $categories = DB::table('categories')->get();
      return $categories;
    }  

    public static function getSubcategories()
    {
      $sub_categories = DB::table('sub_categories')->get();
      return $sub_categories;
    }  

    public static function getAdById($id)
    {
      $ad = DB::table('ads')
                   ->join('categories','categories.id', '=', 'ads.class_add')
                   ->join('users','users.id','=','ads.user_id')
                   ->join('districts','districts.id','=','ads.district')
                   ->join('upazilas','upazilas.id','=','ads.thana')
                   ->where('ads.id',$id)
                   ->select('ads.*','categories.category AS category','districts.bn_name AS district','upazilas.bn_name AS thana')
                   ->get();
      return $ad[0];             
    }
    public static function getUnpublishedAds($user_id)
    {
      $ads = DB::table('ads')
                   ->join('categories','categories.id', '=', 'ads.class_add')
                   ->join('users','users.id','=','ads.user_id')
                   ->where('users.id', $user_id)
                   //->where('ads.status', 1)
                   ->whereNull('ads.status')
                   ->select('ads.id AS id','categories.category AS category','ads.add_title AS title','ads.area AS area','ads.sell_price_basis AS price','ads.sell_rent_basis AS rent')
                   ->orderBy('id', 'desc')
                   ->paginate(10);
      return $ads;      
    }
    public static function getPublishedAds($user_id)
    {
      $ads = DB::table('ads')
                   ->join('categories','categories.id', '=', 'ads.class_add')
                   ->join('users','users.id','=','ads.user_id')
                   ->where('users.id', $user_id)
                   ->where('ads.status', 3)
                   ->select('ads.id AS id','categories.category AS category','ads.add_title AS title','ads.area AS area','ads.sell_price_basis AS price','ads.sell_rent_basis AS rent', 'users.id as user_id')
                   ->orderBy('id', 'desc')
                   ->paginate(10);
      return $ads;      
    }
    public static function getAdsFrontEnd()
    {
      $ad = DB::table('ads')
                   ->join('categories','categories.id', '=', 'ads.class_add')
                   ->join('districts','districts.id','=','ads.district')
                   ->join('upazilas','upazilas.id','=','ads.thana')
                   ->where('ads.status', 3)
                   ->select('ads.*','categories.category AS category','districts.bn_name AS district','upazilas.bn_name AS thana')
                   ->orderBy('ads.id', 'desc')
                   ->paginate(100);
      return $ad;      
    }
    public static function getSavedAds($user_id)
    {
      $ads = DB::table('ads')
                   ->join('categories','categories.id', '=', 'ads.class_add')
                   ->join('users','users.id','=','ads.user_id')
                   ->where('users.id', $user_id)
                   ->where('ads.status', 4)
                   ->select('ads.id AS id','categories.category AS category','ads.add_title AS title','ads.area AS area','ads.sell_price_basis AS price','ads.sell_rent_basis AS rent', 'users.id AS user_id')
                   ->orderBy('id', 'desc')
                   ->paginate(10);
      return $ads;      
    }
    public static function ads()
    {
      $ads = DB::table('ads')
                   ->join('categories','categories.id', '=', 'ads.class_add')
                   ->join('users','users.id','=','ads.user_id')
                   ->select('ads.id AS id', 'users.name AS Username', 'users.id AS Userid', 'categories.category AS category','ads.add_title AS title','ads.area AS area','ads.sell_price_basis AS price','ads.sell_rent_basis AS rent')
                   ->orderBy('ads.id', 'desc')
                   ->paginate(10);
      return $ads;
    }
    public static function pendings()
    {
      $ads = DB::table('ads')
                   ->join('categories','categories.id', '=', 'ads.class_add')
                   ->join('users','users.id','=','ads.user_id')
                   ->whereNull('ads.status')
                   ->select('ads.id AS id', 'users.name AS Username', 'users.id AS Userid', 'categories.category AS category','ads.add_title AS title','ads.area AS area','ads.sell_price_basis AS price','ads.sell_rent_basis AS rent')
                   ->orderBy('ads.id', 'desc')
                   ->paginate(10);
      return $ads;      
    }
    public static function reports()
    {
      $ads = DB::table('reports')
                   ->join('ads','ads.id','=','reports.ad_id')
                   ->join('categories','categories.id', '=', 'ads.class_add')
                   ->join('users','users.id','=','ads.user_id')
                   ->select('ads.id AS id', 'users.name AS Username', 'categories.category AS category','ads.add_title AS title','ads.area AS area','ads.sell_price_basis AS price','ads.sell_rent_basis AS rent')
                   ->orderBy('ads.id', 'desc')
                   ->paginate(10);
      return $ads;      
    }
    public static function reportAdById($id)
    {
      $ad = DB::table('ads')
                   ->join('categories','categories.id', '=', 'ads.class_add')
                   ->join('users','users.id','=','ads.user_id')
                   ->join('districts','districts.id','=','ads.district')
                   ->join('upazilas','upazilas.id','=','ads.thana')
                   ->join('reports','reports.ad_id','=','ads.id')
                   ->where('ads.id', $id)
                   ->select('ads.*','categories.category AS category','districts.bn_name AS district','upazilas.bn_name AS thana', 'reports.report AS report')
                   ->get();
      return $ad[0];     
    }

    public static function finder($id)
    {
      $ad = DB::table('ads')
                   ->join('categories','categories.id', '=', 'ads.class_add')
                   ->join('users','users.id','=','ads.user_id')
                   ->join('divisions','divisions.id','=','ads.division')
                   ->join('districts','districts.id','=','ads.district')
                   ->join('upazilas','upazilas.id','=','ads.thana')
                   ->where('ads.id', $id)
                   ->select('ads.*','categories.id AS category_id','categories.category AS category_name', 'divisions.id AS division_id', 'divisions.bn_name AS division_name','districts.bn_name AS district', 'districts.id AS district_id','upazilas.bn_name AS thana', 'upazilas.id AS thana_id')
                   ->get();
      return $ad[0];
    }
    public static function getPromotedAd($id)
    {
      $ads = DB::table('ads')
                   ->join('categories','categories.id', '=', 'ads.class_add')
                   ->join('users','users.id','=','ads.user_id')
                   ->where('ads.user_id', $id)
                   ->where('ads.promotion', 1)
                   ->select('ads.id AS id', 'categories.category AS category','ads.add_title AS title','ads.area AS area','ads.sell_price_basis AS price','ads.sell_rent_basis AS rent')
                   ->orderBy('ads.id', 'desc')
                   ->paginate(10);
      return $ads;      
    }
    public static function getPromoteRequests()
    {
      $ads = DB::table('ads')
                   ->join('promotions', 'promotions.ad_id', '=', 'ads.id')
                   ->join('categories','categories.id', '=', 'ads.class_add')
                   ->select('ads.id AS id', 'categories.category AS category','ads.add_title AS title','ads.area AS area','ads.sell_price_basis AS price','ads.sell_rent_basis AS rent', 'promotions.transection as transection')
                   ->orderBy('ads.id', 'desc')
                   ->paginate(10);
      return $ads;            
    }
    public static function getAllPromoted()
    {
      $ads = DB::table('ads')
                   ->join('categories','categories.id', '=', 'ads.class_add')
                   ->join('users','users.id','=','ads.user_id')
                   ->where('ads.promotion', 1)
                   ->select('ads.id AS id','categories.category AS category','ads.add_title AS title','ads.area AS area','ads.sell_price_basis AS price','ads.sell_rent_basis AS rent')
                   ->orderBy('ads.id', 'desc')
                   ->paginate(10);
      return $ads;       
    }
}




