<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Favourite extends Model
{
    public static function favourites($id)
    {
        $favourites = DB::table('favourites')
        ->join('ads', 'ads.id', '=', 'favourites.ad_id')
        ->join('categories','categories.id', '=', 'ads.class_add')
        ->where('favourites.user_id', $id)
        ->select('ads.id AS id','categories.category AS category','ads.add_title AS title','ads.area AS area','ads.sell_price_basis AS price','ads.sell_rent_basis AS rent', 'favourites.id AS favId')
        ->orderBy('id', 'desc')
        ->paginate(10);
        return $favourites;
    }    
}
