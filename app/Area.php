<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Area extends Model
{
    protected $fillable = ['division_id','district_id','thana_id','area'];

    public static function searchByAreas($area = null)
    {
    	$area = "%$area%";

    	$data = DB::table('areas')
    	->join('divisions','divisions.id','=','areas.division_id')
    	->join('districts','districts.id','=','areas.district_id')
    	->join('upazilas','upazilas.id','=','areas.thana_id')
    	->select('divisions.bn_name AS division_name','districts.bn_name AS district_name','upazilas.bn_name AS thana_name','areas.id AS area_id','areas.area AS area_name')
    	->where('area', 'LIKE', $area)->get();

    	return $data;
    }

    public static function searchByArea($id = null)
    {

    	$data = DB::table('areas')
    	->join('divisions','divisions.id','=','areas.division_id')
    	->join('districts','districts.id','=','areas.district_id')
    	->join('upazilas','upazilas.id','=','areas.thana_id')
    	->select('divisions.bn_name AS division_name','divisions.id AS division_id','districts.bn_name AS district_name','districts.id AS district_id','upazilas.bn_name AS thana_name','upazilas.id AS thana_id','areas.id AS area_id','areas.area AS area_name')
    	->where('areas.id', '=', $id)
    	->get();

    	return $data;
    }

    public static function getDivisions()
    {
        $data = DB::table('divisions')->get();
        return $data;
    }

    public static function getAreas($id)
    {
        $data = DB::table('areas')->where('thana_id',$id)->get();
        return $data;
    }
}
