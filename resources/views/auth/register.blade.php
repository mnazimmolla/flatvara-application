@extends('template.frontEnd.master')
@section('title')
    Register
@endsection
@section('content')
    <!-- Signup-->
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">সাইন আপ করুন</h4>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('register') }}">   
                 {{ csrf_field() }} 
                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name">ইউজার নাম</label>
                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus placeholder="ইউজার নাম">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                    </div>
                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email">ইমেইল অথবা মোবাইল নং</label>
                        <input type="text" required class="form-control" name="email" id="email" required autofocus placeholder="ইমেইল অথবা মোবাইল নং">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif                        
                    </div>
                    <div class="form-group {{ $errors->has('pro_pic') ? ' has-error' : '' }}">
                        <label for="pro_pic">ছবি আপলোড দিন</label>
                        <input type="file" required class="form-control" name="pro_pic" id="pro_pic" required autofocus>
                            @if ($errors->has('pro_pic'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('pro_pic') }}</strong>
                                </span>
                            @endif                          
                    </div>
                    <div class="form-group {{ $errors->has('dob') ? ' has-error' : '' }}">
                        <label for="dob">জন্ম তারিখ</label>
                        <input type="text" class="form-control" name="dob" required autofocus id="dob" />
                            @if ($errors->has('dob'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('dob') }}</strong>
                                </span>
                            @endif                            
                    </div>
                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password">পাসওয়ার্ড</label>
                        <input type="password" required class="form-control" name="password" id="password" required autofocus placeholder="পাসওয়ার্ড">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif                            
                    </div>
                    <div class="form-group">
                        <label for="password-confirm">আবার পাসওয়ার্ড</label>
                        <input type="password" required class="form-control" name="password_confirmation" id="password-confirm" required autofocus placeholder="আবার পাসওয়ার্ড">
                    </div>
                    <input type="hidden" name="status" value="3">
                    <div class="modal-footer">
                        <button type="submit" class="btn">সাইন আপ</button>
                    </div>
                </form>
            </div>

            <div class="forget_signup">
                <a href="{{ url('/forgetPassword') }}">পাসওয়ার্ড ভুলে গেছি</a>
                <a href="{{ url('/login') }}">লগ ইন করুন</a>
            </div>
        </div>
    </div>
    <!-- Signup-->
@endsection