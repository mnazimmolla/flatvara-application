@extends('template.frontEnd.master')
@section('title')
    Login
@endsection
@section('content')
<!--  Login -->
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">লগ ইন করুন</h4>
        </div>
        <div class="modal-body">
            <form role="form" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }} 
                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email">ইমেইল অথবা মোবাইল নং</label>
                    <input type="text" required class="form-control" name="email" id="email" placeholder="ইমেইল অথবা মোবাইল নং">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif                    
                </div>
                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password">পাসওয়ার্ড</label>
                    <input type="password" required class="form-control" name="password" id="password" placeholder="পাসওয়ার্ড">
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif                    
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn">লগ ইন করুন</button>
                </div>
            </form>
            <hr>
            <p style="display: inline-block;">লগ ইন করুন</p>
            <button type="button" class="btn"><i class="fa fa-facebook" aria-hidden="true"></i></button>
            <button type="button" class="btn"><i class="fa fa-google-plus" aria-hidden="true"></i>+</button>
            <button type="button" class="btn"><i class="fa fa-twitter" aria-hidden="true"></i></button>
            <p style="display: inline-block;">দিয়ে</p>
        </div>
        <div class="forget_signup">
            <a href="{{ url('/reset')  }}">পাসওয়ার্ড ভুলে গেছি</a>
            <a href="{{ url('/register') }}">সাইন আপ</a>
        </div>
    </div>
</div>
<!-- Login -->
@endsection