@extends('template.frontEnd.master')
@section('content')
<!-- Search Form Part Start -->
<div class="container search">
    <div class="">
        <div class="">
            <form class="form-inline from-padding">
                <div class="form-group">
                    <select id="class_add" name="class_add" class="form-control">
                        <option selected disabled>শ্রেণী</option>
                        <option value="1">প্লট ও জমি</option>
                        <option value="2">এপার্টমেন্ট</option>
                        <option value="3">ফ্ল্যাট </option>
                        <option value="4">রুম/সাবলেট</option>
                        <option value="5">পুরুষ মেস</option>
                        <option value="6">মহিলা মেস</option>
                        <option value="7">বয়েজ হোস্টেল</option>
                        <option value="8">গার্লস হোস্টেল</option>
                        <option value="9">হোটেল/বোর্ডিং</option>
                        <option value="10">অফিস/দোকান</option>
                        <option value="11">অন্যান্য</option>
                    </select>
                </div>
                <div class="form-group" id="otherCat_add">
                    <select id="otherCat" name="otherCat" class="form-control">
                        <option selected disabled>প্রপার্টি নির্বাচন করুন</option>
                        <option value="1">গুদামঘর</option>
                        <option value="2">কলকারখানা</option>
                        <option value="3">গ্যারেজ  </option>
                        <option value="4">রেস্তোরাঁ</option>
                        <option value="5"> কমার্শিয়াল স্পেস</option>
                        <option value="6"> অন্যান্য</option>
                    </select>
                </div>
                <div class="form-group">
                    <select id="division" name="division" class="form-control">
                        <option selected disabled>বিভাগ</option>
                        <option>ঢাকা</option>
                        <option>চট্টগ্রাম</option>
                        <option>রাজশাহী</option>
                        <option>সিলেট</option>
                        <option>বরিশাল</option>
                        <option>খুলনা</option>
                        <option>বরিশাল</option>
                    </select>
                </div>
                <div class="form-group">
                    <select id="district" name="district" class="form-control">
                        <option selected disabled>জেলা</option>
                        <option>ঢাকা</option>
                        <option>নারায়ণগঞ্জ</option>
                        <option>নরসিংদী</option>
                        <option>গাজিপুর</option>
                        <option>শরীয়তপুর</option>
                    </select>
                </div>
                <div class="form-group disabled">
                    <select id="thana" id="thana" class="form-control">
                        <option selected disabled>উপজেলা/থানা</option>
                        <option>নীলখেত</option>
                        <option>শাহবাগ</option>
                        <option>ধানমন্ডি</option>
                        <option>গুলশান</option>
                        <option>উত্তরা</option>
                    </select>
                </div>
                <div class="form-group">
                    <input type="text" name="area" id="area" style="width:120px;" class="form-control" placeholder="এলাকা নির্বাচন করুন" />
                </div>
                <div class="form-group" id="hotel_bording">
                    <select id="room_cat" name="room_cat" class="form-control">
                        <option selected disabled>রুমের ধরণ</option>
                        <option value="1">সিংগেল</option>
                        <option value="2">ডাবল</option>
                    </select>
                </div>
                <div class="form-group" id="land_basis_cat">
                    <select id="land_basis" name="land_basis" class="form-control">
                        <option selected disabled>জমির ধরণ</option>
                        <option value="1">কৃষি</option>
                        <option value="2">বাণিজ্যিক</option>
                        <option value="3">আবাসিক</option>
                        <option value="4">অন্যান্য</option>
                    </select>
                </div>
                <div class="form-group" id="sell_basis_cat">
                    <select id="sell_rent_basis" name="sell_rent_basis" class="form-control">
                        <option value="0">বিক্রি/ভাড়া</option>
                        <option value="1">বিক্রি</option>
                        <option value="2">ভাড়া</option>
                    </select>
                </div>
                <div class="form-group" id="month_basis_cat">
                    <select id="month_basis" class="form-control">
                        <option selected disabled>মাস</option>
                        <option value="1">জানুয়ারি</option>
                        <option value="2">ফেব্রুয়ারি</option>
                        <option value="2">মার্চ</option>
                        <option value="2">এপ্রিল</option>
                        <option value="2">মে</option>
                        <option value="2">জুন</option>
                        <option value="2">জুলাই</option>
                        <option value="2">আগস্ট</option>
                        <option value="2">সেপ্টেম্বর</option>
                        <option value="2">অক্টোবর</option>
                        <option value="2">নভেম্বর</option>
                        <option value="2">ডিসেম্বর</option>
                    </select>
                </div>
                <div class="form-group" id="interested_peo">
                    <select id="interested_basis" class="form-control">
                        <option selected disabled>ভাড়াটিয়ার ধরন</option>
                        <option value="1">student</option>
                        <option value="2">female</option>
                        <option value="3">1</option>
                    </select>
                </div>
                <div class="form-group">
                    <input type="number" class="form-control" style="width:75px;" name="lowest" id="lowest" placeholder="সর্বনিম্ন">
                </div>
                <div class="form-group">
                    <input type="number" class="form-control" style="width:75px;" name="highest" id="highest" placeholder="সর্বোচ্চ">
                </div>
                <button type="submit" id="search" class="btn btn-default">খুঁজুন</button>
            </form>
        </div>
    </div>
</div>
<!-- Search Form Part End -->

<!-- Flat Item Part Start -->
<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div id="test">
                
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{ asset('public/assets/js/jquery-3.2.1.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#class_add').change(function(){
            var class_id = $(this).val();
            var append = '';
            $.ajax({
                url : "{{ url('searchByClass') }}",
                method : "GET",
                data : {class_id : class_id},
                dataType : "json",
                success:function(response)
                {
                    console.log(response);
                    var response_length = response.length;
                    for(var i = 0; i < response_length; i++)
                    {
                        console.log(response);
                        append += "<div class='flat-item-search'>"+
                    "<div class='row'>"+
                    "<div class='col-md-5 flat-image'>"+
                        "<img class='brdr_img' src='http://www.book-a-flat.com/images/paris-salon-2.jpg' alt='image'>"+
                    "</div>"+
                    "<div class='col-md-7 flat-desciption'>"+
                        "<table class='table'>"+
                            "<tr>"+
                                "<td></td>"+
                                "<td>"+
                                    "<a href='#'><h4>"+ response[i].add_title +"</h4></a>"+
                                "</td>"+

                            "</tr>"+
                            "<tr>"+
                                "<td class='flat-item-fa'>"+
                                    "<i class='fa fa-home' aria-hidden='true'>"+
                                "</td>"+
                                "<td>"+ response[i].area+ ',' +response[i].thana +',' +response[i].district+ '।' +"</td>"+
                            "</tr>"+
                            "<tr>"+
                                "<td class='flat-item-fa'>"+
                                    "<i class='fa fa-map-marker' aria-hidden='true'>"+
                                "</td>"+
                                "<td>"+ response[i].full_address +"</td>"+
                            "</tr>"+
                            "<tr>"+
                                "<td class='flat-item-fa'>"+
                                    "<i class='fa fa-map-marker' aria-hidden='true'>"+
                                "</td>"+
                                "<td>"+ "ভাড়া শুরুর তারিখ : " + response[i].rent_date_basis +"</td>"+
                            "</tr>"+
                        
                            "<tr>"+
                                "<td class='flat-item-fa'>"+
                                    "<i class='fa fa-map-marker' aria-hidden='true'>"+
                                "</td>"+
                                "<td>"+ "ভাড়া : " + response[i].sell_rent_basis + '/' + response[i].rent_basis + ',' + 'অগ্রিম ভাড়া ' + response[i].advance_cat + "</td>"+
                            "</tr>"+
                            "<tr>"+
                                "<td class='flat-item-fa'>"+
                                    "<i class='fa fa-map-marker' aria-hidden='true'>"+
                                "</td>"+
                                "<td>"+ "মোবাইল : " + response[i].mobile2 + ',' + response[i].mobile3 + ',' + response[i].mobile4 + "</td>"+
                            "</tr>"+

                            
                           
                            
                        "</table>"+
                    "</div>"+
                "</div>"+
            "</div>";
                    }
                    $('#test').append(append);
                },
                error:function()
                {
                    console.log("There was an error");
                }
            });
        });
    });
</script>
<!-- Flat Item Part End -->
@endsection

