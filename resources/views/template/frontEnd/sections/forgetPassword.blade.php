@extends('template.frontEnd.master')
@section('title')
    Title
@endsection
@section('content')
    <!-- Forgot Password-->
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">পাসওয়ার্ড রিসেট করুন</h4>
            </div>
            <div class="modal-body">
               <form method="POST" action="{{ route('password.request') }}">
                 {{ csrf_field() }}
                    <input type="hidden" name="token"  value="{{ csrf_token()}}">
                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email">ইমেইল</label>
                        <input id="email" type="text" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus placeholder="ইমেইল অথবা ফোন">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif                        
                    </div>
                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}" id="reset_pass">
                        <label for="password">পাসওয়ার্ড</label>
                        <input type="password" required class="form-control" name="password" id="password" placeholder="পাসওয়ার্ড">
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif                        
                    </div>
                    <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}" id="reset_re_pass">
                        <label for="password-confirm">আবার পাসওয়ার্ড</label>
                        <input type="password" required class="form-control" name="password_confirmation" id="password-confirm" placeholder="আবার পাসওয়ার্ড">
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif                        
                    </div>
<!--                     <div class="form-group" id="reset_code">
                        <label for="code_pass">রিসেট কোড</label>
                        <input type="code_pass" required class="form-control" name="code_pass" id="code_pass" placeholder="রিসেট কোড">
                    </div> -->
<!--                     <p class="text-success"> আপনার পাসওয়ার্ড রিসেট কোড ইমেইলে পাঠানো হয়েছে।</p>
                    <p class="text-danger"> ভুল ইমেইল দিয়েছেন</p>
                    <div class="modal-footer"> -->
                        <button class="btn" type="submit" id="sub_email">সাবমিট করুন</button>
                    </div>
                </form>
            </div>
            <div class="forget_signup">
                <a href="{{ url('/login') }}">লগ ইন করুন</a>
                <a href="{{ url('/register') }}">সাইন আপ</a>
            </div>
        </div>
    </div>
    <!-- Forgot Password-->
@endsection