@extends('template.frontEnd.master')
@section('title')
    Contact
@endsection
@section('content')
    <!-- Flat Item Part Start -->
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-6 col-xs-12">
                <div class="contact_part">
                    <h3 style="text-transform: uppercase;">contact us</h3><br>
                    @if(Session::has('message'))
                    <h5 style="text-transform: uppercase;" class="text-info">{{ Session::get('message') }}</h5><br>
                    @endif
                    <form action="{{ url('/contact') }}" method="post">
                        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name">Name</label>
                            <input type="text" name="name" id="name" class="form-control" required placeholder="Your Name">
                            @if ($errors->has('name'))
                                <span class="text-danger">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                             @endif                            
                        </div>
                        <div class="form-group {{ $errors->has('subject') ? ' has-error' : '' }}">
                            <label for="subject">Subject</label>
                            <input type="text" name="subject" id="subject" class="form-control" required placeholder="Your Subject">
                            @if ($errors->has('subject'))
                                <span class="text-danger">
                                    <strong>{{ $errors->first('subject') }}</strong>
                                </span>
                             @endif                               
                        </div>
                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email">E-mail</label>
                            <input type="email" name="email" id="email" class="form-control" required placeholder="Your E-mail">
                            @if ($errors->has('email'))
                                <span class="text-danger">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                             @endif                             
                        </div>
                        <div class="form-group {{ $errors->has('message') ? ' has-error' : '' }}">
                            <label for="message">Message</label>
                            <textarea id="message" rows="6" name="message" required class="form-control"></textarea>
                            @if ($errors->has('message'))
                                <span class="text-danger">
                                    <strong>{{ $errors->first('message') }}</strong>
                                </span>
                             @endif                                
                        </div>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button class="btn" type="submit">Send Message</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Flat Item Part End -->
@endsection