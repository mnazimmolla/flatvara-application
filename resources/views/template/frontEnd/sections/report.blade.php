@extends('template.frontEnd.master')
@section('title')
    {{ $ad->add_title }}
@endsection
@section('content')
    <!-- Flat Item Part Start -->
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
            @if(Session::has('message'))
              <h3 class="text-center text-success">{{ Session::get('message') }}</h3>
            @endif                
                <div>
                    <form role="form" method="POST" action="{{ url('ad/report') }}">
                    {{ csrf_field() }} 

                    <div class="form-group {{ $errors->has('report') ? ' has-error' : '' }}">
                        <label for="report">রিপোর্ট করুন</label>
                        <textarea name="report" id="report" class="form-control" rows="5" placeholder="রিপোর্ট বিস্তারিত লেখুন"></textarea>
                               @if ($errors->has('report'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('report') }}</strong>
                                    </span>
                                @endif                         
                        <input type="hidden" name="ad_id" value="{{ $ad->id }}">           
                        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">           
                    </div>

                    <button type="submit" class="btn">রিপোর্ট করুন</button>
                    </form>  
                    <br>                  
                </div>
                <div class="flat-single" id="print">
                        <div class="row">
                            <div class="col-md-8 col-sm-8 col-xs-12 flat-image">
                                <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:960px;height:480px;overflow:hidden;visibility:hidden;background-color:#24262e;">
                                    <!-- Loading Screen -->
                                    <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
                                        <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="{{ asset('public/assets/img/flat/') }}/spin.svg" />
                                    </div>
                                    <div data-u="slides" style="cursor:default;position:relative;top:0px;left:240px;width:720px;height:480px;overflow:hidden;">

                                        <div data-p="150.00">
                                            <img data-u="image" src="{{ asset('public/assets/img/flat/') }}/{{$ad->main_img}}" />
                                            <img data-u="thumb" src="{{ asset('public/assets/img/flat/') }}/{{ $ad->main_img }}" />
                                        </div> 
                                        @if($ad->img_two)
                                        <div data-p="150.00">
                                            <img data-u="image" src="{{ asset('public/assets/img/flat/') }}/{{$ad->img_two}}" />
                                            <img data-u="thumb" src="{{ asset('public/assets/img/flat/') }}/{{ $ad->img_two }}" />
                                        </div>                                         
                                        @endif 
                                        @if($ad->img_three)
                                        <div data-p="150.00">
                                            <img data-u="image" src="{{ asset('public/assets/img/flat/') }}/{{$ad->img_three}}" />
                                            <img data-u="thumb" src="{{ asset('public/assets/img/flat/') }}/{{ $ad->img_three }}" />
                                        </div>                                         
                                        @endif 
                                        @if($ad->img_four)
                                        <div data-p="150.00">
                                            <img data-u="image" src="{{ asset('public/assets/img/flat/') }}/{{$ad->img_four}}" />
                                            <img data-u="thumb" src="{{ asset('public/assets/img/flat/') }}/{{ $ad->img_four }}" />
                                        </div>                                         
                                        @endif 
                                        @if($ad->img_five)
                                        <div data-p="150.00">
                                            <img data-u="image" src="{{ asset('public/assets/img/flat/') }}/{{$ad->img_five}}" />
                                            <img data-u="thumb" src="{{ asset('public/assets/img/flat/') }}/{{ $ad->img_five }}" />
                                        </div>                                         
                                        @endif 
                                        @if($ad->img_six)
                                        <div data-p="150.00">
                                            <img data-u="image" src="{{ asset('public/assets/img/flat/') }}/{{$ad->img_six}}" />
                                            <img data-u="thumb" src="{{ asset('public/assets/img/flat/') }}/{{ $ad->img_six }}" />
                                        </div>                                         
                                        @endif
                                       
                                    </div>
                                    <!-- Thumbnail Navigator -->
                                    <div data-u="thumbnavigator" class="jssort101" style="position:absolute;left:0px;top:0px;width:240px;height:480px;background-color:#000;" data-autocenter="2" data-scale-left="0.75">
                                        <div data-u="slides">
                                            <div data-u="prototype" class="p" style="width:99px;height:66px;">
                                                <div data-u="thumbnailtemplate" class="t"></div>
                                                <svg viewbox="0 0 16000 16000" class="cv">
                                                    <circle class="a" cx="8000" cy="8000" r="3238.1"></circle>
                                                    <line class="a" x1="6190.5" y1="8000" x2="9809.5" y2="8000"></line>
                                                    <line class="a" x1="8000" y1="9809.5" x2="8000" y2="6190.5"></line>
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Arrow Navigator -->
                                    <div data-u="arrowleft" class="jssora093" style="width:50px;height:50px;top:0px;left:270px;" data-autocenter="2">
                                        <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                                            <circle class="c" cx="8000" cy="8000" r="5920"></circle>
                                            <polyline class="a" points="7777.8,6080 5857.8,8000 7777.8,9920 "></polyline>
                                            <line class="a" x1="10142.2" y1="8000" x2="5857.8" y2="8000"></line>
                                        </svg>
                                    </div>
                                    <div data-u="arrowright" class="jssora093" style="width:50px;height:50px;top:0px;right:30px;" data-autocenter="2">
                                        <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                                            <circle class="c" cx="8000" cy="8000" r="5920"></circle>
                                            <polyline class="a" points="8222.2,6080 10142.2,8000 8222.2,9920 "></polyline>
                                            <line class="a" x1="5857.8" y1="8000" x2="10142.2" y2="8000"></line>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 flat-desciption">
                                <table class="table">
                                    <tr>
                                        <td></td>
                                        <td>
                                            <a href="{{ url('view/' . $ad->id) }}"><h4>{{ $ad->add_title }}</h4></a>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td class="flat-item-fa">
                                            <i class="fa fa-home" aria-hidden="true">
                                        </td>
                                        <td>
                                            {{ $ad->area }}, {{ $ad->thana }}, {{ $ad->district }}।
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="flat-item-fa">
                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                        </td>
                                        <td>
                                            {{ $ad->full_address }}
                                        </td>
                                    </tr>
                                    @if(($ad->class_add == 2) || ($ad->class_add == 3) || ($ad->class_add == 4) || ($ad->class_add == 5) || ($ad->class_add == 6))
                                    <tr>
                                        <td class="flat-item-fa">
                                            <i class="fa fa-bed" aria-hidden="true">
                                        </td>
                                        <td>
                                            বেড <span>{{ $ad->bed_room }}</span>, বাথরুম <span>{{($ad->bath_room_attach === 2) ? "এটাস্টড" : $ad->bath_room}}</span>
                                            @if($ad->sq_feet)
                                                , বর্গফুট
                                             <span>{{ $ad->sq_feet }}</span>
                                            @endif
                                        </td>
                                    </tr>
                                    @endif
                                    @if($ad->class_add == 11)
                                    @if($ad->sq_feet)
                                    <tr>
                                        <td class="flat-item-fa">
                                            <i class="fa fa-bed" aria-hidden="true">
                                        </td>
                                        <td> বর্গফুট
                                             <span>{{ $ad->sq_feet }}</span>
                                        </td>
                                    </tr>
                                    @endif
                                    @endif         
                                    
                                    @if($ad->class_add != 9)
                                    @if($ad->sell_rent_basis)
                                    @if($ad->rent_date_basis)
                                    <tr>
                                        <td class="flat-item-fa">
                                            <i class="fa fa-calendar" aria-hidden="true">
                                        </td>
                                        <td>

                                            ভাড়া শুরুর তারিখ : 
                                           {{ ($ad->anytime != null) ? "যেকোন সময়" : $ad->rent_date_basis}}                                              
                                        </td>
                                    </tr>
                                    @endif
                                    @endif
                                    @endif

                                    @if($ad->class_add === 1)
                                    <tr>
                                        <td class="flat-item-fa">
                                            <i class="fa fa-calendar" aria-hidden="true">
                                        </td>
                                        <td>
                                           জমির ধরনঃ
                                           <span>{{ $ad->land_basis }}, </span> জমির আয়তনঃ
                                           <span>{{ $ad->land_area }} {{ $ad->unit }}</span>                                             
                                        </td>
                                    </tr>
                                    @endif
                                    <tr>
                                        <td class="flat-item-fa">
                                            <i class="fa fa-gg" aria-hidden="true"></i>
                                        </td>
                                        <td>
                                            @if($ad->sell_rent_basis)
                                            ভাড়া 
                                            @if($ad->rent_basis !== "আলোচনা সাপেক্ষে")
                                                {{ $ad->sell_rent_basis }}৳/
                                            @endif
                                                @if($ad->rent_basis)
                                                    @if($ad->class_add != 9)
                                                    {{ $ad->rent_basis}}
                                                    @endif
                                                @endif

                                                @if($ad->advance_cat)
                                                , অগ্রীম ভাড়া {{ $ad->advance_cat }}
                                                @endif
                                            @else
                                            বিক্রি {{ $ad->sell_price_basis }}৳
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="flat-item-fa">
                                            <i class="fa fa-phone" aria-hidden="true"></i>
                                        </td>
                                        <td>
                                            যোগাযোগঃ 
                                            <span style="font-weight:900;">
                                                @if($ad->mobile2)
                                                    {{ $ad->mobile2 }}
                                                @endif
                                                @if($ad->another_mobile_2)
                                                    {{ ",".$ad->another_mobile_2 }}
                                                @endif
                                                @if($ad->another_mobile_3)
                                                    {{ ",".$ad->another_mobile_3 }}
                                                @endif

                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="flat-item-fa">
                                            <i class="fa fa-cog" aria-hidden="true"></i>
                                        </td>
                                        <td class="single-option">
                                            <a href="#">চ্যাট করুন</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="flat-item-fa">
                                            <i class="fa fa-share-alt" aria-hidden="true"></i>
                                        </td>
                                        <td class="add-share">শেয়ার করুন
                                            <span style="font-weight:900;">
                                              <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                              <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                              <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                         </span>
                                        </td>
                                    </tr>
                                </table>

                            </div>
                        </div>
                        <div class="single-bottom-desciption">
                            <br/>
                            <div class="row">
                                <div class="col-md-12 col-xs-12 col-xs-12">
                                @if(($ad->class_add == 2 && $ad->sell_rent_basis) || ($ad->class_add == 3 && $ad->sell_rent_basis) || ($ad->class_add == 4 && $ad->sell_rent_basis))
                                 
                                    <span style="font-weight:bold;">যাদের কাছে ভাড়া দিতে আগ্রহীঃ </span>
                                    <p class="inline-text {{ ($ad->bachelor != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp ব্যাচেলর</p>

                                    <p class="inline-text {{ ($ad->female != null) ? '' : 'fa-text' }}"><i class="fa  fa-check-circle" aria-hidden="true"></i>&nbsp নারী</p>

                                    <p class="inline-text {{ ($ad->huswife != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp স্বামী-স্ত্রী</p>

                                    <p class="inline-text {{ ($ad->student != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp ছাত্র</p>

                                    <p class="inline-text {{ ($ad->jobholder != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp চাকুরীজীবী</p>

                                    <p class="inline-text {{ ($ad->business_holder != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp ব্যবসায়ী</p>

                                @endif  
                                                                                           
                                @if(($ad->class_add == 1) || ($ad->class_add == 2) || ($ad->class_add == 10) || ($ad->class_add == 11) )
                                    <h3>Hoilo na</h3>
                                @else
                                    <br>
                                    <span style="font-weight:bold;">সুবিধাসমূহঃ </span>

                                    <p class="inline-text {{ ($ad->ac != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp এয়ার কন্ডিশন</p>

                                    <p class="inline-text {{ ($ad->balcony != null) ? '' : 'fa-text' }}"><i class="fa  fa-check-circle" aria-hidden="true"></i>&nbsp ব্যালকনি</p>

                                    <p class="inline-text {{ ($ad->cab_tv != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp ক্যাবল টিভি</p>

                                    <p class="inline-text {{ ($ad->grill != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp গ্রিল</p>

                                    <p class="inline-text {{ ($ad->electricity != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp বিদ্যুৎ</p>

                                    <p class="inline-text {{ ($ad->water != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp পানি</p>

                                    <p class="inline-text {{ ($ad->cctv != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp সিসিটিভি</p>
                                       
                                     <p class="inline-text {{ ($ad->wifi != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp ওয়াইফাই/ইন্টারনেট</p>

                                    <p class="inline-text {{ ($ad->lift != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp লিফট</p>

                                    <p class="inline-text {{ ($ad->parking != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp পার্কিং</p>

                                    <p class="inline-text {{ ($ad->gas != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp গ্যাস</p>

                                    <p class="inline-text {{ ($ad->generator != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp জেনারেটর</p>

                                    <p class="inline-text {{ ($ad->sec_guard != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp সিকুরিটি গার্ড</p>

                                    <p class="inline-text {{ ($ad->tiles != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp টাইলস</p>

                                    <p class="inline-text {{ ($ad->mosaic != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp মোজাইক</p>  
                                </div>
                                @endif
                                <div class="col-md-12 col-xs-12 col-xs-12">
                                    <p class="text-justify">
                                        <span style="font-weight:bold;">বিস্তারিতঃ</span>
                                        {!! $ad->add_details !!}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <p class="text-center make-add"><button type="button" class="btn">এই বিজ্ঞাপনটি প্রচার করুন</button></p>
        </div>
    </div>
    <!-- Flat Item Part End -->

    <!-- Flat Related Part Start -->
    <div class="container">
        <div class="row">
            <div class="single-related">
                <h3 style="margin-left:20px; padding-bottom:10px;"><span> কলাবাগান </span> আরও <span> ফ্ল্যাট </span></h3>













                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <div class="related-item">
                                <div class="col-md-3 col-xs-12 col-sm-6 single-related-item">
                                    <div class=" flat-image">
                                        <img src="http://www.book-a-flat.com/images/paris-salon-2.jpg" alt="image">
                                    </div>
                                    <div class="flat-desciption">
                                        <table class="table">
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <a href="#"><h4>ফ্ল্যাট ভাড়া হবে</h4></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-home" aria-hidden="true">
                                                </td>
                                                <td>
                                                    কলাবাগান, ঢাকা
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-bed" aria-hidden="true">
                                                </td>
                                                <td>
                                                    ফ্লোরঃ <span>৭</span> তলা, বেডঃ <span>৩</span>, বাথঃ <span>২</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-calendar" aria-hidden="true">
                                                </td>
                                                <td>
                                                    ভাড়া শুরুর তারিখঃ ০১/০৬/১৭
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-gg" aria-hidden="true"></i>
                                                </td>
                                                <td>
                                                    ভাড়াঃ ৬০০০/-
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                                </td>
                                                <td>
                                                    যোগাযোগঃ <span style="font-weight:900;">01711062063</span>
                                                </td>
                                            </tr>
                                        </table>
                                        <a href="#"><button type="button" class="btn">বিস্তারিত</button></a>
                                    </div>
                                </div>
                            </div>
                            <div class="related-item">
                                <div class="col-md-3 col-xs-12 col-sm-6 single-related-item">
                                    <div class=" flat-image">
                                        <img src="http://www.book-a-flat.com/images/paris-salon-2.jpg" alt="image">
                                    </div>
                                    <div class="flat-desciption">
                                        <table class="table">
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <a href="#"><h4>ফ্ল্যাট ভাড়া হবে</h4></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-home" aria-hidden="true">
                                                </td>
                                                <td>
                                                    কলাবাগান, ঢাকা
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-bed" aria-hidden="true">
                                                </td>
                                                <td>
                                                    ফ্লোরঃ <span>৭</span> তলা, বেডঃ <span>৩</span>, বাথঃ <span>২</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-calendar" aria-hidden="true">
                                                </td>
                                                <td>
                                                    ভাড়া শুরুর তারিখঃ ০১/০৬/১৭
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-gg" aria-hidden="true"></i>
                                                </td>
                                                <td>
                                                    ভাড়াঃ ৬০০০/-
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                                </td>
                                                <td>
                                                    যোগাযোগঃ <span style="font-weight:900;">01711062063</span>
                                                </td>
                                            </tr>
                                        </table>
                                        <a href="#"><button type="button" class="btn">বিস্তারিত</button></a>
                                    </div>
                                </div>
                            </div>
                            <div class="related-item">
                                <div class="col-md-3 col-xs-12 col-sm-6 single-related-item">
                                    <div class=" flat-image">
                                        <img src="http://www.book-a-flat.com/images/paris-salon-2.jpg" alt="image">
                                    </div>
                                    <div class="flat-desciption">
                                        <table class="table">
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <a href="#"><h4>ফ্ল্যাট ভাড়া হবে</h4></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-home" aria-hidden="true">
                                                </td>
                                                <td>
                                                    কলাবাগান, ঢাকা
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-bed" aria-hidden="true">
                                                </td>
                                                <td>
                                                    ফ্লোরঃ <span>৭</span> তলা, বেডঃ <span>৩</span>, বাথঃ <span>২</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-calendar" aria-hidden="true">
                                                </td>
                                                <td>
                                                    ভাড়া শুরুর তারিখঃ ০১/০৬/১৭
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-gg" aria-hidden="true"></i>
                                                </td>
                                                <td>
                                                    ভাড়াঃ ৬০০০/-
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                                </td>
                                                <td>
                                                    যোগাযোগঃ <span style="font-weight:900;">01711062063</span>
                                                </td>
                                            </tr>
                                        </table>
                                        <a href="#"><button type="button" class="btn">বিস্তারিত</button></a>
                                    </div>
                                </div>
                            </div>
                            <div class="related-item">
                                <div class="col-md-3 col-xs-12 col-sm-6 single-related-item">
                                    <div class=" flat-image">
                                        <img src="http://www.book-a-flat.com/images/paris-salon-2.jpg" alt="image">
                                    </div>
                                    <div class="flat-desciption">
                                        <table class="table">
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <a href="#"><h4>ফ্ল্যাট ভাড়া হবে</h4></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-home" aria-hidden="true">
                                                </td>
                                                <td>
                                                    কলাবাগান, ঢাকা
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-bed" aria-hidden="true">
                                                </td>
                                                <td>
                                                    ফ্লোরঃ <span>৭</span> তলা, বেডঃ <span>৩</span>, বাথঃ <span>২</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-calendar" aria-hidden="true">
                                                </td>
                                                <td>
                                                    ভাড়া শুরুর তারিখঃ ০১/০৬/১৭
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-gg" aria-hidden="true"></i>
                                                </td>
                                                <td>
                                                    ভাড়াঃ ৬০০০/-
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                                </td>
                                                <td>
                                                    যোগাযোগঃ <span style="font-weight:900;">01711062063</span>
                                                </td>
                                            </tr>
                                        </table>
                                        <a href="#"><button type="button" class="btn">বিস্তারিত</button></a>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="item">
                            <div class="related-item">
                                <div class="col-md-3 col-xs-12 col-sm-6 single-related-item">
                                    <div class=" flat-image">
                                        <img src="http://www.book-a-flat.com/images/paris-salon-2.jpg" alt="image">
                                    </div>
                                    <div class="flat-desciption">
                                        <table class="table">
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <a href="#"><h4>ফ্ল্যাট ভাড়া হবে</h4></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-home" aria-hidden="true">
                                                </td>
                                                <td>
                                                    কলাবাগান, ঢাকা
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-bed" aria-hidden="true">
                                                </td>
                                                <td>
                                                    ফ্লোরঃ <span>৭</span> তলা, বেডঃ <span>৩</span>, বাথঃ <span>২</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-calendar" aria-hidden="true">
                                                </td>
                                                <td>
                                                    ভাড়া শুরুর তারিখঃ ০১/০৬/১৭
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-gg" aria-hidden="true"></i>
                                                </td>
                                                <td>
                                                    ভাড়াঃ ৬০০০/-
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                                </td>
                                                <td>
                                                    যোগাযোগঃ <span style="font-weight:900;">01711062063</span>
                                                </td>
                                            </tr>
                                        </table>
                                        <a href="#"><button type="button" class="btn">বিস্তারিত</button></a>
                                    </div>
                                </div>
                            </div>
                            <div class="related-item">
                                <div class="col-md-3 col-xs-12 col-sm-6 single-related-item">
                                    <div class=" flat-image">
                                        <img src="http://www.book-a-flat.com/images/paris-salon-2.jpg" alt="image">
                                    </div>
                                    <div class="flat-desciption">
                                        <table class="table">
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <a href="#"><h4>ফ্ল্যাট ভাড়া হবে</h4></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-home" aria-hidden="true">
                                                </td>
                                                <td>
                                                    কলাবাগান, ঢাকা
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-bed" aria-hidden="true">
                                                </td>
                                                <td>
                                                    ফ্লোরঃ <span>৭</span> তলা, বেডঃ <span>৩</span>, বাথঃ <span>২</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-calendar" aria-hidden="true">
                                                </td>
                                                <td>
                                                    ভাড়া শুরুর তারিখঃ ০১/০৬/১৭
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-gg" aria-hidden="true"></i>
                                                </td>
                                                <td>
                                                    ভাড়াঃ ৬০০০/-
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                                </td>
                                                <td>
                                                    যোগাযোগঃ <span style="font-weight:900;">01711062063</span>
                                                </td>
                                            </tr>
                                        </table>
                                        <a href="#"><button type="button" class="btn">বিস্তারিত</button></a>
                                    </div>
                                </div>
                            </div>
                            <div class="related-item">
                                <div class="col-md-3 col-xs-12 col-sm-6 single-related-item">
                                    <div class=" flat-image">
                                        <img src="http://www.book-a-flat.com/images/paris-salon-2.jpg" alt="image">
                                    </div>
                                    <div class="flat-desciption">
                                        <table class="table">
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <a href="#"><h4>ফ্ল্যাট ভাড়া হবে</h4></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-home" aria-hidden="true">
                                                </td>
                                                <td>
                                                    কলাবাগান, ঢাকা
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-bed" aria-hidden="true">
                                                </td>
                                                <td>
                                                    ফ্লোরঃ <span>৭</span> তলা, বেডঃ <span>৩</span>, বাথঃ <span>২</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-calendar" aria-hidden="true">
                                                </td>
                                                <td>
                                                    ভাড়া শুরুর তারিখঃ ০১/০৬/১৭
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-gg" aria-hidden="true"></i>
                                                </td>
                                                <td>
                                                    ভাড়াঃ ৬০০০/-
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                                </td>
                                                <td>
                                                    যোগাযোগঃ <span style="font-weight:900;">01711062063</span>
                                                </td>
                                            </tr>
                                        </table>
                                        <a href="#"><button type="button" class="btn">বিস্তারিত</button></a>
                                    </div>
                                </div>
                            </div>
                            <div class="related-item">
                                <div class="col-md-3 col-xs-12 col-sm-6 single-related-item">
                                    <div class=" flat-image">
                                        <img src="http://www.book-a-flat.com/images/paris-salon-2.jpg" alt="image">
                                    </div>
                                    <div class="flat-desciption">
                                        <table class="table">
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <a href="#"><h4>ফ্ল্যাট ভাড়া হবে</h4></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-home" aria-hidden="true">
                                                </td>
                                                <td>
                                                    কলাবাগান, ঢাকা
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-bed" aria-hidden="true">
                                                </td>
                                                <td>
                                                    ফ্লোরঃ <span>৭</span> তলা, বেডঃ <span>৩</span>, বাথঃ <span>২</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-calendar" aria-hidden="true">
                                                </td>
                                                <td>
                                                    ভাড়া শুরুর তারিখঃ ০১/০৬/১৭
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-gg" aria-hidden="true"></i>
                                                </td>
                                                <td>
                                                    ভাড়াঃ ৬০০০/-
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                                </td>
                                                <td>
                                                    যোগাযোগঃ <span style="font-weight:900;">01711062063</span>
                                                </td>
                                            </tr>
                                        </table>
                                        <a href="#"><button type="button" class="btn">বিস্তারিত</button></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="related-item">
                                <div class="col-md-3 col-xs-12 col-sm-6 single-related-item">
                                    <div class=" flat-image">
                                        <img src="http://www.book-a-flat.com/images/paris-salon-2.jpg" alt="image">
                                    </div>
                                    <div class="flat-desciption">
                                        <table class="table">
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <a href="#"><h4>ফ্ল্যাট ভাড়া হবে</h4></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-home" aria-hidden="true">
                                                </td>
                                                <td>
                                                    কলাবাগান, ঢাকা
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-bed" aria-hidden="true">
                                                </td>
                                                <td>
                                                    ফ্লোরঃ <span>৭</span> তলা, বেডঃ <span>৩</span>, বাথঃ <span>২</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-calendar" aria-hidden="true">
                                                </td>
                                                <td>
                                                    ভাড়া শুরুর তারিখঃ ০১/০৬/১৭
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-gg" aria-hidden="true"></i>
                                                </td>
                                                <td>
                                                    ভাড়াঃ ৬০০০/-
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                                </td>
                                                <td>
                                                    যোগাযোগঃ <span style="font-weight:900;">01711062063</span>
                                                </td>
                                            </tr>
                                        </table>
                                        <a href="#"><button type="button" class="btn">বিস্তারিত</button></a>
                                    </div>
                                </div>
                            </div>
                            <div class="related-item">
                                <div class="col-md-3 col-xs-12 col-sm-6 single-related-item">
                                    <div class=" flat-image">
                                        <img src="http://www.book-a-flat.com/images/paris-salon-2.jpg" alt="image">
                                    </div>
                                    <div class="flat-desciption">
                                        <table class="table">
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <a href="#"><h4>ফ্ল্যাট ভাড়া হবে</h4></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-home" aria-hidden="true">
                                                </td>
                                                <td>
                                                    কলাবাগান, ঢাকা
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-bed" aria-hidden="true">
                                                </td>
                                                <td>
                                                    ফ্লোরঃ <span>৭</span> তলা, বেডঃ <span>৩</span>, বাথঃ <span>২</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-calendar" aria-hidden="true">
                                                </td>
                                                <td>
                                                    ভাড়া শুরুর তারিখঃ ০১/০৬/১৭
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-gg" aria-hidden="true"></i>
                                                </td>
                                                <td>
                                                    ভাড়াঃ ৬০০০/-
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                                </td>
                                                <td>
                                                    যোগাযোগঃ <span style="font-weight:900;">01711062063</span>
                                                </td>
                                            </tr>
                                        </table>
                                        <a href="#"><button type="button" class="btn">বিস্তারিত</button></a>
                                    </div>
                                </div>
                            </div>
                            <div class="related-item">
                                <div class="col-md-3 col-xs-12 col-sm-6 single-related-item">
                                    <div class=" flat-image">
                                        <img src="http://www.book-a-flat.com/images/paris-salon-2.jpg" alt="image">
                                    </div>
                                    <div class="flat-desciption">
                                        <table class="table">
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <a href="#"><h4>ফ্ল্যাট ভাড়া হবে</h4></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-home" aria-hidden="true">
                                                </td>
                                                <td>
                                                    কলাবাগান, ঢাকা
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-bed" aria-hidden="true">
                                                </td>
                                                <td>
                                                    ফ্লোরঃ <span>৭</span> তলা, বেডঃ <span>৩</span>, বাথঃ <span>২</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-calendar" aria-hidden="true">
                                                </td>
                                                <td>
                                                    ভাড়া শুরুর তারিখঃ ০১/০৬/১৭
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-gg" aria-hidden="true"></i>
                                                </td>
                                                <td>
                                                    ভাড়াঃ ৬০০০/-
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                                </td>
                                                <td>
                                                    যোগাযোগঃ <span style="font-weight:900;">01711062063</span>
                                                </td>
                                            </tr>
                                        </table>
                                        <a href="#"><button type="button" class="btn">বিস্তারিত</button></a>
                                    </div>
                                </div>
                            </div>
                            <div class="related-item">
                                <div class="col-md-3 col-xs-12 col-sm-6 single-related-item">
                                    <div class=" flat-image">
                                        <img src="http://www.book-a-flat.com/images/paris-salon-2.jpg" alt="image">
                                    </div>
                                    <div class="flat-desciption">
                                        <table class="table">
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <a href="#"><h4>ফ্ল্যাট ভাড়া হবে</h4></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-home" aria-hidden="true">
                                                </td>
                                                <td>
                                                    কলাবাগান, ঢাকা
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-bed" aria-hidden="true">
                                                </td>
                                                <td>
                                                    ফ্লোরঃ <span>৭</span> তলা, বেডঃ <span>৩</span>, বাথঃ <span>২</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-calendar" aria-hidden="true">
                                                </td>
                                                <td>
                                                    ভাড়া শুরুর তারিখঃ ০১/০৬/১৭
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-gg" aria-hidden="true"></i>
                                                </td>
                                                <td>
                                                    ভাড়াঃ ৬০০০/-
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="flat-item-fa">
                                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                                </td>
                                                <td>
                                                    যোগাযোগঃ <span style="font-weight:900;">01711062063</span>
                                                </td>
                                            </tr>
                                        </table>
                                        <a href="#"><button type="button" class="btn">বিস্তারিত</button></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <!-- Flat Related Part End -->
    <div class="clearfix">
        <div class="breaker"></div>
    </div>
@endsection