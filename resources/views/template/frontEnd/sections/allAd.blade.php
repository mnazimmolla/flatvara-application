@extends('template.frontEnd.master')
@section('title')
    সকল বিজ্ঞাপণ
@endsection
@section('content')
    <!-- Flat Item Part Start -->
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-6 col-xs-12">
                @foreach($ads as $ad)
                <div class="flat-item-search">
                    <div class="row">
                        <div class="col-md-5 flat-image">
                            <img class="{{ ($ad->img_two) ? 'brdr_img' : ' ' }}" src="{{ asset('public/assets/img/flat/') }}/{{ $ad->main_img }}" alt="image">
                        </div>
                        <div class="col-md-7 flat-desciption">
                            <table class="table">
                                <tr>
                                    <td></td>
                                    <td>
                                        <a href="{{ url('view/' . $ad->id) }}"><h4>{{ $ad->add_title }}</h4></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="flat-item-fa">
                                        <i class="fa fa-home" aria-hidden="true">
                                    </td>
                                    <td>
                                         {{ $ad->area }}, {{ $ad->thana }}, {{ $ad->district }}।
                                    </td>
                                </tr>
                                <tr>
                                    <td class="flat-item-fa">
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    </td>
                                    <td>
                                        {{ $ad->full_address }}
                                    </td>
                                </tr>

                                    @if(($ad->class_add == 2) || ($ad->class_add == 3) || ($ad->class_add == 4) || ($ad->class_add == 5) || ($ad->class_add == 6))
                                    <tr>
                                        <td class="flat-item-fa">
                                            <i class="fa fa-bed" aria-hidden="true">
                                        </td>
                                        <td>
                                            বেড <span>{{ $ad->bed_room }}</span>, বাথরুম <span>{{($ad->bath_room_attach === 2) ? "এটাস্টড" : $ad->bath_room}}</span>
                                            @if($ad->sq_feet)
                                                , বর্গফুট
                                             <span>{{ $ad->sq_feet }}</span>
                                            @endif
                                        </td>
                                    </tr>
                                    @endif

                                <tr>
                                    <td class="flat-item-fa">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                    </td>
                                    <td>
                                        ৭ দিন আগে
                                    </td>
                                </tr>

                                    @if($ad->class_add == 11)
                                    @if($ad->sq_feet)
                                    <tr>
                                        <td class="flat-item-fa">
                                            <i class="fa fa-bed" aria-hidden="true">
                                        </td>
                                        <td> বর্গফুট
                                             <span>{{ $ad->sq_feet }}</span>
                                        </td>
                                    </tr>
                                    @endif
                                    @endif  

                                    @if($ad->class_add != 9)
                                    @if($ad->sell_rent_basis)
                                    @if($ad->rent_date_basis)
                                    <tr>
                                        <td class="flat-item-fa">
                                            <i class="fa fa-calendar" aria-hidden="true">
                                        </td>
                                        <td>

                                            ভাড়া শুরুর তারিখ : 
                                           {{ ($ad->anytime != null) ? "যেকোন সময়" : $ad->rent_date_basis}}                                              
                                        </td>
                                    </tr>
                                    @endif
                                    @endif
                                    @endif

                                    @if($ad->class_add === 1)
                                    <tr>
                                        <td class="flat-item-fa">
                                            <i class="fa fa-calendar" aria-hidden="true">
                                        </td>
                                        <td>
                                           জমির ধরনঃ
                                           <span>{{ $ad->land_basis }}, </span> জমির আয়তনঃ
                                           <span>{{ $ad->land_area }} {{ $ad->unit }}</span>                                             
                                        </td>
                                    </tr>
                                    @endif

                                <tr>
                                    <td class="flat-item-fa">
                                        <i class="fa fa-gg" aria-hidden="true"></i>
                                    </td>
                                    <td>
                                        @if($ad->sell_rent_basis)
                                            ভাড়া 
                                            @if($ad->rent_basis !== "আলোচনা সাপেক্ষে")
                                                {{ $ad->sell_rent_basis }}৳/
                                            @endif
                                                @if($ad->rent_basis)
                                                    @if($ad->class_add != 9)
                                                    {{ $ad->rent_basis}}
                                                    @endif
                                                @endif

                                                @if($ad->advance_cat)
                                                , অগ্রীম ভাড়া {{ $ad->advance_cat }}
                                                @endif
                                            @else
                                            বিক্রি {{ $ad->sell_price_basis }}৳
                                            @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td class="flat-item-fa">
                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                    </td>
                                    <td>
                                        যোগাযোগঃ 
                                            <span style="font-weight:900;">
                                                @if($ad->mobile2)
                                                    {{ $ad->mobile2 }}
                                                @endif
                                                @if($ad->another_mobile_2)
                                                    {{ ",".$ad->another_mobile_2 }}
                                                @endif
                                                @if($ad->another_mobile_3)
                                                    {{ ",".$ad->another_mobile_3 }}
                                                @endif
                                            </span>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                            <a class="btn btn-default" href="{{ url('view/' . $ad->id) }}">বিস্তারিত</a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- Flat Item Part End -->
@endsection