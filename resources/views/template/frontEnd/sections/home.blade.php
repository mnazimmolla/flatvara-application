@extends('template.frontEnd.master')
@section('title')
    Home
@endsection
@section('content')
    <!-- Home Icon Part Start -->
    <div class="container hero-area">
        <div class="row">
            <div class="col-md-10 col-sm-10 col-xs-12">
                <div class="row">
                    <div class="col-md-4 col-xs-12">
                        <div class="icon-box box-breaker">
                            <a href="#">
                                <img src="{{ asset('public/assets/img/scatter-graph.png') }}">
                                <h3>প্লট ও জমি</h3>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="icon-box box-breaker">
                            <a href="#">
                                <img src="{{ asset('public/assets/img/apartment.png') }}">
                                <h3>এপার্টমেন্ট</h3>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="icon-box box-breaker">
                            <a href="#">
                                <img src="{{ asset('public/assets/img/home.png') }}">
                                <h3>ফ্ল্যাট  ভাড়া</h3>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="icon-box box-breaker">
                            <a href="#">
                                <img src="{{ asset('public/assets/img/hostel.png') }}">
                                <h3>পুরুষ মেস</h3>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="icon-box box-breaker">
                            <a href="#">
                                <img src="{{ asset('public/assets/img/bunk.png') }}">
                                <h3>মহিলা মেস</h3>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="icon-box box-breaker">
                            <a href="#">
                                <img src="{{ asset('public/assets/img/hotel.png') }}">
                                <h3>আবাসিক হোটেল</h3>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="icon-box box-breaker">
                            <a href="#">
                                <img src="{{ asset('public/assets/img/bed.png') }}">
                                <h3>রুম/সাবলেট</h3>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="icon-box box-breaker">
                            <a href="#">
                                <img src="{{ asset('public/assets/img/online-store.png') }}">
                                <h3>অফিস/দোকান</h3>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="icon-box box-breaker">
                            <a href="#">
                                <img src="{{ asset('public/assets/img/room-key.png') }}">
                                <h3>অন্যান্য</h3>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12">
                <div class="right-nav box-breaker">
                    <ul class="text-right">
                        <li><h3>শহর</h3></li>
                        <li><a href="#">ঢাকা</a></li>
                        <li><a href="#">চট্টগ্রাম</a></li>
                        <li><a href="#">রাজশাহী</a></li>
                        <li><a href="#">সিলেট</a></li>
                        <li><a href="#">বরিশাল</a></li>
                        <li><a href="#">খুলনা</a></li>
                        <li><a href="#">রংপুর</a></li>
                    </ul>
                    <ul class="text-right">
                        <li><h3>বিভাগ</h3></li>
                        <li><a href="#">ঢাকা</a></li>
                        <li><a href="#">চট্টগ্রাম</a></li>
                        <li><a href="#">রাজশাহী</a></li>
                        <li><a href="#">সিলেট</a></li>
                        <li><a href="#">বরিশাল</a></li>
                        <li><a href="#">খুলনা</a></li>
                        <li><a href="#">রংপুর</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Home Icon Part End -->
@endsection