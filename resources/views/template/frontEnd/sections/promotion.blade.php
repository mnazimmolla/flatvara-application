@extends('template.frontEnd.master')
@section('title')
    title
@endsection
@section('content')
    <!-- Promotion Part Start -->
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-6 col-xs-12">
                <div class="promotion_part">
                    <h3>আপনার বিজ্ঞাপনটিকে বিশেষভাবে আকর্ষণীয় করুন</h3>
                    <img src="{{ asset('public/assets/img/promotion.png') }}" class="promo-img">
                    <p style="color:#00adef;">আপনার বিজ্ঞাপনটি ৭ দিনের জন্য টপ বিজ্ঞাপন হিসেবে  উপস্থাপন করুন।</p>
                    <p style="margin-top:10px;">বিকাশ অথবা ডাচবাংলা একাউন্ট থেকে ৪৯৯৳ জমা দিন।</p>
                    <p style="font-size:15px; color:#00adef;">বিকাশঃ 01711062063 ডাচবাংলাঃ 017110620633</p>
                    <form>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="ট্রানজেকশন আইডি/নাম্বার">
                        </div>
                        <button class="btn" type="submit">ভেরিফাই করুন</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Promotion Part End -->
@endsection