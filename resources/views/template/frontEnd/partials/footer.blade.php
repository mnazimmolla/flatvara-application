<!-- Footer Part Start -->
<p class="text-center make-add" style="margin-top: 30px !important; font-size: 17px;color: #2f3432;">flatvara.com-এ আপনার বিজ্ঞাপন পোস্ট করুন।</p>
<p class="text-center make-add"><a style="color: #2f3432;" href="{{ url('/ad/create') }}" class="btn">আপনার বিজ্ঞাপন দিন</a></p>
<footer>
    <div class="">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="footer-box">
                        <a href="{{ url('/')  }}">
                            <img src="{{ asset('public/assets/img/footer_logo.png') }}" alt="Footer Logo">
                        </a>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="footer-nav">
                        <ul class="">
                            <li><h5>সামাজিক</h5></li>
                            <li><a href="#">ফেসবুক</a></li>
                            <li><a href="#">টুইটার</a></li>
                            <li><a href="#">ইউটিউব</a></li>
                            <li><a href="#">ব্লগ</a></li>
                            <li><a href="#">গুগল প্লাস</a></li>
                            <li><a href="#">লিঙ্কেড ইন</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="footer-nav">
                        <ul class="">
                            <li><h5>আরও জানুন</h5></li>
                            <li><a href="#">প্রশ্ন-উত্তর</a></li>
                            <li><a href="#">ব্যানার বিজ্ঞাপন</a></li>
                            <li><a href="#">বিজ্ঞাপন প্রচার করুন</a></li>
                            <li><a href="#">মেম্বারশিপ</a></li>
                            <li><a href="#">দ্রুত ভাড়া দিন</a></li>
                            <li><a href="#">ফ্লাটভাড়া.কম</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="footer-nav">
                        <ul class="">
                            <li><h5>আমাদের কথা</h5></li>
                            <li><a href="#">আমাদের কথা</a></li>
                            <li><a href="#">পেশা</a></li>
                            <li><a href="#">শর্তাবলী ও নীতিমালা</a></li>
                            <li><a href="#">গোপনীয়তার নীতিমালা</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>


<!-- Footer Part End -->
<script type="text/javascript" src="{{ asset('public/assets/js/jquery-3.2.1.min.js') }}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $( function() {
        var availablearea = [
            "ActionScript",
            "AppleScript",
            "Asp",
            "BASIC",
            "C",
            "C++",
            "Clojure",
            "COBOL",
            "ColdFusion",
            "Erlang",
            "Fortran",
            "Groovy",
            "Haskell",
            "Java",
            "JavaScript",
            "Lisp",
            "Perl",
            "PHP",
            "Python",
            "Ruby",
            "Scala",
            "Scheme"
        ];
        $( "#area" ).autocomplete({
            source: availablearea
        });
    } );{{ asset('public/') }}
</script>
<script type="text/javascript" src="{{ asset('public/assets/js/jssor.slider.main.js') }} "></script>
<script type="text/javascript" src="{{ asset('public/assets/js/jsor_slider.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/js/navbar.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/js/propper.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/js/boot-min-3.3.7.js') }}"></script>
<script type="text/javascript">jssor_1_slider_init();</script>
<script src="{{ asset('public/assets/js/script.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/js/modal.js')}}"></script>
<script type="text/javascript" src="{{ asset('public/assets/js/pass_reset.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/assets/js/search.js') }}"></script>
<script>
    $( function() {
        $( "#dob" ).datepicker({
            dateFormat: "dd-mm-yy",
            changeMonth: true,
            changeYear: true,
            yearRange: "-120:+0",
            onSelect: function(date) {
                var dateSplit = date.split("-");
                var dob = new Date(dateSplit[1] + " " + dateSplit[0] + " " + dateSplit[2]);
                var today = new Date();
            }
        });
    } );
</script>
</body>
</html>