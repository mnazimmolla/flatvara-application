<!doctype html>
<html lang="en">
<head>
    <head>
        <title>@yield('title')</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('public/assets/css/boot-min-3.3.7.css') }}">
        <link rel="stylesheet" href="{{ asset('public/assets/css/boot-theme-3.3.7.css') }}">
        <link href="https://fonts.googleapis.com/css?family=Hind+Siliguri" rel="stylesheet">
        <!-- <link rel="stylesheet" href="{{ asset('public/assets/css/font-awesome.css') }}"> -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('public/assets/css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('public/assets/css/single-slider.css') }}">
        <link rel="stylesheet" href="{{ asset('public/assets/css/nav.css') }}">
    </head>
<body>
<!-- Header Part Start -->
<div class="row">
    <div class="container-fluid">
        <header>
            <div class="container">
                <nav>
                    <div class="logo-section">
                        <a class="logo" href="{{ url('/')  }}"><img src="{{ asset('public/assets/img/flatvara-cs6.png') }}" alt="Header Logo"></a>
                        <button id="navtop" class="hb-button"><i class="fa fa-bars"></i></button>
                    </div>
                    <ul>
                        <li><div class="text-center top_add_button"><a href="{{ url('ad/create') }}">আপনার বিজ্ঞাপন দিন</a></div></li>
                        @if(Auth::check())
                        <li><div class="text-center top_add_button"><a href="{{ url('/dashboard') }}">ড্যাশবোর্ড</a></div></li>
                        @endif
                        @if(!Auth::check())
                        <li><a href="{{ url('/register') }}">সাইন আপ</a></li>
                        <li><a href="{{ url('/login') }}">লগ ইন করুন</a></li>
                        @endif

                        @if(Auth::check())
                       <li>
                            <div class="text-center top_add_button">
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    লগ আউট
                                </a>
                            </div>
                        </li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                           {{ csrf_field() }}
                        </form>  
                        @endif

                        <li><a href="{{ url('/contact') }}">যোগাযোগ</a></li>
                        <li><a href="{{ url('/search') }}">খুঁজুন</a></li>
                        <li><a href="{{ url('/ads') }}">সকল বিজ্ঞাপণ</a></li>
                        <li><a href="{{ url('/') }}">হোম</a></li>
                    </ul>
                </nav>
            </div>
        </header>
    </div>
</div>
<!-- Header Part End -->
<div class="clearfix">
    <div class="breaker"></div>
</div>