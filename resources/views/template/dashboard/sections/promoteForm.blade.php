@extends('template.dashboard.master')
@section('title')
বিজ্ঞাপন প্রমোট করুন
@endsection
@section('content')
<!-- /.content-wrapper -->
    <div class="content-wrapper">
      <div class="container-fluid">
        <!-- Breadcrumbs -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="{{ url('/dashboard') }}">ড্যাশবোর্ড</a>
          </li>
          <li class="breadcrumb-item active">বিজ্ঞাপন প্রমোট করুন</li>
        </ol>
	    <!-- Promotion Part Start -->
	    <div class="container">
	        <div class="row">
	            <div class="col-md-12 col-sm-6 col-xs-12">
	                <div class="promotion_part">
	                    <h3>আপনার বিজ্ঞাপনকে বিশেষভাবে আকর্ষণীয় করুন</h3>
	                    <img src="{{ asset('public/assets/img/promotion.png') }}" class="promo-img">
	                    <p style="color:#00adef;">আপনার বিজ্ঞাপনটি ৭ দিনের জন্য টপ বিজ্ঞাপন হিসেবে  উপস্থাপন করুন।</p>
	                    <p style="margin-top:10px;">বিকাশ অথবা ডাচবাংলা একাউন্ট থেকে ৪৯৯৳ জমা দিন।</p>
	                    <p style="font-size:15px; color:#00adef;">বিকাশঃ 01711062063 ডাচবাংলাঃ 017110620633</p>
	                    <form method="POST" action="{{ url('/make/promotion') }}">
	                        <div class="form-group {{ $errors->has('transection') ? 'has-error' : '' }}">
	                            <input type="text" class="form-control" name="transection" id="transection" placeholder="বিকাশ/ডাচ বাংলা ট্রানজেকশন আইডি/নাম্বার">
	                        @if ($errors->has('transection'))
	                            <span class="text-danger">
	                                <strong>বিকাশ বা ডাচ বাংলা ব্যাংকের ট্রানজেকশন নাম্বার দিন</strong>
	                            </span>
	                        @endif 	                            
	                        </div>
	                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
	                        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
	                        <input type="hidden" name="ad_id" value="{{ $id }}">
	                        <button class="btn" type="submit">ভেরিফাই করুন</button>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- Promotion Part End -->        	 		
      </div>
      <!-- /.container-fluid -->
    </div>
<!-- /.content-wrapper -->
@endsection