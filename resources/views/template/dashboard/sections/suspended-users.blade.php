@extends('template.dashboard.master')
@section('title')
Suspended Users
@endsection
@section('content')
<!-- /.content-wrapper -->
    <div class="content-wrapper">
      <div class="container-fluid">
        <!-- Breadcrumbs -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">My Dashboard</li>
        </ol>
    		<div class="search_part">
    			<div class="row">
    				<div class="col-md-3">
    				</div>
    				<div class="col-md-6 search_part_mail">
    					<form>
    						<div class="form-group">
    							<label for="user_cat_serach">Search User</label>
    							<input type="text" name="user_cat_serach" id="user_cat_serach" class="form-control" placeholder="User Info" />
    						</div>
    						<div class="radio">
    						  <label><input type="radio" name="serach_base" id="serach_base" value="1" checked>Phone </label>
    						  <label><input type="radio" name="serach_base" id="serach_base" value="2" checked>E-mail </label>
    						  <label><input type="radio" name="serach_base" id="serach_base" value="2" checked>User Name </label>
    						  <label><input type="radio" name="serach_base" id="serach_base" value="3" checked>Name </label>
    						</div>	
    						<button type="submit" class="btn">Search</button>
    					</form>				
    				</div>
    				<div class="col-md-3">
    				</div>
    			</div>
    		</div>       
     <table class="table table-striped table-bordered">
      <thead>
        <tr>
        <th>Name</th>
        <th>Phone</th>
        <th>E-mail</th>
        <th>Add Posted</th>
        <th>On going</th>
        <th>Active | Block</th>
        </tr>
      </thead>
      <tbody>
      <tr>
        <td>Ahmed Imtiaz</td>
        <td>01711062063</td>
        <td>ahmedimtiaz@gmail.com</td>
        <td>8</td>
        <td>5</td>
        <td><a class="btn" href="#">Active</a> |  <a class="btn" href="#">Block</a></td>
      </tr>                                            
      </tbody>
      </table>
      	<div class="pagi-brand">
 			<nav aria-label="Page navigation example">
			  <ul class="pagination">
			    <li class="page-item"><a class="page-link" href="#">Previous</a></li>
			    <li class="page-item"><a class="page-link" href="#">1</a></li>
			    <li class="page-item"><a class="page-link" href="#">2</a></li>
			    <li class="page-item"><a class="page-link" href="#">3</a></li>
			    <li class="page-item"><a class="page-link" href="#">4</a></li>
			    <li class="page-item"><a class="page-link" href="#">5</a></li>
			    <li class="page-item"><a class="page-link" href="#">6</a></li>
			    <li class="page-item"><a class="page-link" href="#">Next</a></li>
			  </ul>
			</nav>     		
      	</div>
      </div>
      <!-- /.container-fluid -->
    </div>
<!-- /.content-wrapper -->
@endsection