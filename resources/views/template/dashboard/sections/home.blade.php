@extends('template.dashboard.master')
@section('title')
  ড্যাশবোর্ড
@endsection
@section('content')
<!-- /.content-wrapper -->
    <div class="content-wrapper">
      <div class="container-fluid">
        <!-- Breadcrumbs -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="{{ url('/dashboard') }}">ড্যাশবোর্ড</a>
          </li>
          <li class="breadcrumb-item active">ড্যাশবোর্ড</li>
        </ol>
        <div class="row">
		
		  @if(Auth::user()->status == 1)
          <div class="col-md-4 col-xs-12 col-sm-4">
            <div class="home-box">
              <div class="home-inner-box">
                  <p>সর্বমোট বিজ্ঞাপন <span class="count">{{ $data['totalAd'] }}</span></p>
              </div>
            <p>ডাটাবেজে সর্বমোট বিজ্ঞাপন এর সংখ্যা এখানে গণনা হচ্ছে। ডাটাবেজ থেকে সকল বিজ্ঞাপন এখান থেকে পাওয়া যাবে। <br><a class="btn btn-success" href="{{ url('/dashboard/ads') }}">সকল বিজ্ঞাপন দেখুন</a></p>
            </div>
          </div> 
          <div class="col-md-4 col-xs-12 col-sm-4">
            <div class="home-box">
              <div class="home-inner-box">
                  <p>অপ্রকাশিত বিজ্ঞাপন <span class="count">{{ $data['unpublished'] }}</span></p>
              </div>
            <p>ডাটাবেজে অপ্রকাশিত বিজ্ঞাপন এর সংখ্যা এখানে গণনা হচ্ছে। ডাটাবেজ থেকে অপ্রকাশিত বিজ্ঞাপন এখান থেকে পাওয়া যাবে। <br><a class="btn btn-success" href="{{ url('/pendings') }}">অপ্রকাশিত বিজ্ঞাপন দেখুন</a></p>
            </div>
          </div>  
          <div class="col-md-4 col-xs-12 col-sm-4">
            <div class="home-box">
              <div class="home-inner-box">
                  <p>প্রমোটেড বিজ্ঞাপন <span class="count">{{ $data['promoted'] }}</span></p>
              </div>
            <p>ডাটাবেজে প্রমোটেড বিজ্ঞাপন এর সংখ্যা এখানে গণনা হচ্ছে। ডাটাবেজ থেকে সকল প্রমোটেড এখান থেকে পাওয়া যাবে। <br><a class="btn btn-success" href="{{ url('/promoted') }}">প্রমোটেড বিজ্ঞাপন দেখুন</a></p>
            </div>
          </div>
          <div class="col-md-4 col-xs-12 col-sm-4">
            <div class="home-box">
              <div class="home-inner-box">
                  <p>সর্বমোট বার্তা <span class="count">{{ $data['message'] }}</span></p>
              </div>
            <p>ডাটাবেজে সর্বমোট বার্তা এর সংখ্যা এখানে গণনা হচ্ছে। ডাটাবেজ থেকে সকল বার্তা এখান থেকে পাওয়া যাবে। <br><a class="btn btn-success" href="{{ url('/messages') }}">সকল বার্তা দেখুন</a></p>
            </div>
          </div> 
          <div class="col-md-4 col-xs-12 col-sm-4">
            <div class="home-box">
              <div class="home-inner-box">
                  <p>রিপোর্টেড বিজ্ঞাপন <span class="count">{{ $data['reports'] }}</span></p>
              </div>
            <p>ডাটাবেজে রিপোর্টেড বিজ্ঞাপন এর সংখ্যা এখানে গণনা হচ্ছে। ডাটাবেজ থেকে রিপোর্টেড বিজ্ঞাপন এখান থেকে পাওয়া যাবে। <br><a class="btn btn-success" href="{{ url('/reports') }}">রিপোর্টেড বিজ্ঞাপন দেখুন</a></p>
            </div>
          </div> 
          <div class="col-md-4 col-xs-12 col-sm-4">
            <div class="home-box">
              <div class="home-inner-box">
                  <p>যোগ করা এরিয়া সমূহ <span class="count">{{ $data['area'] }}</span></p>
              </div>
            <p>ডাটাবেজে যোগ করা এরিয়া সমূহ এর সংখ্যা এখানে গণনা হচ্ছে। ডাটাবেজ থেকে এরিয়া সমূহ এখান থেকে পাওয়া যাবে। <br><a class="btn btn-success" href="{{ url('/location/search') }}">এরিয়া খুঁজুন</a></p>
            </div>
          </div> 
          <div class="col-md-4 col-xs-12 col-sm-4">
            <div class="home-box">
              <div class="home-inner-box">
                  <p>সর্বমোট ব্যবহারকারী<span class="count">{{ $data['users'] }}</span></p>
              </div>
            <p>ডাটাবেজে সর্বমোট ব্যবহারকারী এর সংখ্যা এখানে গণনা হচ্ছে। ডাটাবেজ থেকে সকল ব্যবহারকারী এখান থেকে পাওয়া যাবে। <br><a class="btn btn-success" href="{{ url('/search/user') }}">ব্যবহারকারী খুঁজুন</a></p>
            </div>
          </div> 
          <div class="col-md-4 col-xs-12 col-sm-4">
            <div class="home-box">
              <div class="home-inner-box">
                  <p>সাময়িক বন্ধ ব্যবহারকারী <span class="count">{{ $data['suspended'] }}</span></p>
              </div>
            <p>ডাটাবেজে সাময়িক বন্ধ ব্যবহারকারী এর সংখ্যা এখানে গণনা হচ্ছে। ডাটাবেজ থেকে সাময়িক বন্ধ ব্যবহারকারী এখান থেকে পাওয়া যাবে। <br><a class="btn btn-success" href="{{ url('/suspended/user') }}">সাময়িক বন্ধ ব্যবহারকারী খুঁজুন</a></p>
            </div>
          </div> 
          <div class="col-md-4 col-xs-12 col-sm-4">
            <div class="home-box">
              <div class="home-inner-box">
                  <p>মডারেটর তালিকা <span class="count">{{ $data['moderator'] }}</span></p>
              </div>
            <p>ডাটাবেজে সর্বমোট মডারেটর এর তালিকা এখানে গণনা হচ্ছে। ডাটাবেজ থেকে সকল মডারেটর এখান থেকে পাওয়া যাবে। <br><a class="btn btn-success" href="{{ url('/moderator-list') }}">মডারেটর তালিকা দেখুন</a></p>
            </div>
          </div>
		  @endif
		  
		  @if(Auth::user()->status == 2)
          <div class="col-md-4 col-xs-12 col-sm-4">
            <div class="home-box">
              <div class="home-inner-box">
                  <p>সর্বমোট বিজ্ঞাপন <span class="count">{{ $data['totalAd'] }}</span></p>
              </div>
            <p>ডাটাবেজে সর্বমোট বিজ্ঞাপন এর সংখ্যা এখানে গণনা হচ্ছে। ডাটাবেজ থেকে সকল বিজ্ঞাপন এখান থেকে পাওয়া যাবে। <br><a class="btn btn-success" href="{{ url('/dashboard/ads') }}">সকল বিজ্ঞাপন দেখুন</a></p>
            </div>
          </div> 
          <div class="col-md-4 col-xs-12 col-sm-4">
            <div class="home-box">
              <div class="home-inner-box">
                  <p>অপ্রকাশিত বিজ্ঞাপন <span class="count">{{ $data['unpublished'] }}</span></p>
              </div>
            <p>ডাটাবেজে অপ্রকাশিত বিজ্ঞাপন এর সংখ্যা এখানে গণনা হচ্ছে। ডাটাবেজ থেকে অপ্রকাশিত বিজ্ঞাপন এখান থেকে পাওয়া যাবে। <br><a class="btn btn-success" href="{{ url('/pendings') }}">অপ্রকাশিত বিজ্ঞাপন দেখুন</a></p>
            </div>
          </div>  
          <div class="col-md-4 col-xs-12 col-sm-4">
            <div class="home-box">
              <div class="home-inner-box">
                  <p>প্রমোটেড বিজ্ঞাপন <span class="count">{{ $data['promoted'] }}</span></p>
              </div>
            <p>ডাটাবেজে প্রমোটেড বিজ্ঞাপন এর সংখ্যা এখানে গণনা হচ্ছে। ডাটাবেজ থেকে সকল প্রমোটেড এখান থেকে পাওয়া যাবে। <br><a class="btn btn-success" href="{{ url('/promoted') }}">প্রমোটেড বিজ্ঞাপন দেখুন</a></p>
            </div>
          </div>
          <div class="col-md-4 col-xs-12 col-sm-4">
            <div class="home-box">
              <div class="home-inner-box">
                  <p>সর্বমোট বার্তা <span class="count">{{ $data['message'] }}</span></p>
              </div>
            <p>ডাটাবেজে সর্বমোট বার্তা এর সংখ্যা এখানে গণনা হচ্ছে। ডাটাবেজ থেকে সকল বার্তা এখান থেকে পাওয়া যাবে। <br><a class="btn btn-success" href="{{ url('/messages') }}">সকল বার্তা দেখুন</a></p>
            </div>
          </div> 
          <div class="col-md-4 col-xs-12 col-sm-4">
            <div class="home-box">
              <div class="home-inner-box">
                  <p>রিপোর্টেড বিজ্ঞাপন <span class="count">{{ $data['reports'] }}</span></p>
              </div>
            <p>ডাটাবেজে রিপোর্টেড বিজ্ঞাপন এর সংখ্যা এখানে গণনা হচ্ছে। ডাটাবেজ থেকে রিপোর্টেড বিজ্ঞাপন এখান থেকে পাওয়া যাবে। <br><a class="btn btn-success" href="{{ url('/reports') }}">রিপোর্টেড বিজ্ঞাপন দেখুন</a></p>
            </div>
          </div> 
          <div class="col-md-4 col-xs-12 col-sm-4">
            <div class="home-box">
              <div class="home-inner-box">
                  <p>যোগ করা এরিয়া সমূহ <span class="count">{{ $data['area'] }}</span></p>
              </div>
            <p>ডাটাবেজে যোগ করা এরিয়া সমূহ এর সংখ্যা এখানে গণনা হচ্ছে। ডাটাবেজ থেকে এরিয়া সমূহ এখান থেকে পাওয়া যাবে। <br><a class="btn btn-success" href="{{ url('/location/search') }}">এরিয়া খুঁজুন</a></p>
            </div>
          </div> 
          <div class="col-md-4 col-xs-12 col-sm-4">
            <div class="home-box">
              <div class="home-inner-box">
                  <p>সর্বমোট ব্যবহারকারী<span class="count">{{ $data['users'] }}</span></p>
              </div>
            <p>ডাটাবেজে সর্বমোট ব্যবহারকারী এর সংখ্যা এখানে গণনা হচ্ছে। ডাটাবেজ থেকে সকল ব্যবহারকারী এখান থেকে পাওয়া যাবে। <br><a class="btn btn-success" href="{{ url('/search/user') }}">ব্যবহারকারী খুঁজুন</a></p>
            </div>
          </div>			  
		  @endif	  
		  
		  @if(Auth::user()->status == 3)
          <div class="col-md-4 col-xs-12 col-sm-4">
            <div class="home-box">
              <div class="home-inner-box">
                  <p>আমার সকল বিজ্ঞাপন <span class="count">{{ $data['myAd'] }}</span></p>
              </div>
            <p>আপনার সকল বিজ্ঞাপন এর সংখ্যা এখানে গণনা হচ্ছে। আপনার সকল বিজ্ঞাপন এখান থেকে পাওয়া যাবে। <br><a class="btn btn-success" href="{{ url('/my/ad') }}">সকল বিজ্ঞাপন দেখুন</a></p>
            </div>
          </div> 
          <div class="col-md-4 col-xs-12 col-sm-4">
            <div class="home-box">
              <div class="home-inner-box">
                  <p>প্রকাশিত বিজ্ঞাপন <span class="count">{{ $data['published'] }}</span></p>
              </div>
            <p>এডমিন প্যানেল থেকে প্রকাশিত আপনার বিজ্ঞাপন এর সংখ্যা এখানে গণনা হচ্ছে। এখান থেকে প্রকাশিত আপনার বিজ্ঞাপন সমূহ পাওয়া যাবে। <br><a class="btn btn-success" href="{{ url('/ad/published') }}">প্রকাশিত বিজ্ঞাপন দেখুন</a></p>
            </div>
          </div>  
          <div class="col-md-4 col-xs-12 col-sm-4">
            <div class="home-box">
              <div class="home-inner-box">
                  <p>প্রমোটেড বিজ্ঞাপন <span class="count">{{ $data['promoted'] }}</span></p>
              </div>
            <p>প্রমোটেড বিজ্ঞাপন এর সংখ্যা এখানে গণনা হচ্ছে। সকল প্রমোটেড বিজ্ঞাপন এখান থেকে পাওয়া যাবে। <br><a class="btn btn-success" href="{{ url('/promoted') }}">প্রমোটেড বিজ্ঞাপন দেখুন</a></p>
            </div>
          </div>  
          <div class="col-md-4 col-xs-12 col-sm-4">
            <div class="home-box">
              <div class="home-inner-box">
                  <p>ফেভারিট বিজ্ঞাপন <span class="count">{{ $data['favourite'] }}</span></p>
              </div>
            <p>ফেভারিট বিজ্ঞাপন এর সংখ্যা এখানে গণনা হচ্ছে। ডাটাবেজ থেকে সকল ফেভারিট বিজ্ঞাপন এখান থেকে পাওয়া যাবে। <br><a class="btn btn-success" href="{{ url('/ad/favourite') }}">ফেভারিট বিজ্ঞাপন দেখুন</a></p>
            </div>
          </div>  
          <div class="col-md-4 col-xs-12 col-sm-4">
            <div class="home-box">
              <div class="home-inner-box">
                  <p>অপ্রকাশিত বিজ্ঞাপন <span class="count">{{ $data['unpublishedByUser'] }}</span></p>
              </div>
            <p>অপ্রকাশিত বিজ্ঞাপন এর সংখ্যা এখানে গণনা হচ্ছে। সকল অপ্রকাশিত বিজ্ঞাপন এখান থেকে পাওয়া যাবে। <br><a class="btn btn-success" href="{{ url('/ad/unpublished') }}">অপ্রকাশিত বিজ্ঞাপন দেখুন</a></p>
            </div>
          </div>  
          <div class="col-md-4 col-xs-12 col-sm-4">
            <div class="home-box">
              <div class="home-inner-box">
                  <p>সংরক্ষিত বিজ্ঞাপন <span class="count">{{ $data['saved'] }}</span></p>
              </div>
            <p>সংরক্ষিত বিজ্ঞাপন এর সংখ্যা এখানে গণনা হচ্ছে। সকল সংরক্ষিত বিজ্ঞাপন এখান থেকে পাওয়া যাবে। <br><a class="btn btn-success" href="{{ url('/ad/unpublished') }}">সংরক্ষিত বিজ্ঞাপন দেখুন</a></p>
            </div>
          </div>			  
		  @endif		  
		  
        </div>
      </div>
      <!-- /.container-fluid -->
    </div>
<!-- /.content-wrapper -->
@endsection