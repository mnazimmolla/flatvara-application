@extends('template.dashboard.master')
@section('title')
সকল মডারেটর
@endsection
@section('content')
<!-- /.content-wrapper -->
    <div class="content-wrapper">
      <div class="container-fluid">
        <!-- Breadcrumbs -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="{{ url('/dashboard') }}">ড্যাশবোর্ড</a>
          </li>
          <li class="breadcrumb-item active">সকল মডারেটর</li>
        </ol>
        @if(Session::has('message'))
          <h3 class="text-center text-success">{{ Session::get('message') }}</h3>
        @endif
     <table class="table table-striped table-bordered">
      <thead>
        <tr>
        <th>ফেসবুক</th>
        <th>ব্যবহারকারী নাম</th>
        <th>ইমেইল</th>
        <th>জন্ম তারিখ</th>
        <th>বিস্তারিত</th>
        </tr>
      </thead>
      <tbody>
        @foreach($moderators as $moderator) 
        <tr>
        <td>{{ $moderator->facebook_id }}</td>
        <td>{{ $moderator->name }}</td>
        <td>{{ $moderator->email }}</td>
        <td>{{ $moderator->dob }}</td>
        <td>
          <a title="ফেভারিট থেকে মুছে ফেলুন" onclick="return confirm('মডারেটর থেকে মুছে ফেলুন')" class="btn" href="{{ url('moderator/delete/' . $moderator->id) }}"><i class="fa fa-trash"></i>
          </a>
        </td>
        </tr>
        @endforeach
      </tbody>
      </table>
      {{ $moderators->links() }}        
      
      </div>
      <!-- /.container-fluid -->
    </div>
<!-- /.content-wrapper -->
@endsection