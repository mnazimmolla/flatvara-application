@extends('template.dashboard.master')
@section('title')
বিজ্ঞাপন প্রমোট করুন
@endsection
@section('content')
<!-- /.content-wrapper -->
    <div class="content-wrapper">
      <div class="container-fluid">
        <!-- Breadcrumbs -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="{{ url('/dashboard') }}">ড্যাশবোর্ড</a>
          </li>
          <li class="breadcrumb-item active">বিজ্ঞাপন প্রমোট করুন</li>
        </ol>
		 <table class="table table-striped table-bordered">
			<thead>
			  <tr>
				<th>শ্রেণী</th>
				<th>শিরোনাম</th>
				<th>ভাড়া/বিক্রি</th>
				<th>প্রমোট করুন</th>
			  </tr>
			</thead>
			<tbody>
			  @foreach($ads as $ad)	
			  <tr>
				<td>{{ $ad->category }}</td>
				<td>{{ $ad->title }}</td>
				<td>{{ ($ad->price) ? $ad->price : $ad->rent }}৳</td>
				<td>
					<a title="বিজ্ঞাপন প্রমোট করুন" class="btn" href="{{ url('/ad/promote/'.$ad->id) }}"><i class="fa fa-share"></i>
					</a>
				</td>
			  </tr>
			  @endforeach
			</tbody>
		  </table>
			{{ $ads->links() }}		 		
      </div>
      <!-- /.container-fluid -->
    </div>
<!-- /.content-wrapper -->
@endsection