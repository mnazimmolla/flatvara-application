@extends('template.dashboard.master')
@section('title')
  ব্যবহারকারী খুঁজুন
@endsection
@section('content')
<!-- /.content-wrapper -->
    <div class="content-wrapper">
      <div class="container-fluid">
        <!-- Breadcrumbs -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="{{ url('/dashboard') }}">ড্যাশবোর্ড</a>
          </li>
          <li class="breadcrumb-item active">ব্যবহারকারী খুঁজুন</li>
        </ol>
    		<div class="search_part">
    			<div class="row">
    				<div class="col-md-3">
    				</div>
    				<div class="col-md-6 search_part_mail">
              			<form method="POST" action="{{ url('/search/user') }}" accept-charset="utf-8">
                			<div class="form-group">
    							<label for="user_cat_serach">ব্যবহারকারী খুঁজুন</label>
    							<input type="text" name="user_cat_serach" id="user_cat_serach" class="form-control" placeholder="ব্যবহারকারীর তথ্য" />
    						</div>
    						<div class="radio">
    						  <label><input type="radio" name="serach_base" id="serach_base" value="1" checked>ফোন </label>
    						  <label><input type="radio" name="serach_base" id="serach_base" value="2" checked>ইমেইল </label>
    						  <label><input type="radio" name="serach_base" id="serach_base" value="3" checked>ইউজারনেম </label>
    						  <label><input type="radio" name="serach_base" id="serach_base" value="4" checked>নাম </label>
    						</div>
               				 <input type="hidden" name="_token" value="{{ csrf_token() }}">
    						<button type="submit" class="btn">ব্যবহারকারী খুঁজুন</button>
    					</form>    					
    				</div>
    				<div class="col-md-3">
    				</div>
    			</div>
    		</div>      
     @if($user)
     <table class="table table-striped table-bordered" style="margin-top: 10px;">
	      <thead>
	        <tr>
	          <th>ফেসবুক আইডি</th>
	          <th>নাম</th>
	          <th>ইমেইল</th>
	          <th>জন্মদিন</th>
	          <th>স্ট্যাটাস</th>
	          <th>ব্লক/আনব্লক</th>
	        </tr>
	      </thead>
	      <tbody>  
	      	<td>{{ $user->facebook_id }}</td>                              
	      	<td>{{ $user->name }}</td>                              
	      	<td>{{ $user->email }}</td>                             
	      	<td>{{ $user->dob }}</td>                                
	      	<td>{{ ($user->status == 3 ) ? 'একটিভ' : 'ব্লকড' }}</td>                             
	      	<td>
	      		@if($user->status == 3)
	      		<a title="ব্লক করে দিন" onclick="return confirm('ব্লক করে দিন')" class="btn" href="{{ url('/block-user/' . $user->id) }}">
	      			<i class="fa fa-lock text-danger"></i>
          		</a>
          		@else
	      		<a title="আনব্লক করে দিন" onclick="return confirm('আনব্লক করে দিন')" class="btn" href="{{ url('/unblock-user/' . $user->id) }}">
	      			<i class="fa fa-unlock-alt text-warning"></i>
          		</a>
          		@endif
      		</td>                              
	      </tbody>
      </table>
      @else
      <p class="text-center text-danger">কোন ব্যবহারকারী খুঁজে পাওয়া যায়নি</p>
      @endif
      </div>
      <!-- /.container-fluid -->
    </div>
<!-- /.content-wrapper -->
@endsection