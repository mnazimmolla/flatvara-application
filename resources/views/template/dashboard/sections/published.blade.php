@extends('template.dashboard.master')
@section('title')
  প্রকাশিত বিজ্ঞাপন সমূহ
@endsection
@section('content')
<!-- /.content-wrapper -->
    <div class="content-wrapper">
      <div class="container-fluid">
        <!-- Breadcrumbs -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="{{ url('/dashboard') }}">ড্যাশবোর্ড</a>
          </li>
          <li class="breadcrumb-item active">প্রকাশিত বিজ্ঞাপন সমূহ</li>
        </ol>
        @if(Session::has('message'))
          <h3 class="text-center text-success">{{ Session::get('message') }}</h3>
        @endif        
		 <table class="table table-striped table-bordered">
			<thead>
			  <tr>
				<th>শ্রেণী</th>
				<th>শিরোনাম</th>
				<th>ভাড়া/বিক্রি</th>
				<th>বিস্তারিত</th>
			  </tr>
			</thead>
			<tbody>
			  @foreach($ads as $ad)	
			  <tr>
				<td>{{ $ad->category }}</td>
				<td>{{ $ad->title }}</td>
				<td>{{ ($ad->price) ? $ad->price : $ad->rent }}৳</td>
				<td>
					<a title="বিস্তারিত দেখুন" class="btn" href="{{ url('/ad/view/'.$ad->id) }}"><i class="fa fa-eye"></i>
					</a>
					<a title="এডিট করুন" class="btn" href="{{ url('/ad/edit/'.$ad->id) }}"><i class="fa fa-edit"></i></a>
					<a title="সংরক্ষণ করুন" class="btn" href="{{ url('/ad/save/'.$ad->id . '/' . $ad->user_id) }}"><i class="fa fa-save"></i></a> 
				    <a title="ডিলিট করুন" onclick="return confirm('ডিলিট করুন')" class="btn" href="{{ url('/ad/delete/'.$ad->id) }}"><i class="fa fa-trash"></i></a>
				</td>
			  </tr>
			  @endforeach
			</tbody>
		  </table>
			{{ $ads->links() }}		 		
      </div>
      <!-- /.container-fluid -->
    </div>
<!-- /.content-wrapper -->
@endsection