@extends('template.dashboard.master')
@section('title')
{{ $ad->add_title }}
@endsection
@section('content')
<!-- /.content-wrapper -->
    <div class="content-wrapper">
      <div class="container-fluid">
        <!-- Breadcrumbs -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="{{ url('/dashboard') }}">ড্যাশবোর্ড</a>
          </li>
          <li class="breadcrumb-item active">{{ $ad->add_title }}</li>
        </ol>
        <!-- Flat Item Part Start -->
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="flat-single" id="print">
                    	<div class="text-right dash-button">
							  <a class="btn" href="#">চ্যাট করুন</a>
							  <a class="btn" href="{{ url('ad/report/delete/' . $ad->id) }}">মুছে ফেলুন</a>
						 </div>
						 <div class="report">
		              		<p>
		                		{{ $ad->report }}
		              		</p>
		           		 </div>
                        <div class="row">
                            <div class="col-md-8 col-sm-8 col-xs-12 flat-image">
                                <div id="jssor_1" class="jssor_first">
                                    <!-- Loading Screen -->
                                    <div data-u="loading" class="jssorl-009-spin loading_jssor">
                                        <img class="jssor_spin" src="{{ asset('public/assets/img/flat/') }}/spin.svg" />
                                    </div>
                                    <div data-u="slides" class="sliders_style">

                                        <div data-p="150.00">
                                            <img data-u="image" src="{{ asset('public/assets/img/flat/') }}/{{$ad->main_img}}" />
                                            <img data-u="thumb" src="{{ asset('public/assets/img/flat/') }}/{{ $ad->main_img }}" />
                                        </div> 
                                        @if($ad->img_two)
                                        <div data-p="150.00">
                                            <img data-u="image" src="{{ asset('public/assets/img/flat/') }}/{{$ad->img_two}}" />
                                            <img data-u="thumb" src="{{ asset('public/assets/img/flat/') }}/{{ $ad->img_two }}" />
                                        </div>                                         
                                        @endif 
                                        @if($ad->img_three)
                                        <div data-p="150.00">
                                            <img data-u="image" src="{{ asset('public/assets/img/flat/') }}/{{$ad->img_three}}" />
                                            <img data-u="thumb" src="{{ asset('public/assets/img/flat/') }}/{{ $ad->img_three }}" />
                                        </div>                                         
                                        @endif 
                                        @if($ad->img_four)
                                        <div data-p="150.00">
                                            <img data-u="image" src="{{ asset('public/assets/img/flat/') }}/{{$ad->img_four}}" />
                                            <img data-u="thumb" src="{{ asset('public/assets/img/flat/') }}/{{ $ad->img_four }}" />
                                        </div>                                         
                                        @endif 
                                        @if($ad->img_five)
                                        <div data-p="150.00">
                                            <img data-u="image" src="{{ asset('public/assets/img/flat/') }}/{{$ad->img_five}}" />
                                            <img data-u="thumb" src="{{ asset('public/assets/img/flat/') }}/{{ $ad->img_five }}" />
                                        </div>                                         
                                        @endif 
                                        @if($ad->img_six)
                                        <div data-p="150.00">
                                            <img data-u="image" src="{{ asset('public/assets/img/flat/') }}/{{$ad->img_six}}" />
                                            <img data-u="thumb" src="{{ asset('public/assets/img/flat/') }}/{{ $ad->img_six }}" />
                                        </div>                                         
                                        @endif
                                       
                                    </div>
                                    <!-- Thumbnail Navigator -->
                                    <div data-u="thumbnavigator" class="jssort101  jssor_navigate"data-autocenter="2" data-scale-left="0.75">
                                        <div data-u="slides">
                                            <div data-u="prototype" class="p" style="width:99px;height:66px;">
                                                <div data-u="thumbnailtemplate" class="t"></div>
                                                <svg viewbox="0 0 16000 16000" class="cv">
                                                    <circle class="a" cx="8000" cy="8000" r="3238.1"></circle>
                                                    <line class="a" x1="6190.5" y1="8000" x2="9809.5" y2="8000"></line>
                                                    <line class="a" x1="8000" y1="9809.5" x2="8000" y2="6190.5"></line>
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Arrow Navigator -->
                                    <div data-u="arrowleft" class="jssora093 arrowleft_jssor" data-autocenter="2">
                                        <svg viewbox="0 0 16000 16000" class="svg_style">
                                            <circle class="c" cx="8000" cy="8000" r="5920"></circle>
                                            <polyline class="a" points="7777.8,6080 5857.8,8000 7777.8,9920 "></polyline>
                                            <line class="a" x1="10142.2" y1="8000" x2="5857.8" y2="8000"></line>
                                        </svg>
                                    </div>
                                    <div data-u="arrowright" class="jssora093 arrowright_jssor" data-autocenter="2">
                                        <svg viewbox="0 0 16000 16000" class="svg_style">
                                            <circle class="c" cx="8000" cy="8000" r="5920"></circle>
                                            <polyline class="a" points="8222.2,6080 10142.2,8000 8222.2,9920 "></polyline>
                                            <line class="a" x1="5857.8" y1="8000" x2="10142.2" y2="8000"></line>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 flat-desciption">
                                <table class="table">
                                    <tr>
                                        <td></td>
                                        <td>
                                            <a href="{{ url('ad/report/view/' . $ad->id) }}"><h4>{{ $ad->add_title }}</h4></a>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td class="flat-item-fa">
                                            <i class="fa fa-home" aria-hidden="true">
                                        </td>
                                        <td>
                                            {{ $ad->area }}, {{ $ad->thana }}, {{ $ad->district }}।
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="flat-item-fa">
                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                        </td>
                                        <td>
                                            {{ $ad->full_address }}
                                        </td>
                                    </tr>
                                    @if(($ad->class_add == 2) || ($ad->class_add == 3) || ($ad->class_add == 4) || ($ad->class_add == 5) || ($ad->class_add == 6))
                                    <tr>
                                        <td class="flat-item-fa">
                                            <i class="fa fa-bed" aria-hidden="true">
                                        </td>
                                        <td>
                                            বেড <span>{{ $ad->bed_room }}</span>, বাথরুম <span>{{($ad->bath_room_attach === 2) ? "এটাস্টড" : $ad->bath_room}}</span>
                                            @if($ad->sq_feet)
                                                , বর্গফুট
                                             <span>{{ $ad->sq_feet }}</span>
                                            @endif
                                        </td>
                                    </tr>
                                    @endif
                                    @if($ad->class_add == 11)
                                    @if($ad->sq_feet)
                                    <tr>
                                        <td class="flat-item-fa">
                                            <i class="fa fa-bed" aria-hidden="true">
                                        </td>
                                        <td> বর্গফুট
                                             <span>{{ $ad->sq_feet }}</span>
                                        </td>
                                    </tr>
                                    @endif
                                    @endif         
                                    
                                    @if($ad->class_add != 9)
                                    @if($ad->sell_rent_basis)
                                    @if($ad->rent_date_basis)
                                    <tr>
                                        <td class="flat-item-fa">
                                            <i class="fa fa-calendar" aria-hidden="true">
                                        </td>
                                        <td>

                                            ভাড়া শুরুর তারিখ : 
                                           {{ ($ad->anytime != null) ? "যেকোন সময়" : $ad->rent_date_basis}}                                              
                                        </td>
                                    </tr>
                                    @endif
                                    @endif
                                    @endif

                                    @if($ad->class_add === 1)
                                    <tr>
                                        <td class="flat-item-fa">
                                            <i class="fa fa-calendar" aria-hidden="true">
                                        </td>
                                        <td>
                                           জমির ধরনঃ
                                           <span>{{ $ad->land_basis }}, </span> জমির আয়তনঃ
                                           <span>{{ $ad->land_area }} {{ $ad->unit }}</span>                                             
                                        </td>
                                    </tr>
                                    @endif
                                    <tr>
                                        <td class="flat-item-fa">
                                            <i class="fa fa-gg" aria-hidden="true"></i>
                                        </td>
                                        <td>
                                            @if($ad->sell_rent_basis)
                                            ভাড়া 
                                            @if($ad->rent_basis !== "আলোচনা সাপেক্ষে")
                                                {{ $ad->sell_rent_basis }}৳/
                                            @endif
                                                @if($ad->rent_basis)
                                                    @if($ad->class_add != 9)
                                                    {{ $ad->rent_basis}}
                                                    @endif
                                                @endif

                                                @if($ad->advance_cat)
                                                , অগ্রীম ভাড়া {{ $ad->advance_cat }}
                                                @endif
                                            @else
                                            বিক্রি {{ $ad->sell_price_basis }}৳
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="flat-item-fa">
                                            <i class="fa fa-phone" aria-hidden="true"></i>
                                        </td>
                                        <td>
                                            যোগাযোগঃ 
                                            <span style="font-weight:900;">
                                                @if($ad->mobile2)
                                                    {{ $ad->mobile2 }}
                                                @endif
                                                @if($ad->another_mobile_2)
                                                    {{ ",".$ad->another_mobile_2 }}
                                                @endif
                                                @if($ad->another_mobile_3)
                                                    {{ ",".$ad->another_mobile_3 }}
                                                @endif

                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="flat-item-fa">
                                            <i class="fa fa-cog" aria-hidden="true"></i>
                                        </td>
                                        @if (Auth::check())
                                            <?php $user_id = Auth::user()->id; ?>
                                        @endif
                                        <td class="single-option">
                                            <a href="#">চ্যাট করুন</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="flat-item-fa">
                                            <i class="fa fa-share-alt" aria-hidden="true"></i>
                                        </td>
                                        <td class="add-share">শেয়ার করুন
                                            <span style="font-weight:900;">
                                              <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                              <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                              <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                         </span>
                                        </td>
                                    </tr>
                                </table>

                            </div>
                        </div>
                        <div class="single-bottom-desciption">
                            <br/>
                            <div class="row">
                                <div class="col-md-12 col-xs-12 col-xs-12">
                                @if(($ad->class_add == 2 && $ad->sell_rent_basis) || ($ad->class_add == 3 && $ad->sell_rent_basis) || ($ad->class_add == 4 && $ad->sell_rent_basis))
                                 
                                    <span style="font-weight:bold;">যাদের কাছে ভাড়া দিতে আগ্রহীঃ </span>
                                    <p class="inline-text {{ ($ad->bachelor != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp ব্যাচেলর</p>

                                    <p class="inline-text {{ ($ad->female != null) ? '' : 'fa-text' }}"><i class="fa  fa-check-circle" aria-hidden="true"></i>&nbsp নারী</p>

                                    <p class="inline-text {{ ($ad->huswife != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp স্বামী-স্ত্রী</p>

                                    <p class="inline-text {{ ($ad->student != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp ছাত্র</p>

                                    <p class="inline-text {{ ($ad->jobholder != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp চাকুরীজীবী</p>

                                    <p class="inline-text {{ ($ad->business_holder != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp ব্যবসায়ী</p>

                                @endif  
                                                                                           
                                @if(($ad->class_add == 1) || ($ad->class_add == 2) || ($ad->class_add == 10) || ($ad->class_add == 11) )
                                    <h3>Hoilo na</h3>
                                @else
                                    <br>
                                    <span style="font-weight:bold;">সুবিধাসমূহঃ </span>

                                    <p class="inline-text {{ ($ad->ac != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp এয়ার কন্ডিশন</p>

                                    <p class="inline-text {{ ($ad->balcony != null) ? '' : 'fa-text' }}"><i class="fa  fa-check-circle" aria-hidden="true"></i>&nbsp ব্যালকনি</p>

                                    <p class="inline-text {{ ($ad->cab_tv != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp ক্যাবল টিভি</p>

                                    <p class="inline-text {{ ($ad->grill != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp গ্রিল</p>

                                    <p class="inline-text {{ ($ad->electricity != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp বিদ্যুৎ</p>

                                    <p class="inline-text {{ ($ad->water != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp পানি</p>

                                    <p class="inline-text {{ ($ad->cctv != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp সিসিটিভি</p>
                                       
                                     <p class="inline-text {{ ($ad->wifi != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp ওয়াইফাই/ইন্টারনেট</p>

                                    <p class="inline-text {{ ($ad->lift != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp লিফট</p>

                                    <p class="inline-text {{ ($ad->parking != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp পার্কিং</p>

                                    <p class="inline-text {{ ($ad->gas != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp গ্যাস</p>

                                    <p class="inline-text {{ ($ad->generator != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp জেনারেটর</p>

                                    <p class="inline-text {{ ($ad->sec_guard != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp সিকুরিটি গার্ড</p>

                                    <p class="inline-text {{ ($ad->tiles != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp টাইলস</p>

                                    <p class="inline-text {{ ($ad->mosaic != null) ? '' : 'fa-text' }}"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp মোজাইক</p>  
                                </div>
                                @endif
                                <div class="col-md-12 col-xs-12 col-xs-12">
                                    <p class="text-justify">
                                        <span style="font-weight:bold;">বিস্তারিতঃ</span>
                                        {!! $ad->add_details !!}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Flat Item Part End -->		   
      </div>
      <!-- /.container-fluid -->
    </div>
<!-- /.content-wrapper -->
@endsection