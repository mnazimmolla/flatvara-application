@extends('template.dashboard.master')
@section('title')
    বার্তা
@endsection
@section('content')
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <div class="container-fluid">
            <!-- Breadcrumbs -->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ url('/dashboard') }}">ড্যাশবোর্ড</a>
                </li>
                <li class="breadcrumb-item active">বার্তা</li>
            </ol>
            <div class="text-right dash-button">
                <a href="{{ url('messages') }}"  class="btn">সকল ম্যাসেজ দেখুন</a>
                <a href="{{ url('/message/delete/' . $message->id) }}" class="btn">ইমেল মুছে ফেলুন</a>
            </div>
            <p>প্রেরক: <span>{{ $message->name }}</span></p>
            <p>বিষয়: <span>{{ $message->subject }}</span></p>
            <p>ইমেইল: <span>{{ $message->email }}</span></p>
            <p>বার্তা: <span>{{ $message->message }}</span></p>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-wrapper -->
@endsection