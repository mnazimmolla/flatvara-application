@extends('template.dashboard.master')
@section('title')
	বিজ্ঞাপন প্রকাশ করুন
@endsection
@section('content')
<!-- /.content-wrapper -->
    <div class="content-wrapper">
      <div class="container-fluid">
        <!-- Breadcrumbs -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="{{ url('/dashboard') }}">ড্যাশবোর্ড</a>
          </li>
          <li class="breadcrumb-item active">বিজ্ঞাপন প্রকাশ করুন</li>
        </ol>
         <!-- Create Add Part Start -->
   <div class="container create-add">
   		<form id="form1" method="POST" action="{{ url('ad/create') }}" enctype="multipart/form-data" accept-charset="utf-8">
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-12">
				  <div class="form-group {{ $errors->has('class_add') ? 'has-error' : '' }}">
				    <label for="class_add">শ্রেণী নির্বাচন করুন</label>
					<select required id="class_add" name="class_add" class="form-control">
					  <option selected disabled>শ্রেণী নির্বাচন করুন</option>
					  @foreach($categories as $category)
					  <option value="{{ $category->id }}">{{ $category->category }}</option>
					  @endforeach
					</select>
                        @if ($errors->has('class_add'))
                            <span class="text-danger">
                                <strong>শ্রেণী নির্বাচন করুন</strong>
                            </span>
                        @endif  					
				  </div>
				  <div class="form-group" id="others_cat">
				    <label for="otherCat">প্রপার্টি নির্বাচন করুন</label>
					<select id="otherCat" name="otherCat" class="form-control">
					  <option selected disabled>প্রপার্টি নির্বাচন করুন</option>
					  @foreach($sub_categories as $sub_category)
					  <option value="{{ $sub_category->id }}">{{ $sub_category->sub_category }}</option>
					  @endforeach
					</select>				  
				  </div>				  
				  <div class="form-group {{ $errors->has('division') ? 'has-error' : '' }}">
				    <label for="division">বিভাগ নির্বাচন করুন</label>
					<select id="division" name="division" class="form-control">
					  <option selected disabled>বিভাগ নির্বাচন করুন</option>
					  @foreach($divisions as $division)
					  <option value="{{ $division->id }}">{{ $division->bn_name }}</option>
					  @endforeach
					</select>
                        @if ($errors->has('division'))
                            <span class="text-danger">
                                <strong>বিভাগ নির্বাচন করুন</strong>
                            </span>
                        @endif 					
				  </div>
				  <div class="form-group {{ $errors->has('district') ? 'has-error' : '' }}">
				    <label for="district">জেলা নির্বাচন করুন</label>
					<select id="district" name="district" class="form-control">
						<option disabled selected>জেলা নির্বাচন করুন</option>
					</select>
                        @if ($errors->has('district'))
                            <span class="text-danger">
                                <strong>জেলা নির্বাচন করুন</strong>
                            </span>
                        @endif 					
				  </div>
				  <div class="form-group {{ $errors->has('thana') ? 'has-error' : '' }}">
				    <label for="upazila">উপজেলা/থানা নির্বাচন করুন</label>
					<select id="upazila" name="thana" class="form-control">
						<option disabled selected>উপজেলা/থানা নির্বাচন করুন</option>
					</select>
                        @if ($errors->has('thana'))
                            <span class="text-danger">
                                <strong>উপজেলা/থানা নির্বাচন করুন</strong>
                            </span>
                        @endif 						
				  </div>
				  <div class="form-group {{ $errors->has('area') ? 'has-error' : '' }}">
				    <label for="area">এলাকা নির্বাচন করুন</label>
					<input type="text" id="area" name="area" class="form-control" placeholder="এলাকা নির্বাচন করুন">
                        @if ($errors->has('area'))
                            <span class="text-danger">
                                <strong>এলাকা নির্বাচন করুন</strong>
                            </span>
                        @endif 					
				  </div>
				  <div class="form-group {{ $errors->has('add_title') ? 'has-error' : '' }}">
				  	<label for="add_title">বিজ্ঞাপনের শিরোনাম</label>
				  	<input type="text" name="add_title" id="add_title" placeholder="সংক্ষিপ্ত রাখুন" class="form-control">
                        @if ($errors->has('add_title'))
                            <span class="text-danger">
                                <strong>বিজ্ঞাপনের শিরোনাম দিন</strong>
                            </span>
                        @endif 				  	
				  </div>
				  <div class="form-group {{ $errors->has('add_title') ? 'has-error' : '' }}">
				  	<label for="full_address">পূর্ণাঙ্গ ঠিকানা</label>
				  	<textarea id="full_address" class="form-control" name="full_address" placeholder="রাস্তা, বাড়ির নাম, বাড়ির নম্বর অথবা পোষ্ট কোড।"></textarea>
                        @if ($errors->has('full_address'))
                            <span class="text-danger">
                                <strong>পূর্ণাঙ্গ ঠিকানা দিন</strong>
                            </span>
                        @endif				  	
				  </div>
				  <div class="form-group">
				  	<label for="add_details">বিস্তারিত</label>
				  	<textarea class="form-control" rows="8" name="add_details" placeholder="বেশি তথ্য= বেশি সংখ্যক সাড়া!" id="add_details"></textarea>
				  </div>
				  
				  <div class="form-group" id="land_cat">
					<label for="land_basis">প্লট/জমির ধরন</label><br>
				  	<input type="radio" name="land_basis" id="land_basis" value="কৃষি"> কৃষি
				  	<input type="radio" name="land_basis" id="land_basis" value="বাণিজ্যিক"> বাণিজ্যিক
				  	<input type="radio" name="land_basis" id="land_basis" value="আবাসিক"> আবাসিক
				  	<input type="radio" name="land_basis" id="land_basis" value="অন্যান্য"> অন্যান্য
				  </div>
				  <div class="form-group" id="unit_cat">
					  <div class="row">
						<div class="col-md-6">
						<label for="unit">ইউনিট</label>
						<select id="unit" name="unit" class="form-control">
						  <option selected disabled>একটি ইউনিট নির্বাচন করুন। </option>
						  <option value="কাঠা">কাঠা</option>
						  <option value="বিঘা">বিঘা</option>
						  <option value="একর">একর</option>
						  <option value="শতক">শতক</option>
						  <option value="ডেসিমাল">ডেসিমাল</option>
						</select>					
						</div>
						<div class="col-md-6">
						<label for="land_area">জমির আয়তন</label>
						<input type="text" name="land_area" id="land_area" placeholder="জমির আঁকার  বা আয়তন লিখুন" class="form-control">					
						</div>					
					  </div>
				  </div>
				  
				  <button type="button" style="margin-bottom:12px;" id="rent">ভাড়া দিন</button>
				  <button type="button" style="margin-bottom:12px;" id="sell">বিক্রি করুন </button>
				  <div class="form-group" id="rent_cat">
				  	<label for="sell_rent_basis">ভাড়ার পরিমাণ</label>
				  	<input type="text" name="sell_rent_basis" id="sell_rent_basis" placeholder="ভাড়ার জন্য একটি আকর্ষণীয় রেট দিন।" class="form-control">
				  	<input type="radio" name="rent_basis" id="rent_basis" value="মাসিক"> মাসিক
				  	<input type="radio" name="rent_basis" id="rent_basis" value="দৈনিক"> দৈনিক
				  	<input type="radio" name="rent_basis" id="rent_basis" value="বাৎসরিক"> বাৎসরিক
				  	<input type="radio" name="rent_basis" id="rent_basis" value="আলোচনা সাপেক্ষে"> আলোচনা সাপেক্ষে<br/>
					<label for="rent_date_basis">ভাড়া শুরুর তারিখ</label>
					<input type="text" class="form-control datepicker" id="rent_date_basis" name="rent_date_basis" placeholder="তারিখ নির্বাচন করুন">
					<input type="checkbox" name="anytime" id="anytime" value="1"/> যেকোন সময়
					
					<div id="advance">
						<br>
						<label for="advance_cat">অগ্রীম ভাড়া</label>
						<select id="advance_cat" name="advance_cat" class="form-control">
						  <option value="0" selected disabled>অগ্রীম ভাড়ার মাস নির্বাচন করুন</option>
						  <option value="১ মাস">১ মাস</option>
						  <option value="২ মাস">২ মাস</option>
						  <option value="৩ মাস">৩ মাস</option>
						  <option value="৪ মাস">৪ মাস</option>
						  <option value="৫ মাস">৫ মাস</option>
						  <option value="৬ মাস">৬ মাস</option>
						  <option value="১ বছর">১ বছর</option>
						  <option value="২ বছর">২ বছর</option>
						  <option value="৩ বছর">৩ বছর</option>
						  <option value="আলোচনা সাপেক্ষে">আলোচনা সাপেক্ষে</option>
						</select>
					</div>					
				  </div>
				  <div class="form-group" id="sell_cat">
				  	<label for="sell_price_basis">দাম</label>
				  	<input type="text" name="sell_price_basis" id="sell_price_basis" placeholder="বিক্রির জন্য একটি আকর্ষণীয় দাম দিন।" class="form-control">
				  </div>					  
				  <div class="form-group" id="sqr_for_cat_1">
				  	<label for="sq_feet">বর্গফুট</label>
				  	<input type="text" class="form-control" id="sq_feet" name="sq_feet" placeholder="বর্গফুট জানা থাকলে উল্লেখ করে দিন।">
				  </div>
				  <div class="form-group" id="bed">
				  	<label for="bed_room">বেডরুম</label>
				  	<input type="text" class="form-control" id="bed_room" name="bed_room" placeholder="বেডরুমের সংখ্যা উল্লেখ করে দিন।">
				  </div>
				  <div class="form-group" id="room_cat_id">
					<label for="room_cat">বেড/রুমের ধরণ  <label>
					<input type="radio" name="room_cat" id="room_cat" value="1"> সিঙ্গেল				  
					<input type="radio" name="room_cat" id="room_cat" value="2"> ডাবল				  
				  </div>				  
				  <div class="form-group" id="bath">
				  	<label for="bath_room">বাথরুম</label>
				  	<input type="text" class="form-control" id="bath_room" name="bath_room" placeholder="বাথরুমের সংখ্যা উল্লেখ করে দিন।">
					<div id="bath_room_diff">
						<input type="checkbox" name="bath_room_attach" id="bath_room_attach" value="2"> এটেস্টেড
					</div>					
				  </div>
				  <!-- <p>আপনার মোবাইল নং: 01717324345</p> -->
				  <button type="button" style="margin-bottom:12px; display:block;" id="mobile_add">মোবাইল নং যুক্ত করুন</button>
				  <div class="form-group" id="another_mobile">
				  	<label for="mobile2">মোবাইল নং</label>
				  	<input type="text" class="form-control" id="mobile2" name="mobile2" placeholder="বিজ্ঞাপনে প্রদর্শন করার জন্য ফোন নাম্বার নির্বাচন করুন।">
				  </div>
				  <button type="button" style="margin-bottom:12px;" id="mobile_add_2">আরেকটি মোবাইল নং যুক্ত করুন </button>
				  <div class="form-group" id="another_mobile_id">
				  	<label for="another_mobile_2">মোবাইল নং</label>
				  	<input type="text" class="form-control" id="another_mobile_2" name="another_mobile_2" placeholder="সর্বোচ্চ ৩ টি">
				  </div>
				  <button type="button" style="margin-bottom:12px;" id="mobile_add_3">আরেকটি মোবাইল নং যুক্ত করুন</button>
				  <div class="form-group" id="another_mobile_id_3">
				  	<label for="another_mobile_3">মোবাইল নং</label>
				  	<input type="text" class="form-control" id="another_mobile_3" name="another_mobile_3" placeholder="সর্বোচ্চ ৩ টি">
				  </div>				  
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<br>
				  <div class="form-group">
				    <label for="main_img">প্রধান ছবি নির্বাচন</label>
				    <input type="file" id="main_img" name="main_img">
					<img id="main" src="#" alt="your image" />
				  </div> 
				  <div class="form-group" id="imgtwo">
				    <label for="img_two">ছবি নির্বাচন করুন</label>
				    <input type="file" id="img_two" name="img_two">
					<img id="main_two" src="#" alt="your image" />
				  </div>
				  <div class="form-group" id="imgthree">
				    <label for="img_three">ছবি নির্বাচন করুন</label>
				    <input type="file" id="img_three" name="img_three">
					<img id="main_three" src="#" alt="your image" />
				  </div>
				  <div class="form-group" id="imgfour">
				    <label for="img_four">ছবি নির্বাচন করুন</label>
				    <input type="file" id="img_four" name="img_four">
					<img id="main_four" src="#" alt="your image" />
				  </div>
				  <div class="form-group" id="imgfive">
				    <label for="img_five">ছবি নির্বাচন করুন</label>
				    <input type="file" id="img_five" name="img_five">
					<img id="main_five" src="#" alt="your image" />
				  </div>
				  <div class="form-group" id="imgsix">
				    <label for="img_six">ছবি নির্বাচন করুন</label>
				    <input type="file" id="img_six" name="img_six">
					<img id="main_six" src="#" alt="your image" />
				  </div>
				  <button type="button" id="add" class="href" style="margin-bottom:12px; display:block;" id="mobile_add">নতুন ছবি যোগ করুন</button>
				  <hr>
				   <button type="button" style="margin-bottom:12px;" id="details_info">সুবিধা সমূহ যোগ করুন</button>
				   <div id="details_form">
				  <div class="form-group">
					<label for="ac" class="checkbox-inline">
					  <input type="checkbox" id="ac" name="ac" value="1"> এয়ার কন্ডিশন
					</label>
					<label for="balcony" class="checkbox-inline">
					  <input type="checkbox" id="balcony" name="balcony" value="1"> ব্যালকনি
					</label>
					<label for="cab_tv" class="checkbox-inline">
					  <input type="checkbox" id="cab_tv" name="cab_tv" value="1"> ক্যাবল টিভি
					</label>
					<label for="grill" class="checkbox-inline">
					  <input type="checkbox" id="grill" name="grill" value="1"> গ্রিল
					</label>
				  </div>
				  <div class="form-group">
					<label for="electricity" class="checkbox-inline">
					  <input type="checkbox" id="electricity" name="electricity" value="1"> বিদ্যুত
					</label>
					<label for="water" class="checkbox-inline">
					  <input for="" type="checkbox" id="water" name="water" value="1"> পানি
					</label>
					<label for="cctv" class="checkbox-inline">
					  <input type="checkbox" id="cctv" name="cctv" value="1"> সিসিটিভি
					</label>
					<label for="wifi" class="checkbox-inline">
					  <input type="checkbox" id="wifi" name="wifi" value="1"> ওয়াইফাই/ইন্টারনেট
					</label>
				  </div>
				  <div class="form-group">
					<label for="lift" class="checkbox-inline">
					  <input type="checkbox" id="lift" name="lift" value="1"> লিফট
					</label>
					<label for="parking" class="checkbox-inline">
					  <input type="checkbox" id="parking" name="parking" value="1"> পার্কিং
					</label>
					<label for="gas" class="checkbox-inline">
					  <input type="checkbox" id="gas" name="gas" value="1"> গ্যাস
					</label>
				  </div>
				  <div class="form-group">
					<label for="generator" class="checkbox-inline">
					  <input type="checkbox" id="generator" name="generator" value="1"> জেনারেটর
					</label>
					<label for="sec_guard" class="checkbox-inline">
					  <input type="checkbox" id="sec_guard" name="sec_guard" value="1"> সিকুরিটি গার্ড
					</label>
				  </div>
				  <div class="form-group">
					<label for="tiles" class="checkbox-inline">
					  <input type="checkbox" id="tiles" name="tiles" value="1"> টাইলস
					</label>
					<label for="mosaic" class="checkbox-inline">
					  <input type="checkbox" id="mosaic" name="mosaic" value="1"> মোজাইক
					</label>
				  </div>
				  <hr>
				  </div>
				  <button type="button" style="margin-bottom:12px;" id="interested_peo">যাদের কাছে ভাড়া দিতে আগ্রহী</button>
				  <div class="form-group" id="interested_cat">
					<label for="bachelor" class="checkbox-inline">
					  <input type="checkbox" id="bachelor" name="bachelor" value="1"> ব্যাচেলর
					</label>
					<label for="female" class="checkbox-inline">
					  <input type="checkbox" id="female" name="female" value="1"> নারী/ছাত্রী
					</label>
					<label for="huswife" class="checkbox-inline">
					  <input type="checkbox" id="huswife" name="huswife" value="1"> স্বামী/স্ত্রী
					</label>
					<label for="student" class="checkbox-inline">
					  <input type="checkbox" id="student" name="student" value="1"> ছাত্র
					</label>
					<label for="jobholder" class="checkbox-inline">
					  <input type="checkbox" id="jobholder" name="jobholder" value="1"> চাকুরিজীবী
					</label>
					<label for="business_holder" class="checkbox-inline">
					  <input type="checkbox" id="business_holder" name="business_holder" value="1"> ব্যবসায়ী
					</label>
				  </div>	
				<button type="submit" id="publish" class="btn btn-default">আপনার বিজ্ঞাপন প্রকাশ করুন</button>				  {{ csrf_field() }}
				<input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
				</div>
			</div>
		</form>
   </div>
   <!-- Create Add Part End -->
      </div>
      <!-- /.container-fluid -->
    </div>
<!-- /.content-wrapper -->
<script type="text/javascript">
    $(document).ready(function(){
        $('#division').change(function(){
            var division_id = $(this).val();
            var option = '';

            $.ajax({
                url : "{{ url('/get-districts') }}",
                method : "GET",
                data : {'division_id' : division_id},
                dataType : 'json',

                success:function(response){
                    var district_length = response.length; 
                    option += "<option selected disabled>জেলা নির্বাচন করুন</option>";
                    for( var i = 0; i < district_length; i++){
                        var id = response[i]['id'];
                        var district_name = response[i]['bn_name'];
                        option += "<option value='"+id+"'>"+district_name+"</option>";
                        $('#district').empty();
                    }
                    $("#district").append(option);
                },
                error:function(){
                    console.log("There was an error while fetching District!");
                }
            });
        });

        $('#district').change(function(){
            var district_id = $(this).val();
            var upazila_option = '';

            $.ajax({
                url : "{{ url('/get-upazila') }}",
                method : "GET",
                data : {'district_id' : district_id},
                dataType : 'json',

                success:function(upazilas){
                    upazila_option += "<option selected disabled>থানা নির্বাচন করুন</option>";
                    for(var i = 0; i < upazilas.length; i++){
                        var upazila_id = upazilas[i]['id'];
                        var upazila_name = upazilas[i]['bn_name'];
                        upazila_option += "<option value='"+upazila_id+"'>"+upazila_name+"</option>";
                        $('#upazila').empty();
                    }
                    $('#upazila').append(upazila_option);
                },
                error:function(){
                    console.log("There was an error while fetching Upazila!");
                }

            });
        });

        $('#upazila').change(function(){
            var upazilas_id = $(this).val();
            var area_name = '';
            $.ajax({
                url : "{{ url('/get-area') }}",
                method : "GET",
                data : {'upazilas_id' : upazilas_id},
                dataType : 'json',

                success:function(area){
                	var area_len = area.length;
                	var getData = '';

					for(var i = 0; i < area_len;i++)
				    {
			            getData += area[i].area+",";
			        }
			        getData = getData.split(',');
			        $( function() {

				    var availableTags = getData;
				    $( "#area" ).autocomplete({
				      source: availableTags
				    });
				  } );
					
				
                	
                },
                error:function(){
                    console.log("There was an error while fetching Thana!");
                }

            });
        });
    });




</script>
    <!-- Location Add -->
<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
<script>tinymce.init({ selector:'#add_details' });</script>
@endsection