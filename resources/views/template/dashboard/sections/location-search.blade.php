@extends('template.dashboard.master')
@section('title')
লোকেশন খুঁজুন
@endsection
@section('content')
<!-- /.content-wrapper -->
    <div class="content-wrapper">
      <div class="container-fluid">
        <!-- Breadcrumbs -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="{{ url('/dashboard') }}">ড্যাশবোর্ড</a>
          </li>
          <li class="breadcrumb-item active">লোকেশন খুঁজুন</li>
        </ol>
        <div class="dash-form">
          <form class="form-inline" method="POST" action="{{ url('location/search') }}" accept-charset="utf-8">
            <div class="form-group">
              <input type="text" name="search" id="search" class="form-control" placeholder="এলাকার নাম">
            </div>
            <button type="submit" style="margin-left: 10px;" class="btn">খুঁজুন</button>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
          </form>
        </div>
		 <table class="table table-striped table-bordered" style="margin-top: 10px;">
			<thead>
			  <tr>
				<th>বিভাগ</th>
				<th>জেলা</th>
				<th>থানা</th>
				<th>এলাকা</th>
				<th>বিস্তারিত</th>
			  </tr>
			</thead>
			<tbody>
        @if($data !== NULL)
        @foreach($data as $area)
        <tr>
          <td>{{ $area->division_name }}</td>
          <td>{{ $area->district_name }}</td>
          <td>{{ $area->thana_name }}</td>
          <td>{{ $area->area_name }}</td>
          <td>
            <a title="এডিট করুন" href="{{ url('location/edit/'.$area->area_id) }}" class="btn"><i class="fa fa-edit"></i></a>  
            <a title="ডিলিট করুন" href="{{ url('location/delete/'.$area->area_id) }}" class="btn text-danger"><i class="fa fa-trash"></i></a>
          </td>
        </tr>
        @endforeach
        @endif	  
			</tbody>
		  </table>		
      </div>
      <!-- /.container-fluid -->
    </div>
<!-- /.content-wrapper -->
@endsection