@extends('template.dashboard.master')
@section('content')
<!-- /.content-wrapper -->
    <div class="content-wrapper">
      <div class="container-fluid">
        <!-- Breadcrumbs -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">My Dashboard</li>
        </ol>
		<div class="">
			<div class="row">
				<div class="col-md-3">		
				</div>
				<div class="col-md-6 setting_button">
					<a href="#" id="edit_profile_button" class="btn">Edit Profile</a>
					<a href="#" id="change_pass_button" class="btn">Change Password</a>
					<a href="#" id="viwe_moderators_btn" class="btn">View Moderators</a>
					<a href="#" id="create_moderator_btn" class="btn">Create Moderator</a>
					<a href="{{ url('/user_list') }}" id="download_list_btn" class="btn">Download Mail List</a>

					<div id="edit_profile">
						<br>
						<h6>Edit Your Profile</h6>
						<form>
							<div class="form-group">
								<label for="name">Name</label>
								<input type="text" name="naem" id="name" placeholder="Name" class="form-control" />
							</div>
							<div class="form-group">
								<label for="profile_pic">Profile Photo</label>
								<input type="file" name="profile_pic" id="profile_pic" placeholder="Profile Photo" class="form-control" />
							</div>
							<div class="form-group">
								<label for="birth">Date of Birth</label>
								<input type="text" name="birth" id="birth" placeholder="Profile Photo" class="form-control datepicker" />
							</div>
							<button type="submit" class="btn">Save</button>
						</form>
					</div>
					<div id="change_pass">
						<br>
						<h6>Change Your Password</h6>
						<form>
							<div class="form-group">
								<label for="password">Password</label>
								<input type="password" name="password" id="password" placeholder="Password" class="form-control" />
							</div>
							<div class="form-group">
								<label for="Confrimpassword">Confirm Password</label>
								<input type="password" name="Confrimpassword" id="Confrimpassword" placeholder="Confirm Password" class="form-control" />
							</div>
							<button type="submit" class="btn">Save</button>
						</form>
					</div>					
					<div id="viwe_moderators">
						<br>
						<h6>All Moderators</h6>					
						<table class="table">
							<th>Name</th>
							<th>E-mail</th>
							<th>Action</th>
							<tr>
								<td>Ahmed Safa</td>
								<td>ahmedsafa@gmail.com</td>
								<td>
									<label class="switch">
									  <input type="checkbox">
									  <span class="slider"></span>
									</label>								
								</td>
							</tr>
							<tr>
								<td>Ahmed Safa</td>
								<td>ahmedsafa@gmail.com</td>
								<td>
									<label class="switch">
									  <input type="checkbox">
									  <span class="slider"></span>
									</label>								
								</td>
							</tr>
							<tr>
								<td>Ahmed Safa</td>
								<td>ahmedsafa@gmail.com</td>
								<td>
									<label class="switch">
									  <input type="checkbox">
									  <span class="slider"></span>
									</label>								
								</td>
							</tr>
						</table>							
					</div>
					<div id="create_moderator">
						<br>
						<h6>Create Moderator</h6>
						<form>
							<div class="form-group">
								<label for="phone">Phone</label>
								<input type="number" name="phone" id="phone" placeholder="Phone" class="form-control" />
							</div>
							<div class="form-group">
								<label for="email">E-mail</label>
								<input type="email" name="email" id="email" placeholder="E-mail" class="form-control" />
							</div>
							<div class="form-group">
								<label for="password">Password</label>
								<input type="password" name="password" id="password" placeholder="Password" class="form-control" />
							</div>
							<button type="submit" class="btn">Save</button>
						</form>
					</div>
				</div>
				<div class="col-md-3">		
				</div>		
			</div>		
		</div>		
      </div>
      <!-- /.container-fluid -->
    </div>
<!-- /.content-wrapper -->
@endsection