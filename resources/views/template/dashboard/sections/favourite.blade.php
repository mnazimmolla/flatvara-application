@extends('template.dashboard.master')
@section('title')
ফেভারিট বিজ্ঞাপন সমূহ
@endsection
@section('content')
<!-- /.content-wrapper -->
    <div class="content-wrapper">
      <div class="container-fluid">
        <!-- Breadcrumbs -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="{{ url('/dashboard') }}">ড্যাশবোর্ড</a>
          </li>
          <li class="breadcrumb-item active">ফেভারিট বিজ্ঞাপন সমূহ</li>
        </ol>
        @if(Session::has('message'))
          <h3 class="text-center text-success">{{ Session::get('message') }}</h3>
        @endif
     <table class="table table-striped table-bordered">
      <thead>
        <tr>
        <th>শ্রেণী</th>
        <th>শিরোনাম</th>
        <th>ভাড়া/বিক্রি</th>
        <th>বিস্তারিত</th>
        </tr>
      </thead>
      <tbody>
        @foreach($favourites as $favourite) 
        <tr>
        <td>{{ $favourite->category }}</td>
        <td>{{ $favourite->title }}</td>
        <td>{{ ($favourite->price) ? $favourite->price : $favourite->rent }}৳</td>
        <td>
          <a title="বিস্তারিত দেখুন" class="btn" href="{{ url('/ad/view/'.$favourite->id) }}"><i class="fa fa-eye"></i>
          </a>
          @if(Auth::check())
            <?php $user_id = Auth::User()->id; ?>
          @endif
          <a title="ফেভারিট থেকে মুছে ফেলুন" onclick="return confirm('ফেভারিট থেকে মুছে ফেলুন')" class="btn" href="{{ url('favourite/remove/'.$favourite->favId .'/' . $user_id) }}"><i class="fa fa-trash"></i>
          </a>
        </td>
        </tr>
        @endforeach
      </tbody>
      </table>
      {{ $favourites->links() }}		
      
      </div>
      <!-- /.container-fluid -->
    </div>
<!-- /.content-wrapper -->
@endsection