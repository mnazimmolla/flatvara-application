@extends('template.dashboard.master')
@section('title')
	{{ $ad->add_title }} 
@endsection
@section('content')

<!-- /.content-wrapper -->
    <div class="content-wrapper">
      <div class="container-fluid">
        <!-- Breadcrumbs -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="{{ url('/dashboard') }}">ড্যাশবোর্ড</a>
          </li>
          <li class="breadcrumb-item active">{{ $ad->add_title }}</li>
        </ol>

         <!-- Create Add Part Start -->
   <div class="container create-add">
   		@if(Session::has('message'))
   		<div class="alert alert-info">
   			{{ Session::get('message') }}
   		</div>
   		@endif
   		<form id="form1" method="POST" action="{{ url('ad/edit/'.$ad->id) }}" enctype="multipart/form-data" accept-charset="utf-8">
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-12">
				  <div class="form-group {{ $errors->has('class_add') ? 'has-error' : '' }}">
				    <label for="class_add">শ্রেণী নির্বাচন করুন</label>
					<select required id="class_add" name="class_add" class="form-control">
					  <option value="{{ $ad->category_id }}">{{ $ad->category_name }}</option>
					  @foreach($categories as $category)
					  <option value="{{ $category->id }}">{{ $category->category }}</option>
					  @endforeach
					</select>
                        @if ($errors->has('class_add'))
                            <span class="text-danger">
                                <strong>শ্রেণী নির্বাচন করুন</strong>
                            </span>
                        @endif  					
				  </div>
				  <div class="form-group" id="others_cat">
				    <label for="otherCat">প্রপার্টি নির্বাচন করুন</label>
					<select id="otherCat" name="otherCat" class="form-control">
					  <option selected disabled>প্রপার্টি নির্বাচন করুন</option>
					  @foreach($sub_categories as $sub_category)
					  <option value="{{ $sub_category->id }}">{{ $sub_category->sub_category }}</option>
					  @endforeach
					</select>				  
				  </div>				  
				  <div class="form-group {{ $errors->has('division') ? 'has-error' : '' }}">
				    <label for="division">বিভাগ নির্বাচন করুন</label>
					<select id="division" name="division" class="form-control">
					  <option value="{{ $ad->division_id }}">{{ $ad->division_name }}</option>
					  @foreach($divisions as $division)
					  <option value="{{ $division->id }}">{{ $division->bn_name }}</option>
					  @endforeach
					</select>
                        @if ($errors->has('division'))
                            <span class="text-danger">
                                <strong>বিভাগ নির্বাচন করুন</strong>
                            </span>
                        @endif 					
				  </div>
				  <div class="form-group {{ $errors->has('district') ? 'has-error' : '' }}">
				    <label for="district">জেলা নির্বাচন করুন</label>
					<select id="district" name="district" class="form-control">
						<option value="{{ $ad->district_id }}">{{ $ad->district }}</option>
						</select>
                        @if ($errors->has('district'))
                            <span class="text-danger">
                                <strong>জেলা নির্বাচন করুন</strong>
                            </span>
                        @endif 					
				  </div>
				  <div class="form-group {{ $errors->has('thana') ? 'has-error' : '' }}">
				    <label for="upazila">উপজেলা/থানা নির্বাচন করুন</label>
					<select id="upazila" name="thana" class="form-control">
						<option value="{{ $ad->thana_id }}">{{ $ad->thana }}</option>
					</select>
                        @if ($errors->has('thana'))
                            <span class="text-danger">
                                <strong>উপজেলা/থানা নির্বাচন করুন</strong>
                            </span>
                        @endif 						
				  </div>
				  <div class="form-group {{ $errors->has('area') ? 'has-error' : '' }}">
				    <label for="area">এলাকা নির্বাচন করুন</label>
					<input type="text" id="area" name="area" class="form-control" value="{{ $ad->area }}">
                        @if ($errors->has('area'))
                            <span class="text-danger">
                                <strong>এলাকা নির্বাচন করুন</strong>
                            </span>
                        @endif 					
				  </div>
				  <div class="form-group {{ $errors->has('add_title') ? 'has-error' : '' }}">
				  	<label for="add_title">বিজ্ঞাপনের শিরোনাম</label>
				  	<input type="text" name="add_title" id="add_title" value="{{ $ad->add_title }}" class="form-control">
                        @if ($errors->has('add_title'))
                            <span class="text-danger">
                                <strong>বিজ্ঞাপনের শিরোনাম দিন</strong>
                            </span>
                        @endif 				  	
				  </div>
				  <div class="form-group {{ $errors->has('add_title') ? 'has-error' : '' }}">
				  	<label for="full_address">পূর্ণাঙ্গ ঠিকানা</label>
				  	<textarea id="full_address" class="form-control" name="full_address">{{ $ad->full_address }}</textarea>
                        @if ($errors->has('full_address'))
                            <span class="text-danger">
                                <strong>পূর্ণাঙ্গ ঠিকানা দিন</strong>
                            </span>
                        @endif				  	
				  </div>
				  <div class="form-group">
				  	<label for="add_details">বিস্তারিত</label>
				  	<textarea class="form-control" rows="8" name="add_details" id="add_details">{{ $ad->add_details }}</textarea>
				  </div>
				  
				  <div class="form-group" id="land_cat">
					<label for="land_basis">প্লট/জমির ধরন</label><br>
				  	<input type="radio" name="land_basis" id="land_basis" value="{{ $ad->land_basis }}"> কৃষি
				  	<input type="radio" name="land_basis" id="land_basis" value="{{ $ad->land_basis }}"> বাণিজ্যিক
				  	<input type="radio" name="land_basis" id="land_basis" value="{{ $ad->land_basis }}"> আবাসিক
				  	<input type="radio" name="land_basis" id="land_basis" value="{{ $ad->land_basis }}"> অন্যান্য
				  </div>
				  <div class="form-group" id="unit_cat">
					  <div class="row">
						<div class="col-md-6">
						<label for="unit">ইউনিট</label>
						<select id="unit" name="unit" class="form-control">
						  <option selected disabled>একটি ইউনিট নির্বাচন করুন। </option>
						  <option value="কাঠা">কাঠা</option>
						  <option value="বিঘা">বিঘা</option>
						  <option value="একর">একর</option>
						  <option value="শতক">শতক</option>
						  <option value="ডেসিমাল">ডেসিমাল</option>
						</select>					
						</div>
						<div class="col-md-6">
						<label for="land_area">জমির আয়তন</label>
						<input type="text" name="land_area" id="land_area" value="{{ $ad->land_area }}" class="form-control">					
						</div>					
					  </div>
				  </div>
				  
				  <button type="button" style="margin-bottom:12px;" id="rent">ভাড়া দিন</button>
				  <button type="button" style="margin-bottom:12px;" id="sell">বিক্রি করুন </button>
				  <div class="form-group" id="rent_cat">
				  	<label for="sell_rent_basis">ভাড়ার পরিমাণ</label>
				  	<input type="text" name="sell_rent_basis" id="sell_rent_basis" value="{{ $ad->sell_rent_basis }}" class="form-control">
				  	<input type="radio" name="rent_basis" id="rent_basis" value="{{ $ad->rent_basis }}"> মাসিক
				  	<input type="radio" name="rent_basis" id="rent_basis" value="{{ $ad->rent_basis }}"> দৈনিক
				  	<input type="radio" name="rent_basis" id="rent_basis" value="{{ $ad->rent_basis }}"> বাৎসরিক
				  	<input type="radio" name="rent_basis" id="rent_basis" value="{{ $ad->rent_basis }}"> আলোচনা সাপেক্ষে<br/>
					<label for="rent_date_basis">ভাড়া শুরুর তারিখ</label>
					<input type="text" class="form-control datepicker" id="rent_date_basis" name="rent_date_basis" value="{{ $ad->rent_date_basis }}">
					<input type="checkbox" name="anytime" id="anytime" value="1"/> যেকোন সময়
					
					<div id="advance">
						<br>
						<label for="advance_cat">অগ্রীম ভাড়া</label>
						<select id="advance_cat" name="advance_cat" class="form-control">
						  <option value="0" selected disabled>অগ্রীম ভাড়ার মাস নির্বাচন করুন</option>
						  <option value="১ মাস">১ মাস</option>
						  <option value="২ মাস">২ মাস</option>
						  <option value="৩ মাস">৩ মাস</option>
						  <option value="৪ মাস">৪ মাস</option>
						  <option value="৫ মাস">৫ মাস</option>
						  <option value="৬ মাস">৬ মাস</option>
						  <option value="১ বছর">১ বছর</option>
						  <option value="২ বছর">২ বছর</option>
						  <option value="৩ বছর">৩ বছর</option>
						  <option value="আলোচনা সাপেক্ষে">আলোচনা সাপেক্ষে</option>
						</select>
					</div>					
				  </div>
				  <div class="form-group" id="sell_cat">
				  	<label for="sell_price_basis">দাম</label>
				  	<input type="text" name="sell_price_basis" id="sell_price_basis" value="{{ $ad->sell_price_basis }}" class="form-control">
				  </div>					  
				  <div class="form-group" id="sqr_for_cat_1">
				  	<label for="sq_feet">বর্গফুট</label>
				  	<input type="text" class="form-control" id="sq_feet" name="sq_feet" value="{{ $ad->sq_feet }}">
				  </div>
				  <div class="form-group" id="bed">
				  	<label for="bed_room">বেডরুম</label>
				  	<input type="text" class="form-control" id="bed_room" name="bed_room" value="{{ $ad->bed_room }}">
				  </div>
				  <div class="form-group" id="room_cat_id">
					<label for="room_cat">বেড/রুমের ধরণ  <label>
					<input type="radio" name="room_cat" id="room_cat" value="{{ $ad->room_cat }}"> সিঙ্গেল				  
					<input type="radio" name="room_cat" id="room_cat" value="{{ $ad->room_cat }}"> ডাবল				  
				  </div>				  
				  <div class="form-group" id="bath">
				  	<label for="bath_room">বাথরুম</label>
				  	<input type="text" class="form-control" id="bath_room" name="bath_room" value="{{ $ad->bath_room }}">
					<div id="bath_room_diff">
						<input type="checkbox" name="bath_room_attach" id="bath_room_attach" value="{{ $ad->bath_room_attach }}"> এটেস্টেড
					</div>					
				  </div>
				  <p>আপনার মোবাইল নং: 01717324345</p>
				  <button type="button" style="margin-bottom:12px; display:block;" id="mobile_add">আরেকটি মোবাইল নং যুক্ত করুন</button>
				  <div class="form-group" id="another_mobile">
				  	<label for="mobile2">মোবাইল নং</label>
				  	<input type="text" class="form-control" id="mobile2" name="mobile2" value="{{ $ad->mobile2 }}">
				  </div>
				  <button type="button" style="margin-bottom:12px;" id="mobile_add_2">আরেকটি মোবাইল নং যুক্ত করুন </button>
				  <div class="form-group" id="another_mobile_id">
				  	<label for="another_mobile_2">মোবাইল নং</label>
				  	<input type="text" class="form-control" id="another_mobile_2" name="another_mobile_2" value="{{ $ad->another_mobile_2 }}">
				  </div>
				  <button type="button" style="margin-bottom:12px;" id="mobile_add_3">আরেকটি মোবাইল নং যুক্ত করুন</button>
				  <div class="form-group" id="another_mobile_id_3">
				  	<label for="another_mobile_3">মোবাইল নং</label>
				  	<input type="text" class="form-control" id="another_mobile_3" name="another_mobile_3" value="{{ $ad->another_mobile_3 }}">
				  </div>				  
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<br>
				  <div class="form-group">
				    <label for="main_img">প্রধান ছবি নির্বাচন</label>
				    <input type="file" id="main_img" name="main_img">
					<img id="main_img_view" src="{{ asset('public/assets/img/flat/'.$ad->main_img) }}" alt="your image" height="90" width="90" />
					<img id="main" src="#" alt="your image">

				  </div> 
				  <div class="form-group">
				    <label for="img_two">ছবি নির্বাচন করুন</label>
				    <input type="file" id="img_two" name="img_two">
				    @if($ad->img_two)
					<img id="main_two_view" src="{{ asset('public/assets/img/flat/'.$ad->img_two) }}" alt="your image" height="90" width="90" />
					@endif
					<img id="main_two" src="#" alt="your image">
				  </div>
				  <div class="form-group">
				    <label for="img_three">ছবি নির্বাচন করুন</label>
				    <input type="file" id="img_three" name="img_three">
				    @if($ad->img_three)
					<img id="main_three_view" src="{{ asset('public/assets/img/flat/'.$ad->img_three) }}" alt="your image" height="90" width="90"/>
					@endif
					<img id="main_three" src="#" alt="your image">
				  </div>
				  <div class="form-group">
				    <label for="img_four">ছবি নির্বাচন করুন</label>
				    <input type="file" id="img_four" name="img_four">
				     @if($ad->img_four)
					<img id="main_four_view" src="{{ asset('public/assets/img/flat/'.$ad->img_four) }}" alt="your image" height="90" width="90"/>
					@endif
					<img id="main_four" src="#" alt="your image">
				  </div>
				  <div class="form-group">
				    <label for="img_five">ছবি নির্বাচন করুন</label>
				    <input type="file" id="img_five" name="img_five">
				    @if($ad->img_five)
					<img id="main_five_view" src="{{ asset('public/assets/img/flat/'.$ad->img_five) }}" alt="your image" height="90" width="90"/>
					@endif
					<img id="main_five" src="#" alt="your image">
				  </div>
				  <div class="form-group">
				    <label for="img_six">ছবি নির্বাচন করুন</label>
				    <input type="file" id="img_six" name="img_six">
				    @if($ad->img_six)
					<img id="main_six_view" src="{{ asset('public/assets/img/flat/'.$ad->img_six) }}" alt="your image" height="90" width="90"/>
					@endif
					<img id="main_six" src="#" alt="your image">
				  </div>
				  <hr>
				   <button type="button" style="margin-bottom:12px;" id="details_info">সুবিধা সমূহ যোগ করুন</button>
				   <div id="details_form">
				  <div class="form-group">
					<label for="ac" class="checkbox-inline">
					  <input type="checkbox" id="ac" name="ac" value="{{ $ad->ac }}" {{ ($ad->ac != null) ? "checked" : '' }}> এয়ার কন্ডিশন
					</label>
					<label for="balcony" class="checkbox-inline">
					  <input type="checkbox" id="balcony" name="balcony" value="{{ $ad->balcony }}" {{ ($ad->balcony != null) ? "checked" : '' }}> ব্যালকনি
					</label>
					<label for="cab_tv" class="checkbox-inline">
					  <input type="checkbox" id="cab_tv" name="cab_tv" value="{{ $ad->cab_tv }}" {{ ($ad->cab_tv != null) ? "checked" : '' }}> ক্যাবল টিভি
					</label>
					<label for="grill" class="checkbox-inline">
					  <input type="checkbox" id="grill" name="grill" value="{{ $ad->grill }}" {{ ($ad->grill != null) ? "checked" : '' }}> গ্রিল
					</label>
				  </div>
				  <div class="form-group">
					<label for="electricity" class="checkbox-inline">
					  <input type="checkbox" id="electricity" name="electricity" value="{{ $ad->electricity }}" {{ ($ad->electricity != null) ? "checked" : '' }}> বিদ্যুত
					</label>
					<label for="water" class="checkbox-inline">
					  <input type="checkbox" id="water" name="water" value="{{ $ad->water }}" {{ ($ad->water != null) ? "checked" : '' }}> পানি
					</label>
					<label for="cctv" class="checkbox-inline">
					  <input type="checkbox" id="cctv" name="cctv" value="{{ $ad->cctv }}" {{ ($ad->cctv != null) ? "checked" : '' }}> সিসিটিভি
					</label>
					<label for="wifi" class="checkbox-inline">
					  <input type="checkbox" id="wifi" name="wifi" value="{{ $ad->wifi }}" {{ ($ad->wifi != null) ? "checked" : '' }}> ওয়াইফাই/ইন্টারনেট
					</label>
				  </div>
				  <div class="form-group">
					<label for="lift" class="checkbox-inline">
					  <input type="checkbox" id="lift" name="lift" value="{{ $ad->lift }}" {{ ($ad->lift != null) ? "checked" : '' }}> লিফট
					</label>
					<label for="parking" class="checkbox-inline">
					  <input type="checkbox" id="parking" name="parking" value="{{ $ad->parking }}" {{ ($ad->parking != null) ? "checked" : '' }}> পার্কিং
					</label>
					<label for="gas" class="checkbox-inline">
					  <input type="checkbox" id="gas" name="gas" value="{{ $ad->gas }}" {{ ($ad->gas != null) ? "checked" : '' }}> গ্যাস
					</label>
				  </div>
				  <div class="form-group">
					<label for="generator" class="checkbox-inline">
					  <input type="checkbox" id="generator" name="generator" value="{{ $ad->generator }}" {{ ($ad->generator != null) ? "checked" : '' }}> জেনারেটর
					</label>
					<label for="sec_guard" class="checkbox-inline">
					  <input type="checkbox" id="sec_guard" name="sec_guard" value="{{ $ad->sec_guard }}" {{ ($ad->sec_guard != null) ? "checked" : '' }}> সিকুরিটি গার্ড
					</label>
				  </div>
				  <div class="form-group">
					<label for="tiles" class="checkbox-inline">
					  <input type="checkbox" id="tiles" name="tiles" value="{{ $ad->tiles }}" {{ ($ad->tiles != null) ? "checked" : '' }}> টাইলস
					</label>
					<label for="mosaic" class="checkbox-inline">
					  <input type="checkbox" id="mosaic" name="mosaic" value="{{ $ad->mosaic }}" {{ ($ad->mosaic != null) ? "checked" : '' }}> মোজাইক
					</label>
				  </div>
				  <hr>
				  </div>
				  <button type="button" style="margin-bottom:12px;" id="interested_peo">যাদের কাছে ভাড়া দিতে আগ্রহী</button>
				  <div class="form-group" id="interested_cat">
					<label for="bachelor" class="checkbox-inline">
					  <input type="checkbox" id="bachelor" name="bachelor" value="{{ $ad->bachelor }}" {{ ($ad->bachelor != null) ? "checked" : '' }}> ব্যাচেলর
					</label>
					<label for="female" class="checkbox-inline">
					  <input type="checkbox" id="female" name="female" value="{{ $ad->female }}" {{ ($ad->female != null) ? "checked" : '' }}> নারী/ছাত্রী
					</label>
					<label for="huswife" class="checkbox-inline">
					  <input type="checkbox" id="huswife" name="huswife" value="{{ $ad->huswife }}" {{ ($ad->huswife != null) ? "checked" : '' }}> স্বামী/স্ত্রী
					</label>
					<label for="student" class="checkbox-inline">
					  <input type="checkbox" id="student" name="student" value="{{ $ad->student }}" {{ ($ad->student != null) ? "checked" : '' }}> ছাত্র
					</label>
					<label for="jobholder" class="checkbox-inline">
					  <input type="checkbox" id="jobholder" name="jobholder" value="{{ $ad->jobholder }}" {{ ($ad->jobholder != null) ? "checked" : '' }}> চাকুরিজীবী
					</label>
					<label for="business_holder" class="checkbox-inline">
					  <input type="checkbox" id="business_holder" name="business_holder" value="{{ $ad->business_holder }}" {{ ($ad->business_holder != null) ? "checked" : '' }}> ব্যবসায়ী
					</label>
				  </div>	
				<button type="submit" id="publish" class="btn btn-default">আপনার বিজ্ঞাপন এডিট
				 করুন</button>
				 {{ csrf_field() }}
				</div>
			</div>
		</form>
   </div>
   <!-- Create Add Part End -->
      </div>
      <!-- /.container-fluid -->
    </div>
<!-- /.content-wrapper -->
<script type="text/javascript">
	$('#division').change(function(){
            var division_id = $(this).val();
            var option = '';
            var newOpt = "<option selected disabled>থানা নির্বাচন করুন</option>";
            $('#upazila').empty();
            $('#upazila').append(newOpt);

            $.ajax({
                url : "{{ url('/get-districts') }}",
                method : "GET",
                data : {'division_id' : division_id},
                dataType : 'json',

                success:function(response){
                    var district_length = response.length; 
                    option += "<option selected disabled>জেলা নির্বাচন করুন</option>";
                    for( var i = 0; i < district_length; i++){
                        var id = response[i]['id'];
                        var district_name = response[i]['bn_name'];
                        option += "<option value='"+id+"'>"+district_name+"</option>";
                        $('#district').empty();
                    }
                    $("#district").append(option);
                },
                error:function(){
                    console.log("There was an error while fetching District!");
                }
            });
        });
</script>
<!-- Location Add -->
<!-- Edit JS -->
<script type="text/javascript" src="{{ asset('public/dashboard/js/edit.js') }}"></script>
<!-- Edit JS -->
 <!-- Edit Image -->
<script type="text/javascript">
	$('#main_img').change(function(){
		var new_img = $(this).val();
		if(new_img != null)
		{
			$('#main_img_view').css('display','none');
		}
	});
	$('#img_two').change(function(){
		var new_img = $(this).val();
		if(new_img != null)
		{
			$('#main_two_view').css('display','none');
		}
	});
	$('#img_three').change(function(){
		var new_img = $(this).val();
		if(new_img != null)
		{
			$('#main_three_view').css('display','none');
		}
	});
	$('#img_four').change(function(){
		var new_img = $(this).val();
		if(new_img != null)
		{
			$('#main_four_view').css('display','none');
		}
	});
	$('#img_five').change(function(){
		var new_img = $(this).val();
		if(new_img != null)
		{
			$('#main_five_view').css('display','none');
		}
	});
	$('#img_six').change(function(){
		var new_img = $(this).val();
		if(new_img != null)
		{
			$('#main_six_view').css('display','none');
		}
	});
</script>
<!-- Edit Image -->
<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
<script>tinymce.init({ selector:'#add_details' });</script>    
@endsection