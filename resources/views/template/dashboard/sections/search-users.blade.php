@extends('template.dashboard.master')
@section('title')
  ব্যবহারকারী খুঁজুন
@endsection
@section('content')
<!-- /.content-wrapper -->
    <div class="content-wrapper">
      <div class="container-fluid">
        <!-- Breadcrumbs -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="{{ url('/dashboard') }}">ড্যাশবোর্ড</a>
          </li>
          <li class="breadcrumb-item active">ব্যবহারকারী খুঁজুন</li>
        </ol>
        @if(Session::has('message'))
          <h3 class="text-center text-success">{{ Session::get('message') }}</h3>
        @endif        
    		<div class="search_part">
    			<div class="row">
    				<div class="col-md-3">
    				</div>

    				<div class="col-md-6 search_part_mail">
              <form method="POST" action="{{ url('/search/user') }}" accept-charset="utf-8">  
                
                <div class="form-group">
                  <label for="user_cat_serach">ব্যবহারকারী খুঁজুন</label>
                  <input type="text" name="user_cat_serach" id="user_cat_serach" class="form-control" placeholder="ব্যবহারকারীর তথ্য" />
                </div>

                <div class="radio">
                  <label><input type="radio" name="serach_base" id="serach_base" value="1" checked>ফোন </label>
                  <label><input type="radio" name="serach_base" id="serach_base" value="2" checked>ইমেইল </label>
                  <label><input type="radio" name="serach_base" id="serach_base" value="3" checked>ইউজারনেম </label>
                  <label><input type="radio" name="serach_base" id="serach_base" value="4" checked>নাম </label>
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button type="submit" class="btn">ব্যবহারকারী খুঁজুন</button>

              </form>              
    				</div>
    				<div class="col-md-3">
    				</div>
    			</div>
    		</div>      
     <table class="table table-striped table-bordered" style="margin-top: 10px;">
      <thead>
        <tr>
          <th>ফেসবুক আইডি</th>
          <th>নাম</th>
          <th>ইমেইল</th>
          <th>জন্মদিন</th>
          <th>ব্লক</th>
        </tr>
      </thead>
      <tbody>                                
      </tbody>
      </table>
      </div>
      <!-- /.container-fluid -->
    </div>
<!-- /.content-wrapper -->
@endsection