@extends('template.dashboard.master')
@section('title')
    বার্তা সমূহ
@endsection
@section('content')
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <div class="container-fluid">
            <!-- Breadcrumbs -->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ url('/dashboard') }}">ড্যাশবোর্ড</a>
                </li>
                <li class="breadcrumb-item active">বার্তা সমূহ</li>
            </ol>

            @if(Session::has('delete'))
                <h5 style="text-align: center;" class="text-info">{{ Session::get('delete') }}</h5>
            @endif          
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>প্রেরক</th>
                    <th>বিষয়</th>
                    <th>ইমেইল</th>
                    <th>বার্তা</th>
                    <th>বিস্তারিত</th>
                </tr>
                </thead>
                <tbody>
                @foreach($messages as $message)
                <tr>
                    <td>{{ $message->name }}</td>
                    <td>{{ $message->subject}}</td>
                    <td>{{ $message->email }}</td>
                    <td>{{ @substr($message->message, 0, 20). '...' }}</td>
                    <td>
                        <a title="ইমেইল দেখুন" class="btn" href="{{ url('/message/' . $message->id) }}"><i class="fa fa-eye"></i></a>
                        <a title="ইমেইল মুছুন" class="btn text-danger" href="{{ url('/message/delete/'.$message->id) }}"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            {{ $messages->links() }}             
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-wrapper -->
@endsection