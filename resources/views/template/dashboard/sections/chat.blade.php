@extends('template.dashboard.master')
@section('title')
  Chat
@endsection
@section('content')
	<!-- /.content-wrapper -->
    <div class="content-wrapper">
      <div class="container-fluid">
        <!-- Breadcrumbs -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="{{ url('/dashboard') }}">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">My Dashboard</li>
        </ol>
		<div class="container-message">
		  <img src="http://i.dailymail.co.uk/i/pix/2017/04/20/13/3F6B966D00000578-4428630-image-m-80_1492690622006.jpg" alt="Avatar">
		  <p>Hello. How are you today?</p>
		  <span class="time-right">11:00</span>
		</div>

		<div class="container-message darker">
		  <img src="http://i.dailymail.co.uk/i/pix/2017/04/20/13/3F6B966D00000578-4428630-image-m-80_1492690622006.jpg" alt="Avatar" class="right">
		  <p>Hey! I'm fine. Thanks for asking!</p>
		  <span class="time-left">11:01</span>
		</div>

		<div class="container-message">
		  <img src="http://i.dailymail.co.uk/i/pix/2017/04/20/13/3F6B966D00000578-4428630-image-m-80_1492690622006.jpg" alt="Avatar">
		  <p>Sweet! So, what do you wanna do today?</p>
		  <span class="time-right">11:02</span>
		</div>

		<div class="container-message darker">
		  <img src="http://i.dailymail.co.uk/i/pix/2017/04/20/13/3F6B966D00000578-4428630-image-m-80_1492690622006.jpg" alt="Avatar" class="right">
		  <p>Nah, I dunno. Play soccer.. or learn more coding perhaps?</p>
		  <span class="time-left">11:05</span>
		</div>
		
		<div class="message-sending-box">
			<form action="/action_page.php">
				<div class="form-group">
				  <label for="comment">Message</label>
				  <textarea class="form-control" rows="5" placeholder="Write Your Message Here" id="comment"></textarea>
				</div>				
				<div class="form-group">
					<label for="message_photo">Send Image</label>
					<input type="file" name="message_photo" id="message_photo" />			
				</div>
				<button type="submit" class="btn btn-default">Send Message</button>
			</form>			
		</div>
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content-wrapper -->
@endsection