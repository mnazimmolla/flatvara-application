@extends('template.dashboard.master')
@section('title')
অপ্রকাশিত বিজ্ঞাপন সমূহ
@endsection
@section('content')
<!-- /.content-wrapper -->
    <div class="content-wrapper">
      <div class="container-fluid">
        <!-- Breadcrumbs -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="{{ url('/dashboard') }}">ড্যাশবোর্ড</a>
          </li>
          <li class="breadcrumb-item active">অপ্রকাশিত বিজ্ঞাপন সমূহ</li>
        </ol>
        @if(Session::has('message'))
          <h3 class="text-center text-success">{{ Session::get('message') }}</h3>
        @endif
     <table class="table table-striped table-bordered">
      <thead>
        <tr>
        <th>শ্রেণী</th>
        <th>শিরোনাম</th>
        <th>ভাড়া/বিক্রি</th>
        <th>ইউজার আইডি</th>
        <th>ইউজার</th>
        <th>বিস্তারিত</th>
        </tr>
      </thead>
      <tbody>
        @foreach($ads as $ad) 
        <tr>
        <td>{{ $ad->category }}</td>
        <td>{{ $ad->title }}</td>
        <td>{{ ($ad->price) ? $ad->price : $ad->rent }}৳</td>
        <td>{{ $ad->Userid }}</td>
        <td>{{ $ad->Username }}</td>
        <td>
          <a title="বিস্তারিত দেখুন" class="btn" href="{{ url('/ad/pending/view/'.$ad->id) }}">
            <i class="fa fa-eye"></i>
          </a>
          <a title="প্রকাশ করুন" class="btn" href="{{ url('/ad/pending/publish/'.$ad->id) }}">
            <i class="fa fa-share"></i>
          </a>
          <a title="ম্যাসেজ দিন" class="btn" href="{{ url('/ad/message/'.$ad->id) }}">
            <i class="fa fa-envelope"></i>
          </a>
          <a title="ডিলিট করুন" onclick="return confirm('ফেভারিট থেকে মুছে ফেলুন')" class="btn text-danger" href="{{ url('ad/pending/delete/' . $ad->id) }}">
            <i class="fa fa-trash"></i>
          </a>
        </td>
        </tr>
        @endforeach
      </tbody>
      </table>
      {{ $ads->links() }}        
      
      </div>
      <!-- /.container-fluid -->
    </div>
<!-- /.content-wrapper -->
@endsection