@extends('template.dashboard.master')
@section('title')
    লোকেশন এডিট করুন
@endsection
@section('content')
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <div class="container-fluid">
            <!-- Breadcrumbs -->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ url('/dashboard') }}">ড্যাশবোর্ড</a>
                </li>
                <li class="breadcrumb-item active">লোকেশন এডিট করুন</li>
            </ol>
            <!-- Create Add Part Start -->
            <div class="container create-add">
                <form method="POST" action="{{ url('location/edit/'.$data['edit']->area_id) }}" accept-charset="utf-8">
                    <div class="form-group {{ $errors->has('division_id') ? ' has-error' : '' }}">
                        <label for="division">বিভাগ নির্বাচন করুন</label>
                        <select id="division" class="form-control" name="division_id">
                            <option value="{{ $data['edit']->division_id }}">{{ $data['edit']->division_name }}</option>
                            @foreach($data['divisions'] as $division)
                            <option value="{{$division->id}}">{{ $division->bn_name }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('division_id'))
                            <span class="text-danger">
                                <strong>{{ $errors->first('division_id') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('district_id') ? ' has-error' : '' }}">
                        <label for="district">জেলা নির্বাচন করুন</label>
                        <select class="form-control" id="district" name="district_id">
                            <option value="{{ $data['edit']->district_id }}">{{ $data['edit']->district_name }}</option>
                        </select>
                        
                        @if ($errors->has('district_id'))
                            <span class="text-danger">
                                <strong>{{ $errors->first('district_id') }}</strong>
                            </span>
                         @endif
                    </div>
                    <div class="form-group {{ $errors->has('thana_id') ? ' has-error' : '' }}">
                        <label for="upazila">থানা নির্বাচন করুন</label>
                        <select class="form-control" id="upazila" name="thana_id">
                            <option value="{{ $data['edit']->thana_id }}">{{ $data['edit']->thana_name }}</option>
                        </select>
                        
                        @if ($errors->has('thana_id'))
                            <span class="text-danger">
                                <strong>{{ $errors->first('thana_id') }}</strong>
                            </span>
                         @endif
                    </div>
                    <div class="form-group {{ $errors->has('area') ? ' has-error' : '' }}">
                        <label for="area">এলাকা</label>
                        <input type="text" name="area" id="area" class="form-control" placeholder="এলাকা" value="{{ $data['edit']->area_name }}">
                        @if ($errors->has('area'))
                            <span class="text-danger">
                                <strong>{{ $errors->first('area') }}</strong>
                            </span>
                         @endif
                    </div>
                    <button type="submit" id="publish" class="btn btn-default">এলাকা এডিট করুন</button>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </form>
            </div>
            <!-- Create Add Part End -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-wrapper -->
     <!-- Location Add -->
    <script type="text/javascript">
    $(document).ready(function(){
        $('#division').change(function(){
            var division_id = $(this).val();
            var option = '';
            var newOpt = "<option selected disabled>থানা নির্বাচন করুন</option>";
            $('#upazila').empty();
            $('#upazila').append(newOpt);

            $.ajax({
                url : "{{ url('/get-districts') }}",
                method : "GET",
                data : {'division_id' : division_id},
                dataType : 'json',

                success:function(response){
                    var district_length = response.length; 
                    option += "<option selected disabled>জেলা নির্বাচন করুন</option>";
                    for( var i = 0; i < district_length; i++){
                        var id = response[i]['id'];
                        var district_name = response[i]['bn_name'];
                        option += "<option value='"+id+"'>"+district_name+"</option>";
                        $('#district').empty();
                    }
                    $("#district").append(option);
                },
                error:function(){
                    console.log("There was an error while fetching District!");
                }
            });
        });

        $('#district').change(function(){
            var district_id = $(this).val();
            var upazila_option = '';

            $.ajax({
                url : "{{ url('/get-upazila') }}",
                method : "GET",
                data : {'district_id' : district_id},
                dataType : 'json',

                success:function(upazilas){
                    upazila_option += "<option selected disabled>থানা নির্বাচন করুন</option>";
                    for(var i = 0; i < upazilas.length; i++){
                        var upazila_id = upazilas[i]['id'];
                        var upazila_name = upazilas[i]['bn_name'];
                        upazila_option += "<option value='"+upazila_id+"'>"+upazila_name+"</option>";
                        $('#upazila').empty();
                    }
                    $('#upazila').append(upazila_option);
                },
                error:function(){
                    console.log("There was an error while fetching Upazila!");
                }

            });
        });

        $('#upazila').change(function(){
            var upazilas_id = $(this).val();
            var thana_option = '';

            $.ajax({
                url : "{{ url('/get-thana') }}",
                method : "GET",
                data : {'upazilas_id' : upazilas_id},
                dataType : 'json',

                success:function(thana){
                    thana_option += "<option selected disabled>Select Thana</option>";
                    for(var i = 0; i < thana.length; i++){
                        var thana_id = thana[i]['id'];
                        var thana_name = thana[i]['bn_name'];
                        thana_option += "<option value='"+thana_id+"'>"+thana_name+"</option>";
                        $('#thana').empty();
                    }
                    $('#thana').append(thana_option);
                },
                error:function(){
                    console.log("There was an error while fetching Thana!");
                }

            });
        });
    });


    $( function() {
        var availableTags = [
            "ActionScript",
            "AppleScript",
            "Asp",
            "BASIC",
            "C",
            "C++",
            "Clojure",
            "COBOL",
            "ColdFusion",
            "Erlang",
            "Fortran",
            "Groovy",
            "Haskell",
            "Java",
            "JavaScript",
            "Lisp",
            "Perl",
            "PHP",
            "Python",
            "Ruby",
            "Scala",
            "Scheme"
        ];
        $( "#area" ).autocomplete({
            source: availableTags
        });
    } );

</script>
    <!-- Location Add -->

@endsection