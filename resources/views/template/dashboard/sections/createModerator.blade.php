@extends('template.dashboard.master')
@section('title')
    মোডারেটর যোগ করুন
@endsection
@section('content')
    <!-- /.content-wrapper -->
    <div class="content-wrapper">
        <div class="container-fluid">
            <!-- Breadcrumbs -->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ url('/dashboard') }}">ড্যাশবোর্ড</a>
                </li>
                <li class="breadcrumb-item active">মোডারেটর যোগ করুন</li>
            </ol>
            <!-- Create Add Part Start -->
            <div class="container create-add">
                <form method="POST" action="{{ url('/moderator-create') }}" accept-charset="utf-8">
                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name">নাম</label>
                        <input type="text" name="name" id="name" class="form-control" placeholder="নাম">
                        @if ($errors->has('name'))
                            <span class="text-danger">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                         @endif
                    </div>
                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email">ইমেইল অথবা ফোন</label>
                        <input type="text" name="email" id="email" class="form-control" placeholder="ইমেইল অথবা ফোন">
                        @if ($errors->has('email'))
                            <span class="text-danger">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                         @endif
                    </div>
                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password">পাসওয়ার্ড দিন</label>
                        <input type="password" name="password" id="password" class="form-control" placeholder="পাসওয়ার্ড দিন">
                        @if ($errors->has('password'))
                            <span class="text-danger">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                         @endif
                    </div>
                    <button type="submit" id="publish" class="btn btn-default">মোডারেটর যোগ করুন</button>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </form>
            </div>
            <!-- Create Add Part End -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-wrapper -->


@endsection