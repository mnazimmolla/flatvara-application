@extends('template.dashboard.master')
@section('title')
Suspended Users
@endsection
@section('content')
<!-- /.content-wrapper -->
    <div class="content-wrapper">
      <div class="container-fluid">
        <!-- Breadcrumbs -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">My Dashboard</li>
        </ol>
		<div class="search_part">
			<div class="row">
				<div class="col-md-3">
				</div>
				<div class="col-md-6 search_part_mail">
					<form action="{{ url('/suspended/user') }}" method="POST">
						<div class="form-group">
							<label for="user_cat_serach">Search User</label>
							<input type="text" name="user_cat_serach" id="user_cat_serach" class="form-control" placeholder="User Info" />
						</div>
						<div class="radio">
						  <label><input type="radio" name="serach_base" id="serach_base" value="1" checked>Phone </label>
						  <label><input type="radio" name="serach_base" id="serach_base" value="2" checked>E-mail </label>
						  <label><input type="radio" name="serach_base" id="serach_base" value="2" checked>User Name </label>
						  <label><input type="radio" name="serach_base" id="serach_base" value="3" checked>Name </label>
						</div>	
						<button type="submit" class="btn">Search</button>
					</form>				
				</div>
				<div class="col-md-3">
				</div>
			</div>
		</div>
      </div>
      <!-- /.container-fluid -->
    </div>
<!-- /.content-wrapper -->
@endsection