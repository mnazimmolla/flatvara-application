
<!-- Footer Area -->
<footer class="sticky-footer">
    <div class="container">
        <div class="text-center">
            <small>Copyright &copy; Your Website 2017</small>
        </div>
    </div>
</footer>

<!-- Scroll to Top Button -->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fa fa-angle-up"></i>
</a>

<!-- Logout Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">লগ আউট করতে চান?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                বর্তমান সেশন শেষ করে লগ আউট করতে চাইলে নিচের লগ আউট বাটনে ক্লিক করুন
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">বাদ দিন</button>
                <a class="btn btn-primary" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                             <i class="fa fa-fw fa-sign-out"></i>
                    লগ আউট
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                   {{ csrf_field() }}
                </form>                
            </div>
        </div>
    </div>
</div>

<!-- Slider -->
<script src="{{ asset('public/assets/js/propper.js') }}"></script>
<script src="{{ asset('public/assets/js/jsor_slider.js') }}"></script>
<script src="{{ asset('public/assets/js/jssor.slider.main.js') }}"></script>
<script type="text/javascript">jssor_1_slider_init();</script>
<!-- Slider -->

<!-- Bootstrap core JavaScript -->
<script>
    // $( function() {
    //     var availableTags = [
    //         "ActionScript",
    //         "AppleScript",
    //         "Asp",
    //         "BASIC",
    //         "C",
    //         "C++",
    //         "Clojure",
    //         "COBOL",
    //         "ColdFusion",
    //         "Erlang",
    //         "Fortran",
    //         "Groovy",
    //         "Haskell",
    //         "Java",
    //         "JavaScript",
    //         "Lisp",
    //         "Perl",
    //         "PHP",
    //         "Python",
    //         "Ruby",
    //         "Scala",
    //         "Scheme"
    //     ];
    //     $( "#area" ).autocomplete({
    //         source: availableTags
    //     });
    // } );
</script>
<script src="{{ asset('public/dashboard/vendor/popper/popper.min.js') }}"></script>
<script src="{{ asset('public/dashboard/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- Plugin JavaScript -->
<script src="{{ asset('public/dashboard/vendor/jquery-easing/jquery.easing.min.js') }}"></script>
<script src="{{ asset('public/dashboard/vendor/chart.js/Chart.min.js') }}"></script>
<script src="{{ asset('public/dashboard/vendor/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('public/dashboard/vendor/datatables/dataTables.bootstrap4.js') }}"></script>
<!-- Custom scripts for this template -->
<script src="{{ asset('public/dashboard/js/sb-admin.min.js') }}"></script>
<script src="{{ asset('public/dashboard/js/add-photo.js') }}"></script>
<script src="{{ asset('public/dashboard/js/show-photo.js') }}"></script>
<script src="{{ asset('public/dashboard/js/script.js') }}"></script>
<script src="{{ asset('public/dashboard/js/create-add.js') }}"></script>
<script src="{{ asset('public/dashboard/js/date-basis.js') }}"></script>
<script>
    $(function () {
        $(".datepicker").datepicker({ dateFormat: 'dd-mm-yy' });
    });
</script>
<script src="{{ asset('public/dashboard/js/setting.js') }}"></script>


</body>

</html>
