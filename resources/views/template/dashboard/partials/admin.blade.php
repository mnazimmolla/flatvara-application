<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="{{ url('/') }}">ফ্লাটভাড়া.কম</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
            <li class="nav-item active" data-toggle="tooltip" data-placement="right" title="Dashboard">
                <a class="nav-link" href="{{ url('/dashboard') }}">
                    <i class="fa fa-fw fa-dashboard"></i>
                    <span class="nav-link-text">
                ড্যাশবোর্ড</span>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Charts">
                <a class="nav-link" href="{{ url('/dashboard/ads') }}">
                    <i class="fa fa-fw fa-pencil"></i>
                    <span class="nav-link-text">
                বিজ্ঞাপন সমূহ</span>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Charts">
                <a class="nav-link" href="{{ url('/pendings') }}">
                    <i class="fa fa-fw fa-pencil"></i>
                    <span class="nav-link-text">
               অপ্রকাশিত বিজ্ঞাপন সমূহ</span>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Charts">
                <a class="nav-link" href="{{ url('/promoted') }}">
                    <i class="fa fa-fw fa-pencil"></i>
                    <span class="nav-link-text">
               প্রমোটেড বিজ্ঞাপন সমূহ</span>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Charts">
                <a class="nav-link" href="{{ url('/promote/request') }}">
                    <i class="fa fa-fw fa-pencil"></i>
                    <span class="nav-link-text">
               প্রোমোট রিকুয়েস্ট</span>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tables">
                <a class="nav-link" href="{{ url('messages') }}">
                    <i class="fa fa-fw fa-table"></i>
                    <span class="nav-link-text">
                বার্তা সমূহ</span>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Messages">
                <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMessage" data-parent="#exampleAccordion">
                    <i class="fa fa-fw fa-wrench"></i>
                    <span class="nav-link-text">
                চ্যাট</span>
                </a>
                <ul class="sidenav-second-level collapse" id="collapseMessage">
                    <li>
                        <a href="messages.html">David Miller</a>
                    </li>
                    <li>
                        <a href="messages.html">John Doe</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tables">
                <a class="nav-link" href="{{ url('/reports') }}">
                    <i class="fa fa-fw fa-table"></i>
                    <span class="nav-link-text">
                অভিযোগ সমূহ</span>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Charts">
                <a class="nav-link" href="{{ url('/location/add') }}">
                    <i class="fa fa-fw fa-pencil"></i>
                    <span class="nav-link-text">
                এলাকা যোগ করুন</span>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Charts">
                <a class="nav-link" href="{{ url('/location/search') }}">
                    <i class="fa fa-fw fa-pencil"></i>
                    <span class="nav-link-text">
                 এলাকা খুঁজুন</span>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Charts">
                <a class="nav-link" href="{{ url('/search/user') }}">
                    <i class="fa fa-fw fa-pencil"></i>
                    <span class="nav-link-text">
              ব্যবহারকারী খুঁজুন</span>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Charts">
                <a class="nav-link" href="{{ url('/suspended/user') }}">
                    <i class="fa fa-fw fa-pencil"></i>
                    <span class="nav-link-text">
              সাময়িক বন্ধ ব্যবহারকারী</span>
                </a>
            </li>            
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Messages">
                <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseSetting" data-parent="#exampleAccordion">
                    <i class="fa fa-fw fa-wrench"></i>
                    <span class="nav-link-text">
                সেটিংস</span>
                </a>
                <ul class="sidenav-second-level collapse" id="collapseSetting">
                    <li>
                        <a href="{{ url('get-user') }}">ব্যাবহারকারী তালিকা</a>
                    </li>
                    <li>
                        <a href="{{ url('/moderator-list') }}">মডারেটর তালিকা</a>
                    </li>
                    <li>
                        <a href="{{ url('/moderator-create') }}">মডারেটর যোগ</a>
                    </li>
                </ul>
            </li>
        </ul>
        <ul class="navbar-nav sidenav-toggler">
            <li class="nav-item">
                <a class="nav-link text-center" id="sidenavToggler">
                    <i class="fa fa-fw fa-angle-left"></i>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link">{{Auth::user()->name}}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
                    <i class="fa fa-fw fa-sign-out"></i>
                    লগ আউট</a>
            </li>
        </ul>
    </div>
</nav>
<!-- Navigation -->