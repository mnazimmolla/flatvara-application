<?php
	if (Auth::user()->status === 4) {
		echo 'You are blocked';
		exit();
	}
?>
@include('template.dashboard.partials.header')


@if(Auth::user()->status ==3)
@include('template.dashboard.partials.user')
@elseif(Auth::user()->status ==2)
@include('template.dashboard.partials.moderator')
@elseif(Auth::user()->status ==1)
@include('template.dashboard.partials.admin')
@endif



@yield('content')
@include('template.dashboard.partials.footer')


