<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('status')->nullable();
            $table->integer('promotion')->nullable();
            $table->date('promoted_at')->nullable();
            $table->integer('class_add');
            $table->integer('otherCat')->nullable();
            $table->integer('division');
            $table->integer('district');
            $table->integer('thana');
            $table->string('area');
            $table->string('add_title');
            $table->text('full_address');
            $table->text('add_details');
            $table->string('land_basis')->nullable();
            $table->string('unit')->nullable();
            $table->string('land_area')->nullable();
            $table->string('sell_rent_basis')->nullable();
            $table->string('sell_price_basis')->nullable();
            $table->string('rent_basis')->nullable();
            $table->string('rent_date_basis')->nullable();
            $table->integer('anytime')->nullable();
            $table->string('advance_cat')->nullable();
            $table->string('sq_feet')->nullable();
            $table->string('bed_room')->nullable();
            $table->integer('room_cat')->nullable();
            $table->string('bath_room')->nullable();
            $table->integer('bath_room_attach')->nullable();
            $table->string('mobile2')->nullable();
            $table->string('another_mobile_2')->nullable();
            $table->string('another_mobile_3')->nullable();
            $table->string('main_img');
            $table->string('img_two')->nullable();
            $table->string('img_three')->nullable();
            $table->string('img_four')->nullable();
            $table->string('img_five')->nullable();
            $table->string('img_six')->nullable();
            $table->integer('ac')->nullable();
            $table->integer('balcony')->nullable();
            $table->integer('cab_tv')->nullable();
            $table->integer('grill')->nullable();
            $table->integer('electricity')->nullable();
            $table->integer('water')->nullable();
            $table->integer('cctv')->nullable();
            $table->integer('wifi')->nullable();
            $table->integer('lift')->nullable();
            $table->integer('parking')->nullable();
            $table->integer('gas')->nullable();
            $table->integer('generator')->nullable();
            $table->integer('sec_guard')->nullable();
            $table->integer('tiles')->nullable();
            $table->integer('mosaic')->nullable();
            $table->integer('bachelor')->nullable();
            $table->integer('female')->nullable();
            $table->integer('huswife')->nullable();
            $table->integer('student')->nullable();
            $table->integer('jobholder')->nullable();
            $table->integer('business_holder')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads');
    }
}
