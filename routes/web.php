<?php
Route::get('/', 'FrontEndController@home');

Route::get('/search', function () {
    return view('template.frontEnd.sections.searchAd');
});
Route::get('/login', function () {
    return view('template.frontEnd.sections.login');
});
Route::get('/signup', function () {
    return view('template.frontEnd.sections.signup');
});
Route::get('/forgetPassword', function () {
    return view('template.frontEnd.sections.forgetPassword');
});

Route::get('/contact', 'EmailController@index');
Route::post('/contact', 'EmailController@store');

//Dashboard
Route::get('/dashboard', 'AdController@CounterHome');



Route::get('/messages', 'EmailController@show')->middleware('auth');

Route::get('/message/{id}', 'EmailController@messageByid')->middleware('auth');

Route::get('/message/delete/{id}', 'EmailController@destroy')->middleware('auth');

Route::get('/pending', function () {
    return view('template.dashboard.sections.pending');
});
Route::get('/settings', function () {
    return view('template.dashboard.sections.settings');
});

Route::get('/suspended/user', function () {
    return view('template.dashboard.sections.suspended-user');
});
Route::get('/suspended/users', function () {
    return view('template.dashboard.sections.suspended-users');
});
Route::get('/report', function () {
    return view('template.dashboard.sections.report');
});

//User

Route::get('/chat', function () {
    return view('template.dashboard.sections.chat');
});

//Auth
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//Auth Facebook
Route::get('auth/facebook', 'Auth\LoginController@redirectToProvider');
Route::get('auth/facebook/callback', 'Auth\LoginController@handleProviderCallback');


//Development
Route::get('/location/add', 'LocationController@index');

Route::post('/location/add', 'AreaController@store');

Route::get('/get-districts', 'LocationController@getDistricts');

Route::get('/get-upazila', 'LocationController@getUpazilas');

Route::get('/get-area', 'LocationController@getAreaByThana');

Route::get('location/search', 'AreaController@searchAreaForm');

Route::post('location/search', 'AreaController@searchArea');

Route::get('location/edit/{id}', 'AreaController@editLocationForm');

Route::post('location/edit/{id}', 'AreaController@update');

Route::get('/location/delete/{id}', 'AreaController@destroy');

Route::get('ad/create', 'AdController@create');

Route::post('ad/create', 'AdController@store');

Route::get('my/ad', 'AdController@index');

Route::get('ad/published', 'AdController@publishedAds');

Route::get('/ad/view/{id}', 'AdController@show');

Route::get('/ad/view/admin/{id}', 'AdController@showforAdmin');

Route::get('ad/delete/admin/{id}', 'AdController@destroyforAdmin');

Route::post('ad/edit/{id}', 'AdController@update');

Route::get('ad/edit/{id}', 'AdController@edit');

Route::get('ad/delete/{id}', 'AdController@destroy');

Route::get('/ad/unpublished', 'AdController@unpublishedAd');

Route::get('/ad/saved', 'AdController@savedAd');

Route::get('/ad/save/{id}/{user_id}', 'AdController@saveAd');

Route::get('/ad/saved/publish/{id}/{user_id}', 'AdController@saveAdPublish');

//Admin
Route::get('/dashboard/ads', 'AdController@ads');

Route::get('ad/publish/{id}', 'AdController@publish');

Route::get('/pendings', 'AdController@pendingsAds');

Route::get('/ad/pending/view/{id}', 'AdController@pendingShow');

Route::get('/ad/pending/publish/{id}', 'AdController@pendingPublish');

Route::get('/ad/pending/delete/{id}', 'AdController@pendingDestroy');

//Development Frond End
Route::get('view/{id}', 'FrontEndController@show');

//Development Front End & Back End
Route::get('ad/favourite/{id}', 'AdController@favourite')->middleware('auth');

Route::get('favourite/remove/{id}/{user_id}', 'AdController@removeFavourite');

Route::get('ad/favourite', 'AdController@favouriteAds');


Route::get('ad/report/{id}/', 'AdController@showReportFrom')->middleware('auth');

Route::get('reports', 'AdController@showReports');

Route::get('ad/report/delete/{id}', 'AdController@reportAdDelete');

Route::get('/ad/report/view/{id}', 'AdController@reportViewById');

Route::post('ad/report/', 'AdController@report')->middleware('auth');

//Special
Route::get('/get-user', 'AdController@getUsers');


Route::get('/moderator-list', 'AdminController@index');
Route::get('/moderator-create', 'AdminController@create');
Route::post('/moderator-create', 'AdminController@store');
Route::get('/moderator/delete/{id}', 'AdminController@destroy');

//Front End

Route::get('/ads', 'FrontEndController@index');

Route::get('/ad/promote', 'PromotionController@promoteAd');

Route::get('/ad/promote/{id}', 'PromotionController@promoteAdbyId');

Route::post('/make/promotion', 'PromotionController@store');

Route::get('/promotion/{id}', 'PromotionController@create');

Route::get('my/promoted', 'PromotionController@index');

Route::get('/promoted', 'PromotionController@promoted');

Route::get('/promote/request', 'AdController@promoteRequest');

Route::get('/promote/create/{id}', 'PromotionController@promoteCreate');

//Search

Route::get('searchByClass', 'FrontEndController@searchByClass');


Route::get('/search/user', 'AdminController@searchUser');

Route::post('/search/user', 'AdminController@getUsers');

Route::get('/block-user/{id}', 'AdminController@blockUser');

Route::get('/unblock-user/{id}', 'AdminController@unblockUser');

// Route::get('/search/users', function () {
//     return view('template.dashboard.sections.get-users');
// });



//Backup
// Route::get('/search/user', function () {
//     return view('template.dashboard.sections.search-user');
// });
// Route::post('/search/user', function () {
//     return view('template.dashboard.sections.search-user');
// });

Route::get('clean-up', 'FrontEndController@cleanUp');